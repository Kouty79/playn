package org.pointclick.geometry.ui.swing.main;

import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import org.pointclick.geometry.scenario.Scenario;
import org.pointclick.geometry.scenario.showcases.Scenarios;
import org.pointclick.geometry.ui.swing.ScenarioWindow;


public class ScenarioDrawer {

	private static final int SCREEN = 1;
	
	public static void main(String[] args) {
		new ScenarioDrawer().start();
	}
	
	public void start() {
		showScenarios(Scenarios.getScenarios());
//		ScenarioWindow scenarioWindow = new ScenarioWindow(Scenarios.twoSquaresScenarioB());
//		scenarioWindow.setVisible(true);
	}

	private void showScenarios(List<Scenario> scenarios) {
		List<JFrame> windows = new ArrayList<JFrame>();
		for (Scenario scenario : scenarios) {
			ScenarioWindow scenarioWindow = new ScenarioWindow(scenario);
			scenarioWindow.setVisible(true);
			windows.add(scenarioWindow);
		}
		
		arrangeWindows(windows, SCREEN);
	}
	
	private void arrangeWindows(List<JFrame> windows, int screen) {
//		Rectangle bounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
		Rectangle bounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[screen].getDefaultConfiguration().getBounds();
		
		int screenWidth = (int)bounds.getWidth();
		int screenHeight = (int)bounds.getHeight();
		
		int num = (int)Math.ceil(Math.sqrt(windows.size()));
		int width = screenWidth / num;
		int height = screenHeight / num;
		
		int i = 0;
		int j = 0;
		for (JFrame jFrame : windows) {
			jFrame.setLocation((int)bounds.getX() + i * width,  (int)bounds.getY() + j * height);
			jFrame.setSize(width, height);
			
			i++;
			if (i >= num) {
				i = 0;
				j++;
			}
		}
	}
	
}
