package org.pointclick.geometry.ui.swing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;

import javax.swing.JPanel;

import org.pointclick.geometry.Area;
import org.pointclick.geometry.Path;
import org.pointclick.geometry.Polygon;
import org.pointclick.geometry.Segment;
import org.pointclick.geometry.Vertex;
import org.pointclick.geometry.graph.Node;
import org.pointclick.geometry.graph.impl.DefaultNode;
import org.pointclick.geometry.impl.DefaultSegment;
import org.pointclick.geometry.impl.DefaultVertex;
import org.pointclick.geometry.scenario.ClipArea;
import org.pointclick.geometry.scenario.Scenario;
import org.pointclick.geometry.scenario.Scenario.Element;
import org.pointclick.geometry.shortestpath.VertexTree;

public class ScenarioPanel extends JPanel {

	private static final long serialVersionUID = 923947813645863628L;
	
	private Scenario scenario;
	private double clipW;
	private double clipH;
	private static final int PADDING = 10;
	
	private Rectangle clipBounds;
	
	public ScenarioPanel(Scenario scenario) {
		this.scenario = scenario;
		clipW = scenario.getClipArea().getX2() - scenario.getClipArea().getX1();
		clipH = scenario.getClipArea().getY2() - scenario.getClipArea().getY1();
		
		this.addMouseListener(new MyMouseListener());
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		paintAreas(g);
		paintGraphs(g);
		paintPaths(g);
		paintSegments(g);
		paintVertices(g);
	}

	private void paintVertices(Graphics g) {
		Iterator<Element<Vertex>> verticesIterator = scenario.verticesIterator();
		while (verticesIterator.hasNext()) {
			Element<Vertex> element = (Element<Vertex>) verticesIterator.next();
			g.setColor(element.getColor());
			paint(g, element.getElement());
		}
	}
	
	private void paintSegments(Graphics g) {
		Iterator<Element<Segment>> segmentsIterator = scenario.segmentsIterator();
		while (segmentsIterator.hasNext()) {
			Element<Segment> element = (Element<Segment>) segmentsIterator.next();
			g.setColor(element.getColor());
			paint(g, element.getElement());
		}
	}
	
	private void paintGraphs(Graphics g) {
		Iterator<Element<VertexTree>> graphsIterator = scenario.graphsIterator();
		while (graphsIterator.hasNext()) {
			Element<VertexTree> element = (Element<VertexTree>) graphsIterator.next();
			g.setColor(element.getColor());
			paint(g, element.getElement());
		}
	}
	
	private void paintPaths(Graphics g) {
		Iterator<Element<Path>> pathsIterator = scenario.pathsIterator();
		while (pathsIterator.hasNext()) {
			Element<Path> element = (Element<Path>) pathsIterator.next();
			g.setColor(element.getColor());
			paint(g, element.getElement());
		}
	}

	private void paintAreas(Graphics g) {
		Iterator<Element<Area>> areasIterator = scenario.areasIterator();
		while (areasIterator.hasNext()) {
			Element<Area> element = (Element<Area>) areasIterator.next();
			g.setColor(element.getColor());
			
			Area area = element.getElement();
			area.getBounds();
			paint(g, area.getBounds());
			
			Iterator<Polygon> holesIterator = area.holesIterator();
			g.setColor(element.getAdditionaColors()[0]);
			while (holesIterator.hasNext()) {
				Polygon hole = (Polygon) holesIterator.next();
				paint(g, hole, true);
			}
		}
	}
	
	private void paint(Graphics g, Path path) {
		paint(g, path.segmentIterator());
	}
	
	private void paint(Graphics g, Polygon p) {
		paint(g, p, false);
	}
	
	private void paint(Graphics g, VertexTree vt) {
		Iterator<Node<Vertex>> depthFirstIterator = vt.depthFirstIterator();
		while (depthFirstIterator.hasNext()) {
			DefaultNode<Vertex> node = (DefaultNode<Vertex>) depthFirstIterator.next();
			
			if (node.getParent() != null) {
				paint(g,new DefaultSegment(node.getElement(), node.getParent().getElement()));
			}
			
		}
	}
	
	private void paint(Graphics g, Polygon p, boolean fill) {
		java.awt.Polygon awtPolygon = createAwtPolygon(g, p);
		
		if (fill) {
			g.fillPolygon(awtPolygon);
		} else {
			g.drawPolygon(awtPolygon);
		}
		
		paint(g, p.segmentIterator());
	}
	
	private void paint(Graphics g, Iterator<Segment> segmentIterator) {
		while (segmentIterator.hasNext()) {
			Segment segment = (Segment) segmentIterator.next();
			paint(g, segment);
		}
	}
	
	private void paint(Graphics g, Segment p) {
		int x1 = (int)x(g, p.getV1().getX());
		int y1 = (int)y(g, p.getV1().getY());
		int x2 = (int)x(g, p.getV2().getX());
		int y2 = (int)y(g, p.getV2().getY());
		
		g.drawLine(x1, y1, x2, y2);
		paint(g, p.getV1());
		paint(g, p.getV2());
	}

	private void paint(Graphics g, Vertex v) {
		clipBounds = g.getClipBounds();
		
		final int radious = 4;
		
		int x = (int)x(g, v.getX()) - radious/2;
		int y = (int)y(g, v.getY()) - radious/2;
		
		g.fillOval(x, y, 4, 4);
		
		Color color = g.getColor();
		g.setColor(color.darker());
		g.drawString(v.toString(), x, y);
		g.setColor(color);
	}
	
	private double x(Graphics g, double x) {
		double dx = x - scenario.getClipArea().getX1();
		double relativePos = (dx/clipW) * (g.getClipBounds().width-PADDING) + PADDING/2;
		
		return relativePos;
	}
	
	private java.awt.Polygon createAwtPolygon(Graphics g, Polygon p) {
		java.awt.Polygon awtPolygon = new java.awt.Polygon();
		
		Iterator<Vertex> verticesIterator = p.verticesIterator();
		while (verticesIterator.hasNext()) {
			Vertex v = (Vertex) verticesIterator.next();
			awtPolygon.addPoint((int)x(g, v.getX()), (int)y(g, v.getY()));
		}
		
		return awtPolygon;
	}
	
	private double y(Graphics g, double y) {
		double dy = y - scenario.getClipArea().getY1();
		double relativePos = (dy/clipH) * (g.getClipBounds().height-PADDING) + PADDING/2;
		
		return g.getClipBounds().height - relativePos;
	}

	private class MyMouseListener implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			int x = e.getX();
			int y = e.getY();
			
			double relativeX = (x - PADDING/2) / (clipBounds.getWidth() - PADDING);
			double relativeY = (y - PADDING/2) / (clipBounds.getHeight() - PADDING);
			
			ClipArea clipArea = scenario.getClipArea();
			double areaW = clipArea.getX2() - clipArea.getX1();
			double areaH = clipArea.getY2() - clipArea.getY1();
			
			double newX = relativeX * areaW + clipArea.getX1();
			double newY = clipArea.getY2() - relativeY * areaH;
			
			
			Vertex v = new DefaultVertex(newX, newY);
			
			scenario.add(v, Color.RED);
			ScenarioPanel.this.revalidate();
			ScenarioPanel.this.repaint();
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
}
