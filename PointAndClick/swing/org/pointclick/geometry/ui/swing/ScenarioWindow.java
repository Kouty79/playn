package org.pointclick.geometry.ui.swing;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import org.pointclick.geometry.scenario.Scenario;

public class ScenarioWindow extends JFrame {

	private static final long serialVersionUID = -602925503364929891L;

	public ScenarioWindow(Scenario scenario) {
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(new ScenarioPanel(scenario), BorderLayout.CENTER);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(800, 600);
		this.setTitle(scenario.getTitle());
	}
	
}
