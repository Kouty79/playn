package org.pointclick.geometry.intersection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.pointclick.geometry.SimpleArea;
import org.pointclick.geometry.Vertex;
import org.pointclick.geometry.impl.DefaultVertex;
import org.pointclick.geometry.utils.AreaUtils;

public class AreaUtilsReachableTest {

	private AreaUtils areaUtils;
	
	@Before
	public void setUp() throws Exception {
		areaUtils = new AreaUtils(new SimpleArea());
	}
	
//	canReach tests -------------------------------------------------
	@Test
	public void testCanReach() {
		Vertex v1 = new DefaultVertex(-8, 8);
		Vertex v2 = new DefaultVertex(+8, 8);
		Assert.assertTrue("The two vertex are reachable through a straight line.", areaUtils.canReach(v1, v2));
	}
	
	@Test
	public void testCannotReach() {
		Vertex v1 = new DefaultVertex(-8, 8);
		Vertex v2 = new DefaultVertex(+8, -8);
		Assert.assertFalse("Cannot go directly from v1 to v2!", areaUtils.canReach(v1, v2));
	}
	
	@Test
	public void testCanReachPathThroughEdges() {
		Vertex v1 = new DefaultVertex(-5, 5);
		Vertex v2 = new DefaultVertex(+5, 5);
		Assert.assertTrue("The two vertex are reachable through a straight line.", areaUtils.canReach(v1, v2));
		
		Vertex v3 = new DefaultVertex(+5, 5);
		Vertex v4 = new DefaultVertex(+5, -5);
		Assert.assertTrue("The two vertex are reachable through a straight line.", areaUtils.canReach(v3, v4));
	}
	
	@Test
	public void testCanReachPathThroughVertex() {
		Vertex v1 = new DefaultVertex(-5, 6);
		Vertex v2 = new DefaultVertex(+5, 5);
		Assert.assertTrue("The two vertex are reachable through a straight line.", areaUtils.canReach(v1, v2));
		
		Vertex v3 = new DefaultVertex(+5, 5);
		Vertex v4 = new DefaultVertex(+6, -5);
		Assert.assertTrue("The two vertex are reachable through a straight line.", areaUtils.canReach(v3, v4));
	}
	
}
