package org.pointclick.geometry.impl;

import org.junit.Assert;
import org.junit.Test;

public class PolygonBuilderTest {

	@Test
	public void test() {
		double x1 = 10, x2 = 1;
		double y1 = 8, y2 = 2;
		
		DefaultPolygon p1 = new PolygonBuilder()
			.addVertex(x1, y1)
			.addVertex(x2, y2)
			.build();
		
		DefaultPolygon p2 = new PolygonBuilder()
				.addVertex(x1, y1)
				.addVertex(x2, y2)
				.build();
		
		Assert.assertEquals(p1, p2);
	}

}
