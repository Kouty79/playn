package org.pointclick.geometry.graph;

import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;
import org.pointclick.geometry.graph.impl.DefaultGraph;


public class TreeTest {

	@Test
	public void testBreadthIterator() {
		DefaultGraph<Object> graph = createSimpleGraph();
		
		Iterator<Node<Object>> breadthFirstIterator = graph.breadthFirstIterator();
		
//		Root level
		Assert.assertEquals(graph.getRoot(), breadthFirstIterator.next());
		
//		Level 1
		Assert.assertEquals(graph.getRoot().getChild(0), breadthFirstIterator.next());
		Assert.assertEquals(graph.getRoot().getChild(1), breadthFirstIterator.next());
		
//		Level 2
		Assert.assertEquals(graph.getRoot().getChild(0).getChild(0), breadthFirstIterator.next());
		Assert.assertEquals(graph.getRoot().getChild(0).getChild(1), breadthFirstIterator.next());
		Assert.assertEquals(graph.getRoot().getChild(1).getChild(0), breadthFirstIterator.next());
		
//		Level 3
		Assert.assertEquals(graph.getRoot().getChild(0).getChild(0).getChild(0), breadthFirstIterator.next());
		
		Assert.assertFalse("No more nodes!", breadthFirstIterator.hasNext());
	}
	
	@Test
	public void testDepthIterator() {
		DefaultGraph<Object> graph = createSimpleGraph();
		
		Iterator<Node<Object>> depthFirstIterator = graph.depthFirstIterator();
		
//		Root
		Assert.assertEquals(graph.getRoot(), depthFirstIterator.next());
		
//		Branch 1
		Assert.assertEquals(graph.getRoot().getChild(0), depthFirstIterator.next());
		Assert.assertEquals(graph.getRoot().getChild(0).getChild(0), depthFirstIterator.next());
		Assert.assertEquals(graph.getRoot().getChild(0).getChild(0).getChild(0), depthFirstIterator.next());
		
//		Branch 2
		Assert.assertEquals(graph.getRoot().getChild(0).getChild(1), depthFirstIterator.next());
		
//		Branch 3
		Assert.assertEquals(graph.getRoot().getChild(1), depthFirstIterator.next());
		Assert.assertEquals(graph.getRoot().getChild(1).getChild(0), depthFirstIterator.next());
		
		Assert.assertFalse("No more nodes!", depthFirstIterator.hasNext());
	}
	
	@Test
	public void testDepthIteratorRemove() {
		DefaultGraph<Object> graph = createSimpleGraph();
		
		Iterator<Node<Object>> depthFirstIterator = graph.depthFirstIterator();
		while (depthFirstIterator.hasNext()) {
			depthFirstIterator.next();
			depthFirstIterator.remove();
		}
		
		Assert.assertFalse(graph.getRoot().hasChildren());
	}
	
	private DefaultGraph<Object> createSimpleGraph() {
//		Root level
		Object root = new Object();
		DefaultGraph<Object> graph = new DefaultGraph<Object>(root);
		
//		Level 1
		Object node1 = new Object();
		Object node2 = new Object();
		
//		Level 2
		Object node11 = new Object();
		Object node12 = new Object();
		Object node21 = new Object();
		
//		Level 3
		Object node111 = new Object();
		
//		Adding nodes
		graph.getRoot().addChild(node1, 1);
		graph.getRoot().addChild(node2, 1);
		
		graph.getRoot().getChild(0).addChild(node11, 2);
		graph.getRoot().getChild(0).addChild(node12, 2);
		graph.getRoot().getChild(1).addChild(node21, 2);
		
		graph.getRoot().getChild(0).getChild(0).addChild(node111, 3);
		
		return graph;
	}
	
}
