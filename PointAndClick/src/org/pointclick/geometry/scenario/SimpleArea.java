package org.pointclick.geometry.scenario;

import java.util.Iterator;

import org.pointclick.geometry.Area;
import org.pointclick.geometry.Polygon;
import org.pointclick.geometry.impl.AreaBuilder;
import org.pointclick.geometry.impl.DefaultArea;

public class SimpleArea implements Area {

	private Area wrapped;
	
	public SimpleArea() {
		wrapped = createSimpleArea();
	}
	
	@Override
	public Polygon getBounds() {
		return wrapped.getBounds();
	}

	@Override
	public Iterator<Polygon> holesIterator() {
		return wrapped.holesIterator();
	}
	
	private Area createSimpleArea() {
		DefaultArea area = new AreaBuilder()
			.setBounds()
				.addVertex(-10, -10)
				.addVertex(-10, +10)
				.addVertex(+10, +10)
				.addVertex(+10, -10)
			.done()
			.addHole()
				.addVertex(-5, -5)
				.addVertex(-5, +5)
				.addVertex(+5, +5)
				.addVertex(+5, -5)
				.done()
			.buildArea();
		
		return area;
	}

}
