package org.pointclick.geometry;

import java.util.Iterator;

public interface Path {
	
	/**
	 * Iterator over the vertices of the path.
	 */
	public Iterator<Vertex> verticesIterator();
	
	/**
	 * Iterator on segments of the path
	 * @return
	 */
	public Iterator<Segment> segmentIterator();
	
	public double length();
}
