package org.pointclick.geometry.utils;

import java.util.Iterator;

import org.pointclick.geometry.Polygon;
import org.pointclick.geometry.Segment;
import org.pointclick.geometry.Vertex;
import org.pointclick.geometry.impl.DefaultVertex;

public class GeometryUtils {

	public double distance(Vertex v1, Vertex v2) {
		double dx = v1.getX() - v2.getX();
		double dy = v1.getY() - v2.getY();
		
		return Math.sqrt(dx * dx + dy * dy);
	}
	
	
	/**
	 * Let the point be C (Cx,Cy) and the line be AB (Ax,Ay) to (Bx,By). The
	 * length of the line segment AB is L: <br/>
	 * L= sqrt( (Bx-Ax)^2 + (By-Ay)^2 ) . <br/>
	 * Let P be the point of perpendicular projection of C onto AB. Let r be a
	 * parameter to indicate P's location along the line containing AB, with the
	 * following meaning:
	 * <br/>
	 * <ul>
	 * <li>r=0 P = A</li>
	 * <li>r=1 P = B</li>
	 * <li>r<0 P is on the backward extension of AB</li> 
	 * <li>r>1 P is on the forward extension of AB</li> 
	 * <li>0<r<1 P is interior to AB</li>
	 * </ul>
	 * Compute r with
	 * this:
	 * <br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Ay-Cy)(Ay-By)-(Ax-Cx)(Bx-Ax)
	 * <br/>r = ----------------------------- 
	 * <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L^2
	 * <br/>
	 * The point P can then be found:
	 * <br/>
	 * <ul>
	 * <li>Px = Ax + r(Bx-Ax)</li>
	 * <li>Py = Ay + r(By-Ay)</li>
	 * </ul> And the distance from A to P = r*L.
	 * 
	 * @see <a href=
	 *      'http://www.exaflop.org/docs/cgafaq/cga1.html'>Comp.Graphics.Algorithms</a
	 *      >
	 * @param v vertex
	 * @param segment segment
	 * @return I
	 */
	public PointSegmentDistance pointFromSegment(Vertex v, Segment segment) {
		double ay = segment.getV1().getY();
		double ax = segment.getV1().getX();
		double by = segment.getV2().getY();
		double bx = segment.getV2().getX();
		double cx = v.getX();
		double cy = v.getY();
		
		double l = distance(segment.getV1(), segment.getV2());
		double r = ((ay - cy) * (ay - by) - (ax - cx) * (bx - ax)) / (l * l);
		
		Vertex p = null;
		double px = ax + r*(bx - ax);
		double py = ay + r*(by - ay);
		
//		double distance = r * l;
		
		p = new DefaultVertex(px, py);
		double s = ((ay - cy) * (bx - ax) - (ax - cx) * (by - ay)) / (l * l);
		double distance = s * l;
		
		return new PointSegmentDistance(distance, p, r);
	}

	public Vertex nearestPoint(Polygon p, Vertex v) {
		Iterator<Segment> segmentIterator = p.segmentIterator();
		
		double minDist = Double.MAX_VALUE;
		Vertex nearestPolyPoint = null;
		while (segmentIterator.hasNext()) {
			Segment segment = (Segment) segmentIterator.next();
			
			PointSegmentDistance pointFromSegment = pointFromSegment(v, segment);
			double r = pointFromSegment.getpLocation();
			double distance;
			if(r < 0) {
				distance = distance(segment.getV1(), v);
			} else if (r > 1) {
				distance = distance(segment.getV2(), v);
			} else {
				distance = pointFromSegment.getDistance();
			}
			
			if (distance < minDist) {
				if(r < 0) {
					nearestPolyPoint = segment.getV1();
				} else if (r > 1) {
					nearestPolyPoint = segment.getV2();
				} else {
					nearestPolyPoint = pointFromSegment.getProjection();
				}
				minDist = distance;
			}
			
		}
		
		return nearestPolyPoint;
	}
	
}
