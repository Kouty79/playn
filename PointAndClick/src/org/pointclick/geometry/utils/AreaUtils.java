package org.pointclick.geometry.utils;

import java.util.Iterator;

import org.pointclick.geometry.Area;
import org.pointclick.geometry.Polygon;
import org.pointclick.geometry.Vertex;
import org.pointclick.geometry.intersection.GeometryIntersection;
import org.pointclick.geometry.intersection.GeometryIntersection.PolygonIntersection;

public class AreaUtils {

	private GeometryIntersection intersectUtil;
	
	private Area area;
	
	public AreaUtils(Area area) {
		this.area = area;
	}
	
	public boolean canReach(Vertex v1, Vertex v2) {
		Iterator<Polygon> holesIterator = getArea().holesIterator();
		
//		Check for holes intersection
		while (holesIterator.hasNext()) {
			Polygon polygon = (Polygon) holesIterator.next();
			if(intersectHole(getIntersectUtil().intersect(v1, v2, polygon))) {
				return false;
			}
		}
		
//		Check for border intersection
		if(intersectBorder(getIntersectUtil().intersect(v1, v2, getArea().getBounds()))) {
			return false;
		}
		
		return true;
	}

	public Area getArea() {
		return area;
	}
	
	private boolean intersectHole(PolygonIntersection intersection) {
		return 
			PolygonIntersection.INTERSECT == intersection ||
			PolygonIntersection.INSIDE == intersection;
	}
	
	private boolean intersectBorder(PolygonIntersection intersection) {
		return 
			PolygonIntersection.INTERSECT == intersection ||
			PolygonIntersection.OUTSIDE == intersection;
	}

	protected GeometryIntersection getIntersectUtil() {
		if (intersectUtil == null) {
			intersectUtil = new GeometryIntersection();
		}
		return intersectUtil;
	}
	
}
