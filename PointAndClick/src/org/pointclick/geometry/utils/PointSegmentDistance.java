package org.pointclick.geometry.utils;

import org.pointclick.geometry.Vertex;

public final class PointSegmentDistance {

	private double distance;
	private Vertex p;
	private double pLocation;
	
	PointSegmentDistance(double distance, Vertex p, double pLocation) {
		this.distance = distance;
		this.p = p;
		this.pLocation = pLocation;
	}

	public double getDistance() {
		return distance;
	}

	public Vertex getProjection() {
		return p;
	}

	public double getpLocation() {
		return pLocation;
	}
	
	public boolean isInSegment() {
		return pLocation >= 0 && pLocation <= 1;
	}
	
}
