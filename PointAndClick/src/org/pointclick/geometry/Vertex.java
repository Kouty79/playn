package org.pointclick.geometry;

public interface Vertex {

	double getX();
	double getY();
	
}
