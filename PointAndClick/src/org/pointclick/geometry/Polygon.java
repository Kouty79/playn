package org.pointclick.geometry;

import java.util.Iterator;

public interface Polygon {

	/**
	 * Iterator over the vertices of the polygon.
	 */
	public Iterator<Vertex> verticesIterator();
	
	/**
	 * Iterator on segments of the polygon
	 * @return
	 */
	public Iterator<Segment> segmentIterator();
}
