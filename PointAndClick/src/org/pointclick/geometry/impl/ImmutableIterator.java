package org.pointclick.geometry.impl;

import java.util.Iterator;

class ImmutableIterator<E> implements Iterator<E> {

	private Iterator<E> original;

	public ImmutableIterator(Iterator<E> original) {
		this.original = original;
	}
	
	@Override
	public boolean hasNext() {
		return original.hasNext();
	}

	@Override
	public E next() {
		return original.next();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("remove() is not supported");
	}

}
