package org.pointclick.geometry.impl;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.pointclick.geometry.Area;
import org.pointclick.geometry.Polygon;

public final class DefaultArea implements Area {

	private Polygon bounds;
	private List<Polygon> holes;
	
	public DefaultArea(Polygon bounds) {
		this(bounds, null);
	}
	
	public DefaultArea(Polygon bounds, Collection<? extends Polygon> holes) {
		this.bounds = bounds;
		this.holes = new ArrayList<Polygon>((holes == null) ? new ArrayList<Polygon>() : holes);
	}
	
	@Override
	public Polygon getBounds() {
		return bounds;
	}

	@Override
	public Iterator<Polygon> holesIterator() {
		return new ImmutableIterator<Polygon>(holes.iterator());
	}
	
}
