package org.pointclick.geometry.graph.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.pointclick.geometry.graph.Node;

public class DefaultNode<T> implements Node<T> {

	private double distance;
	private T element;
	private List<DefaultNode<T>> children;
	private DefaultNode<T> parent;
	
	protected DefaultNode(T element, double distance, DefaultNode<T> parent) {
		this.distance = distance;
		this.element = element;
		this.parent = parent;
	}
	
	public double getDistance() {
		return distance;
	}
	
	public T getElement() {
		return element;
	}
	
	public boolean hasChildren() {
		return !getChildren().isEmpty();
	}
	
	public int childrenSize() {
		return getChildren().size();
	}
	
	public DefaultNode<T> getChild(int index) {
		return getChildren().get(index);
	}
	
	public Iterator<DefaultNode<T>> getChildrenIterator() {
		return getChildren().iterator();
	}
	
	public DefaultNode<T> addChild(T child, double distance) {
		DefaultNode<T> node = createChild(child, distance);
		getChildren().add(node);
		
		return node;
	}
	
	protected DefaultNode<T> createChild(T elem, double distance) {
		return new DefaultNode<T>(elem, distance, this);
	}
	
	public DefaultNode<T> getParent() {
		return parent;
	}
	
	protected List<DefaultNode<T>> getChildren() {
		if (children == null) {
			children = new ArrayList<DefaultNode<T>>();
		}
		
		return children;
	}
	
	@Override
	public String toString() {
		return "[" + getElement() + ", d: " + getDistance() + "]";
	}
}
