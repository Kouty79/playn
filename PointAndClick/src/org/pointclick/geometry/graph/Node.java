package org.pointclick.geometry.graph;

import java.util.Iterator;

public interface Node<T> {

	public double getDistance();
	
	public T getElement();
	
	public boolean hasChildren();
	
	public int childrenSize();
	
	public Node<T> getChild(int index);
	
	public Iterator<? extends Node<T>> getChildrenIterator();
	
	public Node<T> addChild(T elem, double distance);
	
	public Node<T> getParent();
}
