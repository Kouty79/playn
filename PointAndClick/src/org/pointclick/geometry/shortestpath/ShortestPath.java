package org.pointclick.geometry.shortestpath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.pointclick.geometry.Area;
import org.pointclick.geometry.Path;
import org.pointclick.geometry.Polygon;
import org.pointclick.geometry.Vertex;
import org.pointclick.geometry.graph.Node;
import org.pointclick.geometry.impl.DefaultPath;
import org.pointclick.geometry.utils.AreaUtils;
import org.pointclick.geometry.utils.GeometryUtils;

public class ShortestPath {

	private GeometryUtils vertexUtils;
	private AreaUtils areaUtils;
	
	private Area area;
	private VertexTree tree;
	private Path shortestPath;
	
	public ShortestPath(Area area) {
		this.area = area;
	}
	
	public Area getArea() {
		return area;
	}
	
	public Path shortestPath(Vertex start, Vertex end) {
		List<Vertex> verticesToEvaluate = mergeAllVertex(getArea(), end);
		
		VertexTree tree = new VertexTree(start);
		Path shortestPath = null;
		
		boolean noMoreReachableNodes;
		do {
			double minDist = Double.MAX_VALUE;
			Vertex nearestVertex = null;
			Node<Vertex> nearestTreeNode = null;
			noMoreReachableNodes = true;
			
			for (Node<Vertex> treeNode : tree) {
				for (Vertex vertex : verticesToEvaluate) {
					if (canReach(vertex, treeNode.getElement())) {
						noMoreReachableNodes = false;
						
						double distance = distance(vertex, treeNode.getElement()) + treeNode.getDistance();
						if (distance < minDist) {
							minDist = distance;
							nearestVertex = vertex;
							nearestTreeNode = treeNode;
						}
					}
				}
			} 
			
			if (nearestVertex != null) {
				verticesToEvaluate.remove(nearestVertex);
				Node<Vertex> newChild = nearestTreeNode.addChild(nearestVertex, minDist);
				
				if (nearestVertex.equals(end)) {
					shortestPath = createPath(newChild);
					break;
				}
			}
			
		} while(!verticesToEvaluate.isEmpty() && !noMoreReachableNodes);
		
		this.tree = tree;
		this.shortestPath = shortestPath;
		
		return shortestPath;
	}
	
	public VertexTree getTree() {
		return tree;
	}

	public Path getShortestPath() {
		return shortestPath;
	}

	private boolean canReach(Vertex v1, Vertex v2) {
		return getAreaUtils().canReach(v1, v2);
	}
	
	private double distance(Vertex v1, Vertex v2) {
		return getVertexUtils().distance(v1, v2);
	}
	
	private Path createPath(Node<Vertex> node) {
		List<Vertex> path = new ArrayList<Vertex>();
		path.add(node.getElement());
		double length = node.getDistance();
		
		while(node.getParent() != null) {
			path.add(node.getParent().getElement());
			node = node.getParent();
		}
		
		Collections.reverse(path);
		return new DefaultPath(path, length);
	}

	private List<Vertex> mergeAllVertex(Area area, Vertex end) {
		List<Vertex> vertices = new ArrayList<Vertex>();
		
		vertices.add(end);
		
		Iterator<Polygon> holesIterator = area.holesIterator();
		while (holesIterator.hasNext()) {
			Polygon polygon = (Polygon) holesIterator.next();
			addVertices(vertices, polygon);
		}
		
		addVertices(vertices, area.getBounds());
		
		return vertices;
	}
	
	private void addVertices(List<Vertex> vertexList, Polygon polygon) {
		Iterator<Vertex> verticesIterator = polygon.verticesIterator();
		while (verticesIterator.hasNext()) {
			Vertex vertex = (Vertex) verticesIterator.next();
			vertexList.add(vertex);
		}
	}
	
	protected GeometryUtils getVertexUtils() {
		if (vertexUtils == null) {
			vertexUtils = new GeometryUtils();
		}
		return vertexUtils;
	}

	protected AreaUtils getAreaUtils() {
		if (areaUtils == null) {
			areaUtils = new AreaUtils(getArea());
		}
		
		return areaUtils;
	}
	
}
