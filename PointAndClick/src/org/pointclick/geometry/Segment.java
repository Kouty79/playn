package org.pointclick.geometry;

public interface Segment {

	Vertex getV1();
	Vertex getV2();
	
}
