package org.pointandclick.fooliginosus.java;

import java.awt.Dimension;
import java.awt.Toolkit;

import org.pointandclick.fooliginosus.core.Fooliginosus;

import playn.core.PlayN;
import playn.java.JavaPlatform;

public class FooliginosusJava {

	public static void main(String[] args) {
		JavaPlatform platform = JavaPlatform.register();
		platform.assets().setPathPrefix("org/pointandclick/fooliginosus/resources");

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		platform.graphics().setSize((int) screenSize.getWidth(), (int) screenSize.getHeight());
		// platform.graphics().setSize(800 / 1, 480 / 1);
		PlayN.run(new Fooliginosus());
	}
}
