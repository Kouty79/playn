package org.pointandclick.fooliginosus.html;

import playn.core.PlayN;
import playn.html.HtmlGame;
import playn.html.HtmlPlatform;

import org.pointandclick.fooliginosus.core.Fooliginosus;

public class FooliginosusHtml extends HtmlGame {

  @Override
  public void start() {
    HtmlPlatform platform = HtmlPlatform.register();
    platform.assets().setPathPrefix("fooliginosus/");
    PlayN.run(new Fooliginosus());
  }
}
