package org.pointandclick.fooliginosus.framework.game;

public interface ImageResource extends Resource {

	public int getWidth();

	public int getHeight();

}
