package org.pointandclick.fooliginosus.core;

import org.pointandclick.fooliginosus.framework.playn.PointAndClick;
import org.pointandclick.fooliginosus.framework.playn.pointer.PointerManager;

import playn.core.Game;

public class Fooliginosus implements Game {

	PointAndClick game;

	@Override
	public void init() {
		PointerManager.get().register();

		game = new PointAndClick(new FooliginosusSettings());
		game.addRoom(new BedRoom());
		game.setCharacter(new Foolboy());

		game.register();
		game.enterRoom(BedRoom.ID, 0);
	}

	@Override
	public void paint(float alpha) {
		game.paint(alpha);
	}

	@Override
	public void update(float delta) {
		game.update(delta);
	}

	@Override
	public int updateRate() {
		return 25;
	}

}
