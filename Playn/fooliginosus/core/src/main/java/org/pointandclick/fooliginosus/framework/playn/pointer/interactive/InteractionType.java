package org.pointandclick.fooliginosus.framework.playn.pointer.interactive;

public enum InteractionType {

	START, END, DRAG;

}
