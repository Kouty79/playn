package org.pointandclick.fooliginosus.framework.playn.pointer;

import static playn.core.PlayN.graphics;
import static playn.core.PlayN.mouse;
import static playn.core.PlayN.pointer;

import java.util.Collection;
import java.util.HashSet;
import java.util.SortedSet;
import java.util.TreeSet;

import org.pointandclick.fooliginosus.framework.playn.pointer.interactive.InteractionType;
import org.pointandclick.fooliginosus.framework.playn.pointer.interactive.Interactive;
import org.pointandclick.fooliginosus.framework.playn.pointer.mouselistener.MouseEventType;
import org.pointandclick.fooliginosus.framework.playn.pointer.mouselistener.MouseListener;

import playn.core.GroupLayer;
import playn.core.Mouse;
import playn.core.Mouse.ButtonEvent;
import playn.core.Mouse.MotionEvent;
import playn.core.Mouse.WheelEvent;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import pythagoras.f.Point;

public class PointerManager {

	private static final PointerManager INSTANCE = new PointerManager();

	private SortedSet<Interactive> interactives;
	private SortedSet<MouseListener> mouseListeners;

	private Interactive draggingElement;
	private Collection<MouseListener> overLayers;

	private PointerManager() {
	}

	public static PointerManager get() {
		return INSTANCE;
	}

	public void register() {
		pointer().setListener(new Pointer.Listener() {

			@Override
			public void onPointerStart(Event event) {
				notifyEvent(event.x(), event.y(), graphics().rootLayer(), InteractionType.START);
			}

			@Override
			public void onPointerEnd(Event event) {
				notifyEvent(event.x(), event.y(), graphics().rootLayer(), InteractionType.END);

			}

			@Override
			public void onPointerDrag(Event event) {
				notifyEvent(event.x(), event.y(), graphics().rootLayer(), InteractionType.DRAG);
			}

		});

		mouse().setListener(new Mouse.Listener() {

			@Override
			public void onMouseWheelScroll(WheelEvent event) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMouseUp(ButtonEvent event) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMouseMove(MotionEvent event) {
				notifyMotionEvent(event.x(), event.y(), graphics().rootLayer());
			}

			@Override
			public void onMouseDown(ButtonEvent event) {
				// TODO Auto-generated method stub

			}
		});
	}

	private SortedSet<Interactive> getInteractives() {
		if (interactives == null) {
			interactives = new TreeSet<Interactive>(new ZOrderComparator());
		}

		return interactives;
	}

	public boolean addInteractive(Interactive interactive) {
		boolean result = getInteractives().add(interactive);
		return result;
	}

	public boolean removeInteractive(Interactive interactive) {
		boolean result = getInteractives().remove(interactive);
		return result;
	}

	public boolean addMouseListener(MouseListener listener) {
		boolean result = getMouseListeners().add(listener);
		return result;
	}

	public boolean removeMouseListener(MouseListener listener) {
		boolean result = getMouseListeners().remove(listener);
		return result;
	}

	private void notifyEvent(float x, float y, GroupLayer groupLayer, InteractionType interactionType) {
		boolean behind = false;

		for (Interactive interactive : getInteractives()) {
			if (interactive.hitTest(x, y)) {

				if (InteractionType.START == interactionType) {
					if (interactive.interactWithOthers()) {
						draggingElement = interactive;
					}
				}

				Point point = interactive.screenToLayer(x, y);
				if (point == null) {
					point = new Point(x, y);
				}

				interactive.onEvent(new PointerInteractionEvent(point, interactionType, interactive.equals(draggingElement) ? null : draggingElement, behind));

				if (InteractionType.END == interactionType) {
					if (interactive.interactWithOthers()) {
						draggingElement = null;
					}
				}

				behind = true;
			}
		}
	}

	private void notifyMotionEvent(float x, float y, GroupLayer groupLayer) {
		for (MouseListener listener : getMouseListeners()) {
			Point point = listener.screenToLayer(x, y);
			if (point == null) {
				point = new Point(x, y);
			}

			if (listener.hitTest(x, y)) {

				if (!getMouseOverLayers().contains(listener)) {
					listener.onMouseEvent(new PointerMouseEvent(point, (getMouseOverLayers().contains(listener)) ? MouseEventType.MOVE : MouseEventType.ENTER));
					getMouseOverLayers().add(listener);
				}

			} else {

				if (getMouseOverLayers().contains(listener)) {
					listener.onMouseEvent(new PointerMouseEvent(point, MouseEventType.LEAVE));
					getMouseOverLayers().remove(listener);
				}

			}
		}
	}

	private Collection<MouseListener> getMouseOverLayers() {
		if (overLayers == null) {
			overLayers = new HashSet<MouseListener>();
		}

		return overLayers;
	}

	private SortedSet<MouseListener> getMouseListeners() {
		if (mouseListeners == null) {
			mouseListeners = new TreeSet<MouseListener>(new ZOrderComparator());
		}

		return mouseListeners;
	}

}
