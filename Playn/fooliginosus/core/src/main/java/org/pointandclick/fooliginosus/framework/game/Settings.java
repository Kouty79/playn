package org.pointandclick.fooliginosus.framework.game;

public interface Settings {

	public String imageBaseUrl();

	public float zoom();

	public boolean debug();

}
