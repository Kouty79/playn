package org.pointandclick.fooliginosus.framework.geometry.impl;

import java.util.ArrayList;
import java.util.List;

import org.pointandclick.fooliginosus.framework.geometry.Vertex;

public class PolygonBuilder {

	private List<Vertex> vertices;

	public PolygonBuilder() {
		vertices = new ArrayList<Vertex>();
	}

	public PolygonBuilder addVertex(double x, double y) {
		return addVertex(new DefaultVertex(x, y));
	}

	public PolygonBuilder addVertex(Vertex v) {
		vertices.add(v);
		return this;
	}

	public DefaultPolygon build() {
		return new DefaultPolygon(vertices);
	}

}
