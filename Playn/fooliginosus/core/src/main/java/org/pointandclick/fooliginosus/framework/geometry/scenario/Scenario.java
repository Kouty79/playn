package org.pointandclick.fooliginosus.framework.geometry.scenario;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.pointandclick.fooliginosus.framework.geometry.Area;
import org.pointandclick.fooliginosus.framework.geometry.Path;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;
import org.pointandclick.fooliginosus.framework.geometry.shortestpath.VertexTree;

public class Scenario {

	private List<Element<Area>> areas;
	private List<Element<Vertex>> vertices;
	private List<Element<Path>> paths;
	private List<Element<VertexTree>> graphs;
	private ClipArea clipArea;
	private String title;

	public Scenario() {
		clipArea = new ClipArea();
	}

	public Scenario(String title) {
		this();
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Scenario add(Area area, Color bounds, Color holes) {
		getAreas().add(new Element<Area>(area, bounds, holes));
		updateClipArea(area);
		return this;
	}

	public Scenario add(Vertex point, Color color) {
		getVertices().add(new Element<Vertex>(point, color));
		updateClipArea(point);
		return this;
	}

	public Scenario add(Path path, Color color) {
		getPaths().add(new Element<Path>(path, color));
		updateClipArea(path);
		return this;
	}

	public Scenario add(VertexTree tree, Color color) {
		getGraphs().add(new Element<VertexTree>(tree, color));
		// updateClipArea(path);
		return this;
	}

	public ClipArea getClipArea() {
		return clipArea;
	}

	public Iterator<Element<Area>> areasIterator() {
		return getAreas().iterator();
	}

	public Iterator<Element<Vertex>> verticesIterator() {
		return getVertices().iterator();
	}

	public Iterator<Element<Path>> pathsIterator() {
		return getPaths().iterator();
	}

	public Iterator<Element<VertexTree>> graphsIterator() {
		return getGraphs().iterator();
	}

	private void updateClipArea(Area area) {
		updateClipArea(area.getBounds().verticesIterator());
	}

	private void updateClipArea(Path path) {
		updateClipArea(path.verticesIterator());
	}

	private void updateClipArea(Iterator<Vertex> verticesIterator) {
		while (verticesIterator.hasNext()) {
			Vertex vertex = (Vertex) verticesIterator.next();
			updateClipArea(vertex);
		}
	}

	private void updateClipArea(Vertex v) {
		clipArea.setX1(Math.min(clipArea.getX1(), v.getX()));
		clipArea.setY1(Math.min(clipArea.getY1(), v.getY()));
		clipArea.setX2(Math.max(clipArea.getX2(), v.getX()));
		clipArea.setY2(Math.max(clipArea.getY2(), v.getY()));
	}

	private List<Element<Area>> getAreas() {
		if (areas == null) {
			areas = new ArrayList<Element<Area>>();
		}
		return areas;
	}

	private List<Element<Vertex>> getVertices() {
		if (vertices == null) {
			vertices = new ArrayList<Element<Vertex>>();
		}
		return vertices;
	}

	private List<Element<Path>> getPaths() {
		if (paths == null) {
			paths = new ArrayList<Element<Path>>();
		}
		return paths;
	}

	private List<Element<VertexTree>> getGraphs() {
		if (graphs == null) {
			graphs = new ArrayList<Element<VertexTree>>();
		}
		return graphs;
	}

	public static class Element<T> {
		private T element;
		private Color color;
		private Color[] additionaColors;

		public Element(T element, Color color, Color... additionaColors) {
			this.element = element;
			this.color = color;
			this.additionaColors = additionaColors;
		}

		public T getElement() {
			return element;
		}

		public Color getColor() {
			return color;
		}

		public Color[] getAdditionaColors() {
			return additionaColors;
		}

	}

}
