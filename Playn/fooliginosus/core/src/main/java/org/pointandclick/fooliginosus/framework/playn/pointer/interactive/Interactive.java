package org.pointandclick.fooliginosus.framework.playn.pointer.interactive;

import org.pointandclick.fooliginosus.framework.playn.pointer.CanReceiveHits;
import org.pointandclick.fooliginosus.framework.playn.pointer.HasZOrder;

public interface Interactive extends HasZOrder, CanReceiveHits {

	@Override
	int getZOrder();

	/**
	 * Receive the event
	 * 
	 * @param event
	 */
	void onEvent(InteractionEvent event);

	/**
	 * 
	 * @return true, if this element can be dragged into another interactive.
	 */
	boolean interactWithOthers();
}
