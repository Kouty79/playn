package org.pointandclick.fooliginosus.framework.playn.layers;

public interface Scalable {

	void scale(float scaleX, float scaleY);

}
