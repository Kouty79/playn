package org.pointandclick.fooliginosus.core;

import org.pointandclick.fooliginosus.framework.game.ImageResource;
import org.pointandclick.fooliginosus.framework.game.Room;
import org.pointandclick.fooliginosus.framework.geometry.Area;
import org.pointandclick.fooliginosus.framework.geometry.impl.AreaBuilder;

public class BedRoom implements Room {

	public static final String ID = BedRoom.class.getName();

	@Override
	public String getRoomId() {
		return ID;
	}

	@Override
	public ImageResource getBackGroundImage() {
		return new ImageResource() {

			@Override
			public String getURI() {
				return "bedroom.jpg";
			}

			@Override
			public int getHeight() {
				return 960;
			}

			@Override
			public int getWidth() {
				return 1600;
			}

		};

		// return null;
	}

	@Override
	public Area getWalkableArea() {
		return new AreaBuilder().buildBounds().addVertex(0 * 2, 2 * 480).addVertex(0 * 2, 2 * 406).addVertex(165 * 2, 2 * 411).addVertex(244 * 2, 2 * 364)
				.addVertex(669 * 2, 2 * 364).addVertex(720 * 2, 2 * 388).addVertex(700 * 2, 2 * 424).addVertex(800 * 2, 2 * 480).done().addHole()
				.addVertex(300 * 2, 2 * 390).addVertex(270 * 2, 2 * 410).addVertex(530 * 2, 2 * 410).addVertex(500 * 2, 2 * 390).done().buildArea();
	}

}
