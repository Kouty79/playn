package org.pointandclick.fooliginosus.framework.geometry.scenario;

import java.util.Iterator;

import org.pointandclick.fooliginosus.framework.geometry.Area;
import org.pointandclick.fooliginosus.framework.geometry.Polygon;
import org.pointandclick.fooliginosus.framework.geometry.impl.AreaBuilder;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultArea;

public class SimpleArea implements Area {

	private Area wrapped;

	public SimpleArea() {
		wrapped = createSimpleArea();
	}

	@Override
	public Polygon getBounds() {
		return wrapped.getBounds();
	}

	@Override
	public Iterator<Polygon> holesIterator() {
		return wrapped.holesIterator();
	}

	private Area createSimpleArea() {
		DefaultArea area = new AreaBuilder().buildBounds().addVertex(-10, -10).addVertex(-10, +10).addVertex(+10, +10).addVertex(+10, -10).done().addHole()
				.addVertex(-5, -5).addVertex(-5, +5).addVertex(+5, +5).addVertex(+5, -5).done().buildArea();

		return area;
	}

}
