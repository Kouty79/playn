package org.pointandclick.fooliginosus.framework.geometry.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.pointandclick.fooliginosus.framework.geometry.Polygon;
import org.pointandclick.fooliginosus.framework.geometry.Segment;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;

public final class DefaultPolygon implements Polygon {

	private List<Vertex> vertices;

	public DefaultPolygon(Collection<? extends Vertex> vertices) {
		this.vertices = new ArrayList<Vertex>(vertices);
	}

	@Override
	public Iterator<Vertex> verticesIterator() {
		return new ImmutableIterator<Vertex>(vertices.iterator());
	}

	@Override
	public Iterator<Segment> segmentIterator() {
		return new SegmentIterator(verticesIterator());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vertices == null) ? 0 : vertices.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DefaultPolygon)) {
			return false;
		}
		DefaultPolygon other = (DefaultPolygon) obj;
		if (vertices == null) {
			if (other.vertices != null) {
				return false;
			}
		} else if (!vertices.equals(other.vertices)) {
			return false;
		}
		return true;
	}

	private class SegmentIterator implements Iterator<Segment> {

		private Iterator<Vertex> vertexIterator;
		private Vertex lastVertex;
		private Vertex firstVertex;

		public SegmentIterator(Iterator<Vertex> vertexIterator) {
			this.vertexIterator = vertexIterator;
			if (vertexIterator.hasNext()) {
				firstVertex = vertexIterator.next();
				lastVertex = firstVertex;
			}
		}

		@Override
		public boolean hasNext() {
			return lastVertex != null;
		}

		@Override
		public DefaultSegment next() {
			DefaultSegment segment = null;

			if (vertexIterator.hasNext()) {
				Vertex next = vertexIterator.next();
				segment = new DefaultSegment(lastVertex, next);
				lastVertex = next;
			} else if (lastVertex != null) {// The last segment joins last point
											// with first.
				segment = new DefaultSegment(lastVertex, firstVertex);
				lastVertex = null;
			}

			return segment;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Removing a segment is not supported.");
		}

	}
}
