package org.pointandclick.fooliginosus.framework.playn.layers.impl;

import static playn.core.PlayN.graphics;

import org.pointandclick.fooliginosus.framework.playn.graphics.CanvasGraphics;
import org.pointandclick.fooliginosus.framework.playn.layers.VectorLayer;

import playn.core.Canvas;
import playn.core.CanvasImage;
import playn.core.Image;
import playn.core.ImageLayer;

public abstract class AbstractVectorLayer extends AbstractScalableLayer implements VectorLayer {

	private CanvasImage image;
	private ImageLayer imageLayer;
	private CanvasGraphics canvasGraphics;

	protected abstract CanvasImage createImage();

	public AbstractVectorLayer(int zOrder) {
		super(zOrder);
	}

	@Override
	public final ImageLayer getLayer() {
		if (imageLayer == null) {
			imageLayer = createImageLayer(getImage());
		}

		return imageLayer;
	}

	@Override
	public CanvasImage getImage() {
		if (image == null) {
			image = createImage();
		}

		return image;
	}

	@Override
	public CanvasGraphics getVectorGraphics() {
		if (canvasGraphics == null) {
			canvasGraphics = new CanvasGraphics(getImage());
		}

		return canvasGraphics;
	}

	@Override
	protected void doScale() {
		getVectorGraphics().setScaleX(getScaleX());
		getVectorGraphics().setScaleY(getScaleY());
	}

	protected ImageLayer createImageLayer(Image image) {
		return graphics().createImageLayer(image);
	}

	protected Canvas getCanvas() {
		return getVectorGraphics().getCanvas();
	}

}
