package org.pointandclick.fooliginosus.framework.playn;

import org.pointandclick.fooliginosus.framework.playn.pointer.interactive.InteractionEvent;

public interface PointerListener {

	public void onEvent(InteractionEvent event);

}
