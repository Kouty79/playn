package org.pointandclick.fooliginosus.framework.playn.room;

import static playn.core.PlayN.log;

import org.pointandclick.fooliginosus.framework.game.Room;
import org.pointandclick.fooliginosus.framework.geometry.Area;
import org.pointandclick.fooliginosus.framework.geometry.Path;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;
import org.pointandclick.fooliginosus.framework.geometry.shortestpath.ShortestPath;
import org.pointandclick.fooliginosus.framework.geometry.utils.GeometryUtils;
import org.pointandclick.fooliginosus.framework.playn.GameElementFactory;
import org.pointandclick.fooliginosus.framework.playn.PointerListener;
import org.pointandclick.fooliginosus.framework.playn.graphics.VectorGraphics;
import org.pointandclick.fooliginosus.framework.playn.layers.PointAndClickLayer;
import org.pointandclick.fooliginosus.framework.playn.layers.impl.AreaLayer;
import org.pointandclick.fooliginosus.framework.playn.layers.impl.CharacterLayer;
import org.pointandclick.fooliginosus.framework.playn.pointer.interactive.InteractionEvent;
import org.pointandclick.fooliginosus.framework.playn.pointer.interactive.InteractionType;

import playn.core.GroupLayer;

/**
 * Point&Click room controller.
 * 
 * @author akoutifaris
 * 
 */
public class RoomPresenter implements PointerListener {

	private Room room;
	private RoomView roomView;
	private GameElementFactory roomViewFactory;
	private GroupLayer presenterLayer;
	private CharacterLayer characterLayer;
	private float scaleX;
	private float scaleY;

	public RoomPresenter(GroupLayer presenterLayer, GameElementFactory roomViewFactory, float scaleX, float scaleY) {
		this.presenterLayer = presenterLayer;
		this.roomViewFactory = roomViewFactory;

		this.scaleX = scaleX;
		this.scaleY = scaleY;
	}

	public void changeRoom(Room room) {
		if (getRoomView() != null) {
			getRoomView().detach(getPresenterLayer());
		}

		this.room = room;

		setRoomView(getRoomViewFactory().create(room));

		PointAndClickLayer walkableAreaLayer = new AreaLayer(getWalkableArea(), 10);
		getRoomView().getLayers().add(walkableAreaLayer);

		getRoomView().getLayers().add(getCharacterLayer());
		getRoomView().scale(getScaleX(), getScaleY());
		getRoomView().attach(getPresenterLayer());
	}

	public void setCharacter(CharacterLayer characterView) {
		this.characterLayer = characterView;
	}

	public CharacterLayer getCharacterLayer() {
		return characterLayer;
	}

	public RoomView getRoomView() {
		return roomView;
	}

	public void setRoomView(RoomView roomView) {
		this.roomView = roomView;
		roomView.setPointerListener(this);
	}

	public Room getRoom() {
		return room;
	}

	public GroupLayer getPresenterLayer() {
		return presenterLayer;
	}

	protected GameElementFactory getRoomViewFactory() {
		return roomViewFactory;
	}

	@Override
	public void onEvent(InteractionEvent event) {
		if (InteractionType.END == event.getInteraction().getType()) {
			log().debug("event coords: (" + event.x() + ", " + event.y() + ")");

			if (getCharacterLayer() != null) {
				float x = getCharacterLayer().getX();
				float y = getCharacterLayer().getY();

				ShortestPath shortestPath = new ShortestPath(getWalkableArea(), VectorGraphics.PRECISION);
				Vertex start = new DefaultVertex(x, y);

				log().debug("Character position: " + start);

				Vertex p = new DefaultVertex(event.x(), event.y());

				GeometryUtils geometryUtils = new GeometryUtils();
				Vertex end = geometryUtils.nearestPoint(getWalkableArea(), p);

				log().debug("end: " + end);

				Path path = shortestPath.shortestPath(start, end);
				if (path != null) {
					getCharacterLayer().followPath(path);
				}

			}
		}
	}

	public Area getWalkableArea() {
		return room.getWalkableArea();
	}

	public float getScaleX() {
		return scaleX;
	}

	public float getScaleY() {
		return scaleY;
	}

}
