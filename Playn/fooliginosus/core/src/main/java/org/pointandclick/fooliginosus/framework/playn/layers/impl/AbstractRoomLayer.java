package org.pointandclick.fooliginosus.framework.playn.layers.impl;

import org.pointandclick.fooliginosus.framework.playn.layers.PointAndClickLayer;

public abstract class AbstractRoomLayer implements PointAndClickLayer {

	private int zOrder;

	public AbstractRoomLayer(int zOrder) {
		this.zOrder = zOrder;
	}

	@Override
	public int getZOrder() {
		return zOrder;
	}

	public void setZOrder(int zOrder) {
		this.zOrder = zOrder;
	}

}
