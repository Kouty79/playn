package org.pointandclick.fooliginosus.framework.playn.layers.impl;

import java.util.Iterator;

import org.pointandclick.fooliginosus.framework.geometry.Path;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;
import org.pointandclick.fooliginosus.framework.playn.layers.PointAndClickLayer;

import playn.core.ImageLayer;

public class CharacterLayer extends AbstractRasterLayer implements PointAndClickLayer {

	private ImageLayer layer;
	private double speed = 0.4;
	private float lx, ly;
	private float x, y;
	private float toX, toY;
	private float vx, vy;
	private Iterator<Vertex> pathIt;
	boolean move;

	// long timeU;
	// int countU;
	// int countP;
	// private long timeP;

	public CharacterLayer(int zOrder, ImageLayer layer) {
		super(zOrder);
		this.layer = layer;
		// timeU = System.currentTimeMillis();
		// timeP = timeU;

		x = getLayer().transform().tx();
		y = getLayer().transform().ty();
		lx = x;
		ly = y;

		move = false;
	}

	@Override
	public ImageLayer getLayer() {
		return layer;
	}

	@Override
	public void paint(float alpha) {
		if (mustMove()) {
			move = false;
			float x = (lx * (1f - alpha)) + (this.x * alpha);
			float y = (ly * (1f - alpha)) + (this.y * alpha);
			setTranslation(x, y);
		}

		// countP++;
		// long dt = System.currentTimeMillis() - timeP;
		// if (dt > 10000) {
		// float pRate = (float)countP / dt * 1000;
		// countP = 0;
		// timeP = System.currentTimeMillis();
		// log().debug("P rate: " + pRate);
		// }
	}

	@Override
	public void update(float delta) {
		if (mustMove()) {
			float dxSignum = Math.signum(x - toX);
			float dySignum = Math.signum(y - toY);

			lx = x;
			ly = y;
			x += vx * delta;
			y += vy * delta;

			float newDxSignum = Math.signum(x - toX);
			float newDySignum = Math.signum(y - toY);

			if (newDxSignum != dxSignum || newDySignum != dySignum) {

				setTranslation(toX, toY);
				x = toX;
				y = toY;
				lx = x;
				ly = y;

				if (pathIt != null && pathIt.hasNext()) {
					Vertex next = pathIt.next();
					moveTo((float) next.getX(), (float) next.getY());
				} else {
					pathIt = null;
				}
			}

		}

		// countU++;
		// long dt = System.currentTimeMillis() - timeU;
		// if (dt > 10000) {
		// float uRate = (float)countU / dt * 1000;
		// countU = 0;
		// timeU = System.currentTimeMillis();
		// log().debug("U rate: " + uRate);
		// }
	}

	public void moveTo(float x, float y) {
		toX = x;
		toY = y;

		double atan = Math.atan2(y - this.y, x - this.x);
		vx = (float) (Math.cos(atan) * speed);
		vy = (float) (Math.sin(atan) * speed);
	}

	public void setPosition(float x, float y) {
		this.x = x;
		this.y = y;
		toX = x;
		toY = y;
		move = true;
	}

	public void followPath(Path path) {
		pathIt = path.verticesIterator();

		if (pathIt.hasNext()) {
			Vertex next = pathIt.next();
			moveTo((float) next.getX(), (float) next.getY());
		} else {
			pathIt = null;
		}
	}

	protected boolean mustMove() {
		return toX != x || toY != y || pathIt != null || move;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

}
