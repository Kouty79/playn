package org.pointandclick.fooliginosus.framework.geometry;

public interface Segment extends Has2Vertices {

	@Override
	Vertex getV1();

	@Override
	Vertex getV2();
}
