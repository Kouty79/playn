package org.pointandclick.fooliginosus.framework.playn.room;

import java.util.ArrayList;
import java.util.List;

import org.pointandclick.fooliginosus.framework.playn.PlaynDrawable;
import org.pointandclick.fooliginosus.framework.playn.PointerListener;
import org.pointandclick.fooliginosus.framework.playn.layers.PointAndClickLayer;
import org.pointandclick.fooliginosus.framework.playn.pointer.PointerManager;
import org.pointandclick.fooliginosus.framework.playn.pointer.interactive.InteractionEvent;
import org.pointandclick.fooliginosus.framework.playn.pointer.interactive.Interactive;
import org.pointandclick.fooliginosus.framework.playn.pointer.mouselistener.MouseListener;

import playn.core.GroupLayer;
import playn.core.ImageLayer;
import pythagoras.f.Point;

public class RoomView implements Interactive, PlaynDrawable {

	private ImageLayer backGround;
	private List<PointAndClickLayer> layers;
	private PointerListener pointerListener;
	private float scaleX, scaleY;

	public RoomView() {
		scaleX = 1;
		scaleY = 1;
	}

	public void attach(GroupLayer rootLayer) {
		rootLayer.add(getBackGround());

		for (PointAndClickLayer element : getLayers()) {
			rootLayer.add(element.getLayer());

			if (element instanceof Interactive) {
				PointerManager.get().addInteractive((Interactive) element);
			}

			if (element instanceof MouseListener) {
				PointerManager.get().addMouseListener((MouseListener) element);
			}
		}

		PointerManager.get().addInteractive(this);
	}

	public void detach(GroupLayer rootLayer) {
		rootLayer.remove(getBackGround());

		for (PointAndClickLayer element : getLayers()) {
			rootLayer.remove(element.getLayer());

			if (element instanceof Interactive) {
				PointerManager.get().removeInteractive((Interactive) element);
			}

			if (element instanceof MouseListener) {
				PointerManager.get().removeMouseListener((MouseListener) element);
			}
		}

		PointerManager.get().removeInteractive(this);
	}

	public ImageLayer getBackGround() {
		return backGround;
	}

	public void setBackGround(ImageLayer backGround) {
		this.backGround = backGround;
	}

	public List<PointAndClickLayer> getLayers() {
		if (layers == null) {
			layers = new ArrayList<PointAndClickLayer>();
		}

		return layers;
	}

	public void setLayers(List<PointAndClickLayer> layers) {
		this.layers = layers;
	}

	@Override
	public boolean hitTest(float x, float y) {
		return true;
	}

	@Override
	public Point screenToLayer(float x, float y) {
		float sx = (float) (x / scaleX);
		float sy = (float) (y / scaleY);
		return new Point(sx, sy);
	}

	@Override
	public void onEvent(InteractionEvent event) {
		getPointerListener().onEvent(event);
	}

	@Override
	public boolean interactWithOthers() {
		return false;
	}

	@Override
	public int getZOrder() {
		return -1;
	}

	public void setPointerListener(PointerListener pointerListener) {
		this.pointerListener = pointerListener;
	}

	protected PointerListener getPointerListener() {
		return pointerListener;
	}

	@Override
	public void update(float delta) {
		for (PointAndClickLayer roomLayer : getLayers()) {
			roomLayer.update(delta);
		}
	}

	@Override
	public void paint(float alpha) {
		for (PointAndClickLayer roomLayer : getLayers()) {
			roomLayer.paint(alpha);
		}
	}

	public void scale(float scaleX, float scaleY) {
		this.scaleX = scaleX;
		this.scaleY = scaleY;

		getBackGround().setScale(scaleX, scaleY);

		List<PointAndClickLayer> layers = getLayers();
		for (PointAndClickLayer roomLayer : layers) {
			roomLayer.scale(scaleX, scaleY);
		}
	}
}
