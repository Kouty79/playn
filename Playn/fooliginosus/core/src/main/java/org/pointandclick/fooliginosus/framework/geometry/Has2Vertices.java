package org.pointandclick.fooliginosus.framework.geometry;

public interface Has2Vertices {

	Vertex getV1();

	Vertex getV2();

}
