package org.pointandclick.fooliginosus.framework.geometry.scenario.showcases;

import java.awt.Color;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.pointandclick.fooliginosus.framework.geometry.Area;
import org.pointandclick.fooliginosus.framework.geometry.Path;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;
import org.pointandclick.fooliginosus.framework.geometry.scenario.Scenario;
import org.pointandclick.fooliginosus.framework.geometry.shortestpath.ShortestPath;

public class Scenarios implements Iterable<Scenario> {

	private static List<Scenario> scenarios;

	public static Scenario chimneysScenario() {
		Scenario scenario = new Scenario("Chimneys");

		Area area = Areas.cimneyArea();
		scenario.add(area, Color.BLACK, Color.LIGHT_GRAY);

		DefaultVertex start = new DefaultVertex(7, 9);
		DefaultVertex end = new DefaultVertex(21, 9);
		scenario.add(start, Color.RED);
		scenario.add(end, Color.GREEN);

		ShortestPath shortestPath = new ShortestPath(scenario.areasIterator().next().getElement());
		Path path = shortestPath.shortestPath(start, end);

		if (path != null) {
			scenario.add(path, Color.BLUE);
		}

		return scenario;
	}

	public static Scenario concaveScenario() {
		Scenario scenario = new Scenario("Concave Bounds");

		Area area = Areas.concaveArea();
		scenario.add(area, Color.BLACK, Color.LIGHT_GRAY);

		DefaultVertex start = new DefaultVertex(-12, 60);
		DefaultVertex end = new DefaultVertex(7.5, 56.9);
		scenario.add(start, Color.RED);
		scenario.add(end, Color.GREEN);

		ShortestPath shortestPath = new ShortestPath(scenario.areasIterator().next().getElement());
		Path path = shortestPath.shortestPath(start, end);

		if (path != null) {
			scenario.add(path, Color.BLUE);
			// scenario.add(shortestPath.getTree(), Color.ORANGE);
		}

		return scenario;
	}

	public static Scenario simpleScenario() {
		Scenario scenario = new Scenario("Simple scenarios");

		Area area = Areas.squareArea();

		Vertex start = new DefaultVertex(-8, 8);
		Vertex end = new DefaultVertex(+8, -7);
		scenario.add(start, Color.RED);
		scenario.add(end, Color.GREEN.darker());

		ShortestPath shortestPath = new ShortestPath(area);
		shortestPath.shortestPath(start, end);

		if (shortestPath.getShortestPath() != null) {
			scenario.add(shortestPath.getShortestPath(), Color.BLUE);
			scenario.add(shortestPath.getTree(), Color.ORANGE);
		}

		scenario.add(shortestPath.getArea(), Color.BLACK, Color.DARK_GRAY);

		return scenario;
	}

	public static Scenario simpleScenarioB() {
		Scenario scenario = new Scenario("Simple scenario B");

		Area area = Areas.squareArea();

		Vertex start = new DefaultVertex(-5, 2);
		Vertex end = new DefaultVertex(+8, -7);
		scenario.add(start, Color.RED);
		scenario.add(end, Color.GREEN.darker());

		ShortestPath shortestPath = new ShortestPath(area);
		shortestPath.shortestPath(start, end);

		if (shortestPath.getShortestPath() != null) {
			scenario.add(shortestPath.getShortestPath(), Color.BLUE);
			scenario.add(shortestPath.getTree(), Color.ORANGE);
		}

		scenario.add(shortestPath.getArea(), Color.BLACK, Color.DARK_GRAY);

		return scenario;
	}

	public static Scenario twoSquaresScenario() {
		Scenario scenario = new Scenario("Two Squares");

		Vertex start = new DefaultVertex(-4, 2);
		Vertex end = new DefaultVertex(+5, -2);

		Area area = Areas.twoSquaresArea();

		ShortestPath shortestPath = new ShortestPath(area);
		Path path = shortestPath.shortestPath(start, end);

		if (path != null) {
			scenario.add(path, Color.BLUE);
		}
		scenario.add(shortestPath.getArea(), Color.BLACK, Color.DARK_GRAY);
		scenario.add(start, Color.RED);
		scenario.add(end, Color.GREEN.darker());
		// scenario.add(shortestPath.getTree(), Color.ORANGE);

		return scenario;
	}

	public static Scenario twoSquaresScenarioB() {
		Scenario scenario = new Scenario("Two Squares B");

		Vertex start = new DefaultVertex(-6, 0);
		Vertex end = new DefaultVertex(+6, 0);

		Area area = Areas.twoSquaresArea();

		ShortestPath shortestPath = new ShortestPath(area);
		shortestPath.shortestPath(start, end);

		Path path = shortestPath.getShortestPath();
		if (path != null) {
			scenario.add(path, Color.BLUE);
		}
		scenario.add(shortestPath.getArea(), Color.BLACK, Color.DARK_GRAY);
		scenario.add(start, Color.RED);
		scenario.add(end, Color.GREEN.darker());
		// scenario.add(shortestPath.getTree(), Color.ORANGE);

		return scenario;
	}

	public static Scenario twoSquaresScenarioNoPath() {
		Scenario scenario = new Scenario("Two squares, no path from A to B");

		Vertex start = new DefaultVertex(-4, 0);
		Vertex end = new DefaultVertex(+6, 0);

		Area area = Areas.twoSquaresArea();

		ShortestPath shortestPath = new ShortestPath(area);
		shortestPath.shortestPath(start, end);

		Path path = shortestPath.getShortestPath();
		if (path != null) {
			scenario.add(path, Color.BLUE);
		}
		scenario.add(shortestPath.getArea(), Color.BLACK, Color.DARK_GRAY);
		scenario.add(start, Color.RED);
		scenario.add(end, Color.GREEN.darker());
		scenario.add(shortestPath.getTree(), Color.ORANGE);

		return scenario;
	}

	public static List<Scenario> getScenarios() {
		if (scenarios == null) {
			scenarios = createScenarios();
		}

		return new ArrayList<Scenario>(scenarios);
	}

	private static List<Scenario> createScenarios() {
		ArrayList<Scenario> scenarios = new ArrayList<Scenario>();

		// scenarios.add(chimneysScenario());
		// scenarios.add(concaveScenario());
		// scenarios.add(simpleScenario());
		// scenarios.add(simpleScenarioB());
		// scenarios.add(twoSquaresScenario());
		// scenarios.add(twoSquaresScenarioB());
		// scenarios.add(twoSquaresScenarioNoPath());

		try {

			List<Method> declaredMethods = findBuilderMethods(Scenarios.class, Scenario.class);
			for (Method method : declaredMethods) {
				if (Modifier.isStatic(method.getModifiers())) {
					Scenario scenario = (Scenario) method.invoke(null);
					scenarios.add(scenario);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return scenarios;
	}

	private static List<Method> findBuilderMethods(Class<?> builder, Class<?> retType) {
		Method[] declaredMethods = builder.getDeclaredMethods();

		List<Method> methods = new ArrayList<Method>();
		for (Method method : declaredMethods) {
			if (method.getReturnType().equals(retType) && method.getParameterTypes().length == 0) {
				methods.add(method);
			}
		}

		return methods;
	}

	@Override
	public Iterator<Scenario> iterator() {
		return getScenarios().iterator();
	}

}
