package org.pointandclick.fooliginosus.framework.playn.layers.impl;

import org.pointandclick.fooliginosus.framework.playn.layers.Scalable;

public abstract class AbstractScalableLayer extends AbstractRoomLayer implements Scalable {

	private float scaleX;
	private float scaleY;

	protected abstract void doScale();

	public AbstractScalableLayer(int zOrder) {
		super(zOrder);
	}

	@Override
	public final void scale(float scaleX, float scaleY) {
		this.scaleX = scaleX;
		this.scaleY = scaleY;
		doScale();
	}

	protected float getScaleX() {
		return scaleX;
	}

	protected float getScaleY() {
		return scaleY;
	}

	protected float screenToLayerX(double x) {
		return (float) (x / getScaleX());
	}

	protected float screenToLayerY(double y) {
		return (float) (y / getScaleY());
	}

}
