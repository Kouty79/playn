package org.pointandclick.fooliginosus.framework.playn.pointer;

public interface HasZOrder {

	int getZOrder();
}
