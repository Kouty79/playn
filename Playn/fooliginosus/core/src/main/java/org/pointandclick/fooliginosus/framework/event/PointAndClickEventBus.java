package org.pointandclick.fooliginosus.framework.event;

import com.google.common.eventbus.EventBus;

public class PointAndClickEventBus {

	private static final EventBus instance = new EventBus();

	private PointAndClickEventBus() {
	}

	public static EventBus get() {
		return instance;
	}
}
