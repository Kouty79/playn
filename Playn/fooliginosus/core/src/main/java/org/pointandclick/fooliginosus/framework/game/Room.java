package org.pointandclick.fooliginosus.framework.game;

import org.pointandclick.fooliginosus.framework.geometry.Area;

public interface Room {

	String getRoomId();

	ImageResource getBackGroundImage();

	Area getWalkableArea();

}
