package org.pointandclick.fooliginosus.framework.geometry.intersection;

public class SegmentIntersection {

	public static final double DEFAULT_PRECISION = 0.0001;

	private double precision;

	/**
	 * Constructor with default precision.
	 */
	public SegmentIntersection() {
		this(DEFAULT_PRECISION);
	}

	/**
	 * 
	 * @param precision
	 *            difference between two numbers to be equal
	 */
	public SegmentIntersection(double precision) {
		this.precision = precision;
	}

	public double getPrecision() {
		return precision;
	}

	/**
	 * Check if the two segments intersect
	 * 
	 * @param s1x1
	 *            segment 1 x1
	 * @param s1y1
	 *            segment 1 y1
	 * @param s1x2
	 *            segment 1 x2
	 * @param s1y2
	 *            segment 1 y2
	 * @param s2x1
	 *            segment 2 x1
	 * @param s2y1
	 *            segment 2 y1
	 * @param s2x2
	 *            segment 2 x2
	 * @param s2y2
	 *            segment 2 y2
	 * @return true if the two segments intersect.
	 */
	public IntersectResult intersect2(double s1x1, double s1y1, double s1x2, double s1y2, double s2x1, double s2y1, double s2x2, double s2y2) {

		// Checking if X or Y of the second segment are right or left segment 1
		// line
		double s2p1 = side(s1x1, s1y1, s1x2, s1y2, s2x1, s2y1);
		double s2p2 = side(s1x1, s1y1, s1x2, s1y2, s2x2, s2y2);

		int numBorderIntersect = 0;
		int numEdgeIntersection = 0;
		// Check if at least one point lies on the segment line
		if (s2p1 == 0 && between(s2x1, s1x1, s1x2) && between(s2y1, s1y1, s1y2)) {

			if (onEdge(s2x1, s1x1, s1x2) && onEdge(s2y1, s1y1, s1y2)) {
				numEdgeIntersection++;
			} else {
				numBorderIntersect++;
			}

		}
		if (s2p2 == 0 && between(s2x2, s1x1, s1x2) && between(s2y2, s1y1, s1y2)) {

			if (onEdge(s2x2, s1x1, s1x2) && onEdge(s2y2, s1y1, s1y2)) {
				numEdgeIntersection++;
			} else {
				numBorderIntersect++;
			}
		}

		IntersectResult endpointIntersection = evaluateEndpointIntersection(numBorderIntersect, numEdgeIntersection, s2p1, s2p2);
		if (endpointIntersection != null) {
			return endpointIntersection;
		}

		// If the same signum (not 0) and not aligned, both point if segment 2
		// are right or left of segment 1 line
		if (Math.signum(s2p1) == Math.signum(s2p2) && s2p1 != 0) {
			return IntersectResult.NO_INTERSECTION;
		}

		// In this case on point is right, and the other left. Must re-check
		// swapping segments
		double s1p1 = side(s2x1, s2y1, s2x2, s2y2, s1x1, s1y1);
		double s1p2 = side(s2x1, s2y1, s2x2, s2y2, s1x2, s1y2);

		// Check if at least one point lies on the segment line
		numBorderIntersect = 0;
		numEdgeIntersection = 0;
		if (s1p1 == 0 && between(s1x1, s2x1, s2x2) && between(s1y1, s2y1, s2y2)) {

			if (onEdge(s1x1, s2x1, s2x2) && onEdge(s1y1, s2y1, s2y2)) {
				numEdgeIntersection++;
			} else {
				numBorderIntersect++;
			}
		}
		if (s1p2 == 0 && between(s1x2, s2x1, s2x2) && between(s1y2, s2y1, s2y2)) {

			if (onEdge(s1x2, s2x1, s2x2) && onEdge(s1y2, s2y1, s2y2)) {
				numEdgeIntersection++;
			} else {
				numBorderIntersect++;
			}
		}

		endpointIntersection = evaluateEndpointIntersection(numBorderIntersect, numEdgeIntersection, s1p1, s1p2);
		if (endpointIntersection != null) {
			return endpointIntersection;
		}

		if (s2p1 == 0 && s2p2 == 0) {

			// Aligned but not intersecting (otherwise endpointIntersection
			// would have detected the intersection)
			return IntersectResult.NO_INTERSECTION;
		}

		// If the same signum, both point if segment 2 are right or left segment
		// 1 line
		if (Math.signum(s1p1) == Math.signum(s1p2)) {
			return IntersectResult.NO_INTERSECTION;
		}

		// Segment 1 has on point on the left and one on the right of segment 2.
		// In addition
		// segment 2 has one point on the left and one on the right of segment
		// 1. So they intersect.
		return IntersectResult.INTERSECTION;
	}

	private IntersectResult evaluateEndpointIntersection(int numBorderIntersect, int numEdgeIntersection, double v1Side, double v2Side) {
		if (numBorderIntersect == 1 || numEdgeIntersection == 1) {
			if (v1Side > 0 || v2Side > 0) {
				return IntersectResult.EDGE_INTERSECTION_RIGHT;
			}

			if (v1Side < 0 || v2Side < 0) {
				return IntersectResult.EDGE_INTERSECTION_LEFT;
			}

			return numEdgeIntersection == 1 ? IntersectResult.VERTEX_INTERSECTION_ALIGNED : IntersectResult.PARTIAL_OVERLAP;
		}

		if (numEdgeIntersection == 2) {
			return IntersectResult.OVERLAP;
		}

		if (numBorderIntersect == 2) {
			return IntersectResult.PARTIAL_OVERLAP;
		}

		return null;
	}

	public IntersectResult intersect(double s1x1, double s1y1, double s1x2, double s1y2, double s2x1, double s2y1, double s2x2, double s2y2) {

		double s2p1 = side(s1x1, s1y1, s1x2, s1y2, s2x1, s2y1);
		double s2p2 = side(s1x1, s1y1, s1x2, s1y2, s2x2, s2y2);

		if (s2p1 == 0 && s2p2 == 0) {

			// Aligned segments
			return evaluateAlignedSegments(s1x1, s1y1, s1x2, s1y2, s2x1, s2y1, s2x2, s2y2);
		}

		// If the same signum and not 0 (one edge intersection), both point of
		// segment 2 are right or left of segment 1 line
		if (Math.signum(s2p1) == Math.signum(s2p2)) {
			return IntersectResult.NO_INTERSECTION;
		}

		double s1p1 = side(s2x1, s2y1, s2x2, s2y2, s1x1, s1y1);
		double s1p2 = side(s2x1, s2y1, s2x2, s2y2, s1x2, s1y2);

		// If the same signum and not 0 (one edge intersection), both point of
		// segment 1 are right or left of segment 2 line
		if (Math.signum(s1p1) == Math.signum(s1p2)) {
			return IntersectResult.NO_INTERSECTION;
		}

		// if we are here, they are not aligned and they intersect (in one
		// point)
		if (s2p1 == 0 || s2p2 == 0) {

			// One point of segment 2 is inside the segment 1, the other outside
			if (s2p1 > 0 || s2p2 > 0) {
				return IntersectResult.EDGE_INTERSECTION_RIGHT;
			} else {
				return IntersectResult.EDGE_INTERSECTION_LEFT;
			}
		}

		if (s1p1 == 0 || s1p2 == 0) {

			// One point of segment 1 is inside the segment 2, the other outside
			if (s1p1 > 0 || s1p2 > 0) {
				return IntersectResult.EDGE_INTERSECTION_RIGHT;
			} else {
				return IntersectResult.EDGE_INTERSECTION_LEFT;
			}
		}

		// if we are here, the do intersect, they are not aligned
		// and none of the vertices intersects the other segment. Therefore they
		// intersect.
		return IntersectResult.INTERSECTION;
	}

	private IntersectResult evaluateAlignedSegments(double s1x1, double s1y1, double s1x2, double s1y2, double s2x1, double s2y1, double s2x2, double s2y2) {

		boolean s2v1InsideS1 = between(s2x1, s1x1, s1x2) && between(s2y1, s1y1, s1y2);
		boolean s2v1onVertex = s2v1InsideS1 && onEdge(s2x1, s1x1, s1x2) && onEdge(s2y1, s1y1, s1y2);

		boolean s2v2InsideS1 = between(s2x2, s1x1, s1x2) && between(s2y2, s1y1, s1y2);
		boolean s2v2onVertex = s2v2InsideS1 && onEdge(s2x2, s1x1, s1x2) && onEdge(s2y2, s1y1, s1y2);

		boolean s1v1InsideS2 = between(s1x1, s2x1, s2x2) && between(s1y1, s2y1, s2y2);
		// boolean s1v1onVertex = s1v1InsideS2 &&
		// onEdge(s1x1, s2x1, s2x2) &&
		// onEdge(s1y1, s2y1, s2y2);

		boolean s1v2InsideS2 = between(s1x2, s2x1, s2x2) && between(s1y2, s2y1, s2y2);
		// boolean s1v2onVertex = s1v2InsideS2 &&
		// onEdge(s1x2, s2x1, s2x2) &&
		// onEdge(s1y2, s2y1, s2y2);

		if (!s2v1InsideS1 && !s2v2InsideS1 && !s1v1InsideS2 && !s1v2InsideS2) {
			return IntersectResult.NO_INTERSECTION;
		}

		if (s2v1onVertex && s2v2onVertex) {
			return IntersectResult.OVERLAP;
		}

		if (s2v1InsideS1 && s2v2InsideS1) {
			return IntersectResult.S2_INSIDE_S1;
		}

		if (s1v1InsideS2 && s1v2InsideS2) {
			return IntersectResult.S1_INSIDE_S2;
		}

		if (s2v1onVertex || s2v2onVertex) {
			return IntersectResult.VERTEX_INTERSECTION_ALIGNED;
		}

		return IntersectResult.PARTIAL_OVERLAP;
	}

	public boolean pointInSegment(double x, double y, double sx1, double sy1, double sx2, double sy2) {

		return side(sx1, sy1, sx2, sy2, x, y) == 0 && between(x, sx1, sx2) && between(y, sy1, sy2);
	}

	public boolean between(double x, double a, double b) {
		return x >= Math.min(a, b) && x <= Math.max(a, b);
	}

	public boolean onEdge(double x, double a, double b) {
		return x == Math.min(a, b) || x == Math.max(a, b);
	}

	/**
	 * Tells which side of the line the given vertex is.
	 * 
	 * @param x1
	 *            Segment x1
	 * @param y1
	 *            Segment y1
	 * @param x2
	 *            Segment x2
	 * @param y2
	 *            Segment y2
	 * @param x
	 *            Test point x
	 * @param y
	 *            Test point y
	 * @return < 0 point is left of the segment (x1,y1)->(x2,y2). 0 same line. >
	 *         0 right
	 */
	public double side(double x1, double y1, double x2, double y2, double x, double y) {
		double val = (y2 - y1) * x + (x1 - x2) * y + (x2 * y1 - x1 * y2);

		// double val = y2*x - y1*x + x1*y - x2*y + x2*y1 - x1*y2;

		return (Math.abs(val) < getPrecision()) ? 0 : val;
	}

	public double orizzontaLineSide(double lineY, double y) {
		return lineY - y;
	}

	public double[] middlePoint(double x1, double y1, double x2, double y2) {
		double x = (x1 + x2) / 2;
		double y = (y1 + y2) / 2;

		return new double[] { x, y };
	}

	public IntersectResult segmentIntersectHalfLine(double x1, double y1, double x2, double y2, double x, double y) {
		double sp1 = orizzontaLineSide(y, y1);
		double sp2 = orizzontaLineSide(y, y2);

		int numBorderIntersect = 0;
		int numEdgeIntersect = 0;
		if (sp1 == 0 && x1 >= x) {
			if (x1 == x) {
				numEdgeIntersect++;
			} else {
				numBorderIntersect++;
			}
		}

		if (sp2 == 0 && x2 >= x) {
			if (x2 == x) {
				numEdgeIntersect++;
			} else {
				numBorderIntersect++;
			}
		}

		IntersectResult endpointIntersection = evaluateEndpointIntersection(numBorderIntersect, numEdgeIntersect, sp1, sp2);
		if (endpointIntersection != null) {
			return endpointIntersection;
		}

		if (Math.signum(sp1) == Math.signum(sp2)) {
			return IntersectResult.NO_INTERSECTION;
		}

		// One point one one side of the line, and one on the other.
		// Must swap and recheck

		double side;
		if (y2 >= y1) {
			side = side(x1, y1, x2, y2, x, y);
		} else {
			side = side(x2, y2, x1, y1, x, y);
		}

		return (side > 0) ? IntersectResult.NO_INTERSECTION : IntersectResult.INTERSECTION;
	}

	public enum IntersectResult {
		NO_INTERSECTION,

		/**
		 * One vertex in the border, the other left
		 */
		EDGE_INTERSECTION_LEFT,
		/**
		 * One vertex in the border, the other right
		 */
		EDGE_INTERSECTION_RIGHT,
		/**
		 * Segments aligned and intersecting in one vertex
		 */
		VERTEX_INTERSECTION_ALIGNED,

		/**
		 * Segments overlapping
		 */
		OVERLAP,
		/**
		 * Segments partial overlapping
		 */
		PARTIAL_OVERLAP,

		S2_INSIDE_S1, S1_INSIDE_S2,

		/**
		 * Segments intersecting in one point
		 */
		INTERSECTION,

	}

}
