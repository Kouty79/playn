package org.pointandclick.fooliginosus.framework.playn;

public interface PlaynDrawable {

	void update(float delta);

	void paint(float alpha);

}
