package org.pointandclick.fooliginosus.framework.playn.pointer;

import org.pointandclick.fooliginosus.framework.playn.pointer.interactive.Interaction;
import org.pointandclick.fooliginosus.framework.playn.pointer.interactive.InteractionEvent;
import org.pointandclick.fooliginosus.framework.playn.pointer.interactive.InteractionType;
import org.pointandclick.fooliginosus.framework.playn.pointer.interactive.Interactive;

import pythagoras.f.Point;

class PointerInteractionEvent implements InteractionEvent {

	private InteractionType interactionType;
	private PointerInteraction pointerInteraction;
	private Interactive draggingElement;
	private boolean behind;
	private float x, y;

	public PointerInteractionEvent(Point point, InteractionType interactionType, Interactive draggingElement, boolean behind) {
		this.x = point.x;
		this.y = point.y;
		this.interactionType = interactionType;
		this.draggingElement = draggingElement;
		this.behind = behind;
	}

	@Override
	public float x() {
		return x;
	}

	@Override
	public float y() {
		return y;
	}

	@Override
	public Interaction getInteraction() {
		if (pointerInteraction == null) {
			pointerInteraction = new PointerInteraction();
		}
		return pointerInteraction;
	}

	@Override
	public String toString() {
		return "" + getInteraction() + " at " + "(" + x() + "; " + y() + ")";
	}

	@Override
	public boolean alreadyHandled() {
		return behind;
	}

	private class PointerInteraction implements Interaction {

		public PointerInteraction() {
		}

		@Override
		public InteractionType getType() {
			return interactionType;
		}

		@Override
		public Interactive getDraggingElement() {
			return draggingElement;
		}

		@Override
		public String toString() {
			String dragged = (getDraggingElement() == null) ? "" : " dragged: " + getDraggingElement().toString();
			return String.valueOf(interactionType) + dragged;
		}
	}

}
