package org.pointandclick.fooliginosus.core;

import org.pointandclick.fooliginosus.framework.game.ImageResource;

public class Foolboy implements ImageResource {

	@Override
	public String getURI() {
		return "foolboy.png";
	}

	@Override
	public int getWidth() {
		return 221;
	}

	@Override
	public int getHeight() {
		return 321;
	}

}
