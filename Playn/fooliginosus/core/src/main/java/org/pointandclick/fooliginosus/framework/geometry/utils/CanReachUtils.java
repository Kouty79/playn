package org.pointandclick.fooliginosus.framework.geometry.utils;

import java.util.Iterator;

import org.pointandclick.fooliginosus.framework.geometry.Area;
import org.pointandclick.fooliginosus.framework.geometry.Polygon;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;
import org.pointandclick.fooliginosus.framework.geometry.intersection.GeometryIntersection;
import org.pointandclick.fooliginosus.framework.geometry.intersection.GeometryIntersection.PolygonIntersection;

public class CanReachUtils {

	private GeometryIntersection intersectUtil;

	private Area area;

	public CanReachUtils(Area area) {
		this.area = area;
	}

	public CanReachUtils(Area area, double precision) {
		this.area = area;
		intersectUtil = new GeometryIntersection(precision);
	}

	public Area getArea() {
		return area;
	}

	public boolean canReach(Vertex v1, Vertex v2) {
		Iterator<Polygon> holesIterator = getArea().holesIterator();

		// Check for holes intersection
		while (holesIterator.hasNext()) {
			Polygon polygon = (Polygon) holesIterator.next();
			if (intersectHole(getIntersectUtil().intersect(v1, v2, polygon))) {
				return false;
			}
		}

		// Check for border intersection
		if (intersectBorder(getIntersectUtil().intersect(v1, v2, getArea().getBounds()))) {
			return false;
		}

		return true;
	}

	private boolean intersectHole(PolygonIntersection intersection) {
		return PolygonIntersection.INTERSECT == intersection || PolygonIntersection.INSIDE == intersection;
	}

	private boolean intersectBorder(PolygonIntersection intersection) {
		return PolygonIntersection.INTERSECT == intersection || PolygonIntersection.OUTSIDE == intersection;
	}

	protected GeometryIntersection getIntersectUtil() {
		if (intersectUtil == null) {
			intersectUtil = new GeometryIntersection();
		}
		return intersectUtil;
	}

}
