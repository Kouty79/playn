package org.pointandclick.fooliginosus.framework.playn;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;

import org.pointandclick.fooliginosus.framework.game.ImageResource;

import playn.core.Canvas;
import playn.core.CanvasImage;
import playn.core.Color;
import playn.core.Image;
import playn.core.ImageLayer;
import playn.core.ResourceCallback;

public class ResourceLoader {

	private String imageRelativeUrl;

	public ResourceLoader(String imageRelativeUrl) {
		this.imageRelativeUrl = imageRelativeUrl;
	}

	public Image createImage(ImageResource resource) {
		Image image;
		if (resource != null) {
			image = assets().getImage(imageRelativeUrl + "/" + resource.getURI());
		} else {
			image = createUniformColorImage(800, 480, Color.argb(128, 255, 255, 255));
		}

		return image;
	}

	public Image createCharacter(ImageResource resource) {
		return assets().getImage(imageRelativeUrl + "/" + resource.getURI());
	}

	public ImageLayer createCharacterLayer(final Image image) {
		final ImageLayer characterLayer = graphics().createImageLayer(image);

		image.addCallback(new ResourceCallback<Image>() {
			@Override
			public void error(Throwable err) {
				log().error("Error while loading a image", err);
			}

			@Override
			public void done(Image image) {
				characterLayer.setOrigin((int) (image.width() * 0.84), (int) (image.height() * 0.97));
			}
		});

		return characterLayer;
	}

	public CanvasImage createUniformColorImage(int width, int height, int color) {
		CanvasImage image = graphics().createImage(width, height);

		Canvas canvas = image.canvas();
		canvas.setFillColor(color);
		canvas.fillRect(0, 0, width, height);

		return image;
	}

}
