package org.pointandclick.fooliginosus.framework.geometry.scenario.showcases;

import java.util.ArrayList;

import org.pointandclick.fooliginosus.framework.geometry.Area;
import org.pointandclick.fooliginosus.framework.geometry.Polygon;
import org.pointandclick.fooliginosus.framework.geometry.impl.AreaBuilder;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultArea;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultPolygon;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;
import org.pointandclick.fooliginosus.framework.geometry.impl.PolygonBuilder;
import org.pointandclick.fooliginosus.framework.geometry.scenario.SimpleArea;

public class Areas {

	public static Area cimneyArea() {
		DefaultPolygon border = new PolygonBuilder().addVertex(new DefaultVertex(0, 2)).addVertex(new DefaultVertex(0, 18))
				.addVertex(new DefaultVertex(30, 18)).addVertex(new DefaultVertex(30, 2)).build();

		DefaultPolygon hole = new PolygonBuilder().addVertex(new DefaultVertex(8, 8)).addVertex(new DefaultVertex(10, 10)).addVertex(new DefaultVertex(12, 8))
				.addVertex(new DefaultVertex(16, 8)).addVertex(new DefaultVertex(18, 10)).addVertex(new DefaultVertex(20, 8))
				.addVertex(new DefaultVertex(20, 7)).addVertex(new DefaultVertex(8, 7)).build();

		ArrayList<Polygon> holes = new ArrayList<Polygon>();
		holes.add(hole);

		return new DefaultArea(border, holes);
	}

	public static Area concaveArea() {
		double[] vertices = { -13.183594, 58.904646, -13.183594, 62.754726, 28.652344, 62.835089, 27.421875, 59.175928, 4.394531, 59.712097, -0.175781,
				56.656226, 2.285156, 50.958427, 8.789063, 50.513427, 8.085938, 56.072035, 6.328125, 57.421294, 26.542969, 57.326521, 14.941406, 43.325178,
				-1.933594, 46.195042, -5.976562, 52.696361, -3.867187, 60.75916 };

		PolygonBuilder polygonBuilder = new PolygonBuilder();
		for (int i = 0; i < vertices.length; i += 2) {
			polygonBuilder.addVertex(vertices[i], vertices[i + 1]);
		}
		DefaultPolygon border = polygonBuilder.build();

		return new DefaultArea(border);
	}

	public static Area squareArea() {
		return new SimpleArea();
	}

	public static Area twoSquaresArea() {
		return new AreaBuilder().buildBounds().addVertex(-10, -10).addVertex(-10, +10).addVertex(+10, +10).addVertex(+10, -10).done().addHole()
				.addVertex(-5, -2).addVertex(-5, +2).addVertex(-1, +2).addVertex(-1, -2).done().addHole().addVertex(+5, +2).addVertex(+1, +2).addVertex(+1, -2)
				.addVertex(+5, -2).done().buildArea();
	}

}
