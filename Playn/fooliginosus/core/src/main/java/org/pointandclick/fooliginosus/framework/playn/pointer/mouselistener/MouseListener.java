package org.pointandclick.fooliginosus.framework.playn.pointer.mouselistener;

import org.pointandclick.fooliginosus.framework.playn.pointer.CanReceiveHits;
import org.pointandclick.fooliginosus.framework.playn.pointer.HasZOrder;

public interface MouseListener extends HasZOrder, CanReceiveHits {

	void onMouseEvent(MouseEvent event);
}
