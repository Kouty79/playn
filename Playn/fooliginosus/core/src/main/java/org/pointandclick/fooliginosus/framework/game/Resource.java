package org.pointandclick.fooliginosus.framework.game;

public interface Resource {

	String getURI();
}
