package org.pointandclick.fooliginosus.framework.playn.pointer.interactive;

public interface InteractionEvent {

	float x();

	float y();

	/**
	 * 
	 * @return true, if the event has already dispatched to a layer with higher
	 *         ZOrder
	 */
	boolean alreadyHandled();

	Interaction getInteraction();

}
