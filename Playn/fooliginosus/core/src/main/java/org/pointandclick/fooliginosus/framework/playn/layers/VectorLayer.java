package org.pointandclick.fooliginosus.framework.playn.layers;

import org.pointandclick.fooliginosus.framework.playn.graphics.VectorGraphics;

import playn.core.CanvasImage;
import playn.core.ImageLayer;

public interface VectorLayer extends PointAndClickLayer {

	ImageLayer getLayer();

	CanvasImage getImage();

	VectorGraphics getVectorGraphics();

}
