package org.pointandclick.fooliginosus.framework.geometry;

import java.util.Iterator;

public interface Area {

	/**
	 * Shape external bounds
	 * 
	 * @return
	 */
	Polygon getBounds();

	/**
	 * Iterates over holes.
	 */
	public Iterator<Polygon> holesIterator();

}
