package org.pointandclick.fooliginosus.core;

import org.pointandclick.fooliginosus.framework.game.Settings;

public class FooliginosusSettings implements Settings {

	private static final float zoom = 1.2f;

	@Override
	public String imageBaseUrl() {
		return "images/";
	}

	@Override
	public float zoom() {
		return zoom;
	}

	@Override
	public boolean debug() {
		return true;
	}

}
