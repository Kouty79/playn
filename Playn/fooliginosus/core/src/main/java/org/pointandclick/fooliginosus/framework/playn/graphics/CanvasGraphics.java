package org.pointandclick.fooliginosus.framework.playn.graphics;

import static playn.core.PlayN.graphics;

import java.util.Iterator;

import org.pointandclick.fooliginosus.framework.geometry.Polygon;
import org.pointandclick.fooliginosus.framework.geometry.Rectangle;
import org.pointandclick.fooliginosus.framework.geometry.Segment;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;

import playn.core.Canvas;
import playn.core.CanvasImage;
import playn.core.Path;

public class CanvasGraphics implements VectorGraphics {

	private CanvasImage image;
	private float scaleX;
	private float scaleY;

	protected CanvasGraphics() {
		scaleX = 1;
		scaleY = 1;
	}

	public CanvasGraphics(CanvasImage image) {
		this();
		this.image = image;
	}

	public float getScaleX() {
		return scaleX;
	}

	public void setScaleX(float scaleX) {
		this.scaleX = scaleX;
	}

	public float getScaleY() {
		return scaleY;
	}

	public void setScaleY(float scaleY) {
		this.scaleY = scaleY;
	}

	@Override
	public void draw(Segment segment) {
		float x0 = scaleX(segment.getV1().getX());
		float y0 = scaleY(segment.getV1().getY());
		float x1 = scaleX(segment.getV2().getX());
		float y1 = scaleY(segment.getV2().getY());

		getCanvas().drawLine(x0, y0, x1, y1);
	}

	@Override
	public void draw(Vertex vertex) {
		float x = scaleX(vertex.getX());
		float y = scaleY(vertex.getY());
		getCanvas().drawPoint(x, y);
	}

	@Override
	public void stroke(Rectangle rectangle) {
		// TODO Auto-generated method stub

	}

	@Override
	public void stroke(Polygon polygon) {
		Path path = createPolygonPath(new DefaultVertex(0, 0), polygon);
		getCanvas().strokePath(path);
	}

	@Override
	public void stroke(Vertex origin, double radius) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fill(Polygon polygon) {
		Path path = createPolygonPath(new DefaultVertex(0, 0), polygon);
		getCanvas().fillPath(path);
	}

	@Override
	public void fill(Rectangle rectangle) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fillCircle(Vertex origin, double radius) {
		// TODO Auto-generated method stub

	}

	public CanvasImage getImage() {
		return image;
	}

	public Canvas getCanvas() {
		return getImage().canvas();
	}

	protected Path createPolygonPath(Vertex shift, Polygon polygon) {
		Path path = graphics().createPath();

		Iterator<Vertex> vIt = polygon.verticesIterator();

		Vertex first = null;
		if (vIt.hasNext()) {
			first = (Vertex) vIt.next();
			float x = (float) (first.getX() - shift.getX());
			float y = (float) (first.getY() - shift.getY());
			path.moveTo(scaleX(x), scaleY(y));
		}

		while (vIt.hasNext()) {
			Vertex vertex = (Vertex) vIt.next();
			float x = (float) (vertex.getX() - shift.getX());
			float y = (float) (vertex.getY() - shift.getY());
			path.lineTo(scaleX(x), scaleY(y));
		}

		if (first != null) {
			float x = (float) (first.getX() - shift.getX());
			float y = (float) (first.getY() - shift.getY());
			path.lineTo(scaleX(x), scaleY(y));
		}

		return path;
	}

	protected float scaleX(double x) {
		return (float) (x * getScaleX());
	}

	protected float scaleY(double y) {
		return (float) (y * getScaleY());
	}

}
