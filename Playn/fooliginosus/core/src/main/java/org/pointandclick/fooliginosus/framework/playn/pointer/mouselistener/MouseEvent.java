package org.pointandclick.fooliginosus.framework.playn.pointer.mouselistener;

public interface MouseEvent {

	float x();

	float y();

	MouseEventType getType();
}
