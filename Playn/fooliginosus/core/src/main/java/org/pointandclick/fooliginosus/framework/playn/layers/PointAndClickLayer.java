package org.pointandclick.fooliginosus.framework.playn.layers;

import org.pointandclick.fooliginosus.framework.playn.PlaynDrawable;

import playn.core.Layer;

public interface PointAndClickLayer extends PlaynDrawable, Scalable {

	Layer getLayer();

	int getZOrder();

	@Override
	void scale(float scaleX, float scaleY);

	@Override
	void update(float delta);

	@Override
	void paint(float alpha);

}
