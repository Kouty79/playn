package org.pointandclick.fooliginosus.framework.geometry.graph.impl;

import java.util.Iterator;

import org.pointandclick.fooliginosus.framework.geometry.graph.Graph;
import org.pointandclick.fooliginosus.framework.geometry.graph.Node;

public class DefaultGraph<T> implements Graph<T> {

	private DefaultNode<T> root;
	private T rootElement;

	// protected DefaultGraph() {};

	public DefaultGraph(T root) {
		this.rootElement = root;
	}

	public DefaultNode<T> getRoot() {
		if (root == null) {
			root = newNode(rootElement, 0, null);
		}

		return root;
	}

	@Override
	public Iterator<Node<T>> iterator() {
		return depthFirstIterator();
	}

	public Iterator<Node<T>> depthFirstIterator() {
		return new DepthFirstIterator<T>(getRoot());
	}

	public Iterator<Node<T>> breadthFirstIterator() {
		return new BreadthFirstIterator<T>(getRoot());
	}

	public DefaultNode<T> find(T element) {
		Iterator<Node<T>> depthFirstIterator = breadthFirstIterator();
		while (depthFirstIterator.hasNext()) {
			DefaultNode<T> node = (DefaultNode<T>) depthFirstIterator.next();
			if (node.getElement().equals(element)) {
				return node;
			}
		}

		return null;
	}

	protected DefaultNode<T> newNode(T element, double distance, DefaultNode<T> parent) {
		return new DefaultNode<T>(element, distance, parent);
	}

}
