package org.pointandclick.fooliginosus.framework.playn.layers.impl;

import org.pointandclick.fooliginosus.framework.playn.layers.PointAndClickLayer;

public abstract class AbstractRasterLayer extends AbstractRoomLayer implements PointAndClickLayer {

	private float scaleX, scaleY;

	public AbstractRasterLayer(int zOrder) {
		super(zOrder);
		init();
	}

	protected void init() {
		scaleX = 1;
		scaleY = 1;
	}

	@Override
	public void scale(float scaleX, float scaleY) {
		this.scaleX = scaleX;
		this.scaleY = scaleY;

		getLayer().setScale(scaleX, scaleY);
	}

	public float getScaleX() {
		return scaleX;
	}

	public float getScaleY() {
		return scaleY;
	}

	protected void setTranslation(float x, float y) {
		getLayer().setTranslation(x * getScaleX(), y * getScaleY());
	}

}
