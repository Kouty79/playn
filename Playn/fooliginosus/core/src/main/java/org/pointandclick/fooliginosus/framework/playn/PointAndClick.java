package org.pointandclick.fooliginosus.framework.playn;

import static playn.core.PlayN.graphics;

import java.util.HashMap;
import java.util.Map;

import org.pointandclick.fooliginosus.framework.game.ImageResource;
import org.pointandclick.fooliginosus.framework.game.Room;
import org.pointandclick.fooliginosus.framework.game.Settings;
import org.pointandclick.fooliginosus.framework.playn.layers.impl.CharacterLayer;
import org.pointandclick.fooliginosus.framework.playn.room.RoomPresenter;

import playn.core.GroupLayer;
import playn.core.Image;
import playn.core.ImageLayer;

/**
 * Point&Click game controller
 * 
 * @author Kouty
 * 
 */
public class PointAndClick implements PlaynDrawable {

	private GameElementFactory roomViewFactory;
	private Settings settings;
	private Map<String, Room> rooms;
	private RoomPresenter roomPresenter;
	private ResourceLoader resourceManager;
	private CharacterLayer characterLayer;

	public PointAndClick(Settings settings) {
		this.settings = settings;
	}

	public void addRoom(Room room) {
		getRooms().put(room.getRoomId(), room);
	}

	public Room getRoom(String roomId) {
		return getRooms().get(roomId);
	}

	public void setCharacter(ImageResource character) {
		Image characterImage = getResourceManager().createCharacter(character);
		ImageLayer characterImageLayer = getResourceManager().createCharacterLayer(characterImage);
		createCharacterLayer(characterImageLayer);
	}

	protected void createCharacterLayer(ImageLayer characterImageLayer) {
		characterLayer = new CharacterLayer(100, characterImageLayer);
	}

	public CharacterLayer getCharacterLayer() {
		return characterLayer;
	}

	public void register() {
		graphics().setSize(graphics().screenWidth(), graphics().screenHeight());
	}

	public void enterRoom(String roomId, int direction) {
		Room room = getRoom(roomId);

		int width = room.getBackGroundImage().getWidth();
		int height = room.getBackGroundImage().getHeight();
		float scaleX = ((float) graphics().width()) / width;
		float scaleY = ((float) graphics().height()) / height;

		createRoomPresenter(scaleX, scaleY);

		getRoomPresenter().setCharacter(getCharacterLayer());
		getRoomPresenter().changeRoom(room);
		getCharacterLayer().setPosition(100, 840);
	}

	public Settings getSettings() {
		return settings;
	}

	protected Map<String, Room> getRooms() {
		if (rooms == null) {
			rooms = new HashMap<String, Room>();
		}
		return rooms;
	}

	public ResourceLoader getResourceManager() {
		if (resourceManager == null) {
			resourceManager = new ResourceLoader(getSettings().imageBaseUrl());
		}
		return resourceManager;
	}

	protected GameElementFactory getRoomViewFactory() {
		if (roomViewFactory == null) {
			roomViewFactory = new GameElementFactory(getResourceManager());
		}
		return roomViewFactory;
	}

	public RoomPresenter getRoomPresenter() {
		return roomPresenter;
	}

	protected void createRoomPresenter(float scaleX, float scaleY) {
		roomPresenter = new RoomPresenter(getRootLayer(), getRoomViewFactory(), scaleX, scaleY);
	}

	public GroupLayer getRootLayer() {
		return graphics().rootLayer();
	}

	@Override
	public void update(float delta) {
		if (getRoomPresenter() != null) {
			getRoomPresenter().getRoomView().update(delta);
		}
	}

	@Override
	public void paint(float alpha) {
		if (getRoomPresenter() != null) {
			getRoomPresenter().getRoomView().paint(alpha);
		}
	}

}
