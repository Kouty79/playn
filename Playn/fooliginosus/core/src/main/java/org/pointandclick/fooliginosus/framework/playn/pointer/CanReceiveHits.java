package org.pointandclick.fooliginosus.framework.playn.pointer;

import pythagoras.f.Point;

public interface CanReceiveHits {

	// /**
	// *
	// * @return Hitting rectangle.
	// */
	// HasSize getHittableLayer();
	//
	// /**
	// * Refined hit system to hit Areas and not only rectangles.
	// *
	// * @return Hitting area or null if rectangular shape.
	// */
	// Area getHittableArea();

	boolean hitTest(float x, float y);

	Point screenToLayer(float x, float y);

}
