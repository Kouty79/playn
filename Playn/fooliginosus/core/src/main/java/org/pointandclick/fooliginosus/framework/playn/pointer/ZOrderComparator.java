package org.pointandclick.fooliginosus.framework.playn.pointer;

import java.util.Comparator;

class ZOrderComparator implements Comparator<HasZOrder> {

	@Override
	public int compare(HasZOrder i1, HasZOrder i2) {
		return i1.getZOrder() - i2.getZOrder();
	}

}
