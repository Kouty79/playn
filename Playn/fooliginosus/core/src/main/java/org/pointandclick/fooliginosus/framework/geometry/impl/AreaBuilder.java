package org.pointandclick.fooliginosus.framework.geometry.impl;

import java.util.ArrayList;
import java.util.List;

import org.pointandclick.fooliginosus.framework.geometry.Polygon;

public class AreaBuilder {

	private Polygon bounds;
	private List<Polygon> holes;

	public AreaBuilder() {
		holes = new ArrayList<Polygon>();
	}

	public BoundsBuilder buildBounds() {
		return new BoundsBuilder();
	}

	public class BoundsBuilder {

		private PolygonBuilder builder;

		private BoundsBuilder() {
			builder = new PolygonBuilder();
		}

		public BoundsBuilder addVertex(double x, double y) {
			builder.addVertex(x, y);
			return this;
		}

		public HolesBuilder done() {
			bounds = builder.build();
			return new HolesBuilder();
		}
	}

	public class HolesBuilder {

		private HolesBuilder() {
		}

		public HoleBuilder addHole() {
			return new HoleBuilder();
		}

		public DefaultArea buildArea() {
			return new DefaultArea(bounds, holes);
		}
	}

	public class HoleBuilder {
		private PolygonBuilder builder;

		private HoleBuilder() {
			builder = new PolygonBuilder();
		}

		public HoleBuilder addVertex(double x, double y) {
			builder.addVertex(x, y);
			return this;
		}

		public HolesBuilder done() {
			holes.add(builder.build());
			return new HolesBuilder();
		}

	}

}
