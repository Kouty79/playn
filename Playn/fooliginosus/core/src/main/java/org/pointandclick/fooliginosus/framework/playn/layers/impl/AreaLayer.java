package org.pointandclick.fooliginosus.framework.playn.layers.impl;

import static playn.core.PlayN.graphics;

import java.util.Iterator;

import org.pointandclick.fooliginosus.framework.geometry.Area;
import org.pointandclick.fooliginosus.framework.geometry.Polygon;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;
import org.pointandclick.fooliginosus.framework.geometry.utils.GeometryUtils;
import org.pointandclick.fooliginosus.framework.playn.graphics.VectorGraphics;
import org.pointandclick.fooliginosus.framework.playn.pointer.mouselistener.MouseEvent;
import org.pointandclick.fooliginosus.framework.playn.pointer.mouselistener.MouseEventType;
import org.pointandclick.fooliginosus.framework.playn.pointer.mouselistener.MouseListener;

import playn.core.CanvasImage;
import playn.core.Color;
import pythagoras.f.Point;

public class AreaLayer extends AbstractVectorLayer implements MouseListener {

	private Area area;
	private boolean redraw;

	private GeometryUtils geometryUtils;

	private int strokeColor;
	private int fillColor;
	private int unselectedStroke = Color.argb(150, 100, 155, 255);
	private int unselectedFill = Color.argb(50, 100, 155, 255);
	private int selectedStroke = Color.argb(150, 255, 140, 0);
	private int selectedFill = Color.argb(50, 255, 140, 0);

	public AreaLayer(Area area, int zOrder) {
		super(zOrder);
		this.area = area;
		init();
		markForRedraw();
	}

	protected void init() {
		setStrokeColor(unselectedStroke);
		setFillColor(unselectedFill);
		geometryUtils = new GeometryUtils(VectorGraphics.PRECISION);
	}

	@Override
	public void update(float delta) {
		if (needsRedraw()) {
			getCanvas().clear();

			getCanvas().setStrokeColor(getStrokeColor());
			getCanvas().setFillColor(getFillColor());
			getVectorGraphics().stroke(getArea().getBounds());
			getVectorGraphics().fill(getArea().getBounds());

			Iterator<Polygon> holesIterator = area.holesIterator();
			getCanvas().setFillColor(Color.argb(100, 255, 255, 255));
			while (holesIterator.hasNext()) {
				Polygon hole = (Polygon) holesIterator.next();
				getVectorGraphics().stroke(hole);
				getVectorGraphics().fill(hole);
			}
		}
	}

	@Override
	public void paint(float alpha) {
	}

	public Area getArea() {
		return area;
	}

	@Override
	protected CanvasImage createImage() {
		markForRedraw();
		CanvasImage image = graphics().createImage(graphics().width(), graphics().height());

		return image;
	}

	protected void markForRedraw() {
		redraw = true;
	}

	protected boolean needsRedraw() {
		boolean tmp = redraw;
		redraw = false;

		return tmp;
	}

	@Override
	public boolean hitTest(float x, float y) {
		x = screenToLayerX(x);
		y = screenToLayerY(y);

		return geometryUtils.pointInArea(getArea(), new DefaultVertex(x, y));
	}

	@Override
	public Point screenToLayer(float x, float y) {
		return new Point(screenToLayerX(x), screenToLayerY(y));
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (MouseEventType.ENTER == event.getType()) {
			strokeColor = getSelectedStroke();
			fillColor = getSelectedFill();
			markForRedraw();
		} else if (MouseEventType.LEAVE == event.getType()) {
			strokeColor = getUnselectedStroke();
			fillColor = getUnselectedFill();
			markForRedraw();
		}
	}

	public int getUnselectedStroke() {
		return unselectedStroke;
	}

	public void setUnselectedStroke(int unselectedStroke) {
		this.unselectedStroke = unselectedStroke;
	}

	public int getUnselectedFill() {
		return unselectedFill;
	}

	public void setUnselectedFill(int unselectedFill) {
		this.unselectedFill = unselectedFill;
	}

	public int getSelectedStroke() {
		return selectedStroke;
	}

	public void setSelectedStroke(int selectedStroke) {
		this.selectedStroke = selectedStroke;
	}

	public int getSelectedFill() {
		return selectedFill;
	}

	public void setSelectedFill(int selectedFill) {
		this.selectedFill = selectedFill;
	}

	protected int getStrokeColor() {
		return strokeColor;
	}

	protected void setStrokeColor(int strokeColor) {
		this.strokeColor = strokeColor;
	}

	protected int getFillColor() {
		return fillColor;
	}

	protected void setFillColor(int fillColor) {
		this.fillColor = fillColor;
	}

}
