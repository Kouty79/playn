package org.pointandclick.fooliginosus.framework.playn.pointer.mouselistener;

public enum MouseEventType {

	MOVE, ENTER, LEAVE;

}
