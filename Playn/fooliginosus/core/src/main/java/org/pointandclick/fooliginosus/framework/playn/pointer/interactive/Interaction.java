package org.pointandclick.fooliginosus.framework.playn.pointer.interactive;

public interface Interaction {

	InteractionType getType();

	Interactive getDraggingElement();

}
