package org.pointandclick.fooliginosus.framework.geometry;

public interface Vertex {

	double getX();

	double getY();

}
