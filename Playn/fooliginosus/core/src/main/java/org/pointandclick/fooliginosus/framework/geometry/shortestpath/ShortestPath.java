package org.pointandclick.fooliginosus.framework.geometry.shortestpath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.pointandclick.fooliginosus.framework.geometry.Area;
import org.pointandclick.fooliginosus.framework.geometry.Path;
import org.pointandclick.fooliginosus.framework.geometry.Polygon;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;
import org.pointandclick.fooliginosus.framework.geometry.graph.Node;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultPath;
import org.pointandclick.fooliginosus.framework.geometry.utils.CanReachUtils;
import org.pointandclick.fooliginosus.framework.geometry.utils.GeometryUtils;

public class ShortestPath {

	private GeometryUtils geometryUtils;
	private CanReachUtils canReachUtils;

	private Area area;
	private VertexTree tree;
	private Path shortestPath;

	public ShortestPath(Area area) {
		this.area = area;
		geometryUtils = new GeometryUtils();
		canReachUtils = new CanReachUtils(area);
	}

	public ShortestPath(Area area, double precision) {
		this.area = area;
		geometryUtils = new GeometryUtils(precision);
		canReachUtils = new CanReachUtils(area, precision);
	}

	public Area getArea() {
		return area;
	}

	public Path shortestPath(Vertex start, Vertex end) {
		List<Vertex> verticesToEvaluate = mergeAllVertex(getArea(), end);

		VertexTree tree = new VertexTree(start);
		Path shortestPath = null;

		boolean noMoreReachableNodes;
		do {
			double minDist = Double.MAX_VALUE;
			Vertex nearestVertex = null;
			Node<Vertex> nearestTreeNode = null;
			noMoreReachableNodes = true;

			for (Node<Vertex> treeNode : tree) {
				for (Vertex vertex : verticesToEvaluate) {
					if (canReach(vertex, treeNode.getElement())) {
						noMoreReachableNodes = false;

						double distance = distance(vertex, treeNode.getElement()) + treeNode.getDistance();
						if (distance < minDist) {
							minDist = distance;
							nearestVertex = vertex;
							nearestTreeNode = treeNode;
						}
					}
				}
			}

			if (nearestVertex != null) {
				verticesToEvaluate.remove(nearestVertex);
				Node<Vertex> newChild = nearestTreeNode.addChild(nearestVertex, minDist);

				if (nearestVertex.equals(end)) {
					shortestPath = createPath(newChild);
					break;
				}
			}

		} while (!verticesToEvaluate.isEmpty() && !noMoreReachableNodes);

		this.tree = tree;
		this.shortestPath = shortestPath;

		return shortestPath;
	}

	public VertexTree getTree() {
		return tree;
	}

	public Path getShortestPath() {
		return shortestPath;
	}

	private boolean canReach(Vertex v1, Vertex v2) {
		return getCanReachUtils().canReach(v1, v2);
	}

	private double distance(Vertex v1, Vertex v2) {
		return getGeometryUtils().distance(v1, v2);
	}

	private Path createPath(Node<Vertex> node) {
		List<Vertex> path = new ArrayList<Vertex>();
		path.add(node.getElement());
		double length = node.getDistance();

		while (node.getParent() != null) {
			path.add(node.getParent().getElement());
			node = node.getParent();
		}

		Collections.reverse(path);
		return new DefaultPath(path, length);
	}

	private List<Vertex> mergeAllVertex(Area area, Vertex end) {
		List<Vertex> vertices = new ArrayList<Vertex>();

		vertices.add(end);

		Iterator<Polygon> holesIterator = area.holesIterator();
		while (holesIterator.hasNext()) {
			Polygon polygon = (Polygon) holesIterator.next();
			addVertices(vertices, polygon);
		}

		addVertices(vertices, area.getBounds());

		return vertices;
	}

	private void addVertices(List<Vertex> vertexList, Polygon polygon) {
		Iterator<Vertex> verticesIterator = polygon.verticesIterator();
		while (verticesIterator.hasNext()) {
			Vertex vertex = (Vertex) verticesIterator.next();
			vertexList.add(vertex);
		}
	}

	private GeometryUtils getGeometryUtils() {
		return geometryUtils;
	}

	private CanReachUtils getCanReachUtils() {
		return canReachUtils;
	}

	public double getPrecision() {
		return getGeometryUtils().getPrecision();

	}
}
