package org.pointandclick.fooliginosus.framework.geometry.impl;

import org.pointandclick.fooliginosus.framework.geometry.Rectangle;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;

public class DefaultRectangle extends DefaultSegment implements Rectangle {

	public DefaultRectangle(Vertex top, Vertex bottom) {
		super(top, bottom);
	}

	public DefaultRectangle(Vertex top, double width, double height) {
		super(top, new DefaultVertex(top.getX() + width, top.getY() + height));
	}

	public DefaultRectangle(double x, double y, double width, double height) {
		super(new DefaultVertex(x, y), new DefaultVertex(x + width, y + height));
	}

}
