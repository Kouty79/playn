package org.pointandclick.fooliginosus.framework.playn.pointer;

import org.pointandclick.fooliginosus.framework.playn.pointer.mouselistener.MouseEvent;
import org.pointandclick.fooliginosus.framework.playn.pointer.mouselistener.MouseEventType;

import pythagoras.f.Point;

class PointerMouseEvent implements MouseEvent {

	private float x;
	private float y;
	private MouseEventType type;

	public PointerMouseEvent(Point p, MouseEventType type) {
		this.x = p.x;
		this.y = p.y;
		this.type = type;
	}

	@Override
	public float x() {
		return x;
	}

	@Override
	public float y() {
		return y;
	}

	@Override
	public MouseEventType getType() {
		return type;
	}

	@Override
	public String toString() {
		return "" + getType() + " at " + "(" + x() + "; " + y() + ")";
	}
}
