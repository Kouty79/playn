package org.pointandclick.fooliginosus.framework.playn;

import static playn.core.PlayN.graphics;

import org.pointandclick.fooliginosus.framework.game.ImageResource;
import org.pointandclick.fooliginosus.framework.game.Room;
import org.pointandclick.fooliginosus.framework.playn.room.RoomView;

import playn.core.Image;
import playn.core.ImageLayer;

public class GameElementFactory {

	private ResourceLoader resourceLoader;

	public GameElementFactory(ResourceLoader resourceManager) {
		this.resourceLoader = resourceManager;
	}

	public RoomView create(Room room) {
		RoomView roomView = new RoomView();

		ImageLayer backGround = createImageLayer(room.getBackGroundImage());
		roomView.setBackGround(backGround);

		return roomView;
	}

	public ImageLayer createImageLayer(ImageResource image) {
		Image backGroundImage = getResourceLoader().createImage(image);
		return graphics().createImageLayer(backGroundImage);
	}

	public ResourceLoader getResourceLoader() {
		return resourceLoader;
	}

}
