package org.pointandclick.fooliginosus.framework.playn.graphics;

import org.pointandclick.fooliginosus.framework.geometry.Polygon;
import org.pointandclick.fooliginosus.framework.geometry.Rectangle;
import org.pointandclick.fooliginosus.framework.geometry.Segment;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;

public interface VectorGraphics {

	double PRECISION = 0.01;

	void setScaleX(float scaleX);

	void setScaleY(float scaleX);

	float getScaleX();

	float getScaleY();

	void draw(Vertex vertex);

	void draw(Segment segment);

	void stroke(Polygon polygon);

	void stroke(Rectangle rectangle);

	void stroke(Vertex origin, double radius);

	void fill(Polygon polygon);

	void fill(Rectangle rectangle);

	void fillCircle(Vertex origin, double radius);
}
