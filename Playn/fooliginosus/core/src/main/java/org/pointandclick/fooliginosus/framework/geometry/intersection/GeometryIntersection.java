package org.pointandclick.fooliginosus.framework.geometry.intersection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.pointandclick.fooliginosus.framework.geometry.Polygon;
import org.pointandclick.fooliginosus.framework.geometry.Segment;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultSegment;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;
import org.pointandclick.fooliginosus.framework.geometry.intersection.SegmentIntersection.IntersectResult;

public class GeometryIntersection {

	public static final double DEFAULT_PRECISION = SegmentIntersection.DEFAULT_PRECISION;

	private SegmentIntersection intersectUtils;

	public GeometryIntersection() {
		this(DEFAULT_PRECISION);
	}

	public GeometryIntersection(double precision) {
		intersectUtils = new SegmentIntersection(precision);
	}

	public PolygonIntersection intersect(Vertex v1, Vertex v2, Polygon polygon) {
		return intersect(new DefaultSegment(v1, v2), polygon);
	}

	public PolygonIntersection intersect(Segment segment, Polygon polygon) {
		Iterator<Segment> segmentIt = polygon.segmentIterator();

		double maxX = -Double.MAX_VALUE;
		double minX = Double.MAX_VALUE;
		double maxY = -Double.MAX_VALUE;
		double minY = Double.MAX_VALUE;

		// Test if the segment intersect any of the polygon segments.
		int numEdgeIntersections = 0;

		while (segmentIt.hasNext()) {
			Segment polySegment = segmentIt.next();

			IntersectResult intersection = intersect(segment, polySegment);
			if (intersection == IntersectResult.INTERSECTION || intersection == IntersectResult.PARTIAL_OVERLAP) {
				return PolygonIntersection.INTERSECT;

			} else if (intersection == IntersectResult.OVERLAP || intersection == IntersectResult.S1_INSIDE_S2) {
				return PolygonIntersection.EDGE_OVERLAP;

			} else if (intersection == IntersectResult.EDGE_INTERSECTION_LEFT || intersection == IntersectResult.EDGE_INTERSECTION_RIGHT
					|| intersection == IntersectResult.VERTEX_INTERSECTION_ALIGNED) {
				if (pointInSegment(segment.getV1(), polySegment) || pointInSegment(segment.getV2(), polySegment)) {
					numEdgeIntersections++;
				} else {
					return PolygonIntersection.INTERSECT;
				}
			}

			maxX = Math.max(polySegment.getV1().getX(), maxX);
			minX = Math.min(polySegment.getV1().getX(), minX);
			maxY = Math.max(polySegment.getV1().getY(), maxY);
			minY = Math.min(polySegment.getV1().getY(), minY);
		}

		// In this case the segments start from one edge of the polygon and ends
		// in another.
		// Must check if it pass inside or outside.
		if (numEdgeIntersections > 0) {
			if (pointInPolygon(polygon, middlePoint(segment))) {
				return PolygonIntersection.INSIDE;
			}

			return PolygonIntersection.OUTSIDE;
		}

		// In this case or the segment is either inside the polygon or outside.
		if (intersectUtils.between(segment.getV1().getX(), minX, maxX) && intersectUtils.between(segment.getV1().getY(), minY, maxY)) {

			// Inside polygon
			return PolygonIntersection.INSIDE;
		}

		return PolygonIntersection.OUTSIDE;
	}

	public boolean pointInSegment(Vertex p, Segment s) {
		return intersectUtils.pointInSegment(p.getX(), p.getY(), s.getV1().getX(), s.getV1().getY(), s.getV2().getX(), s.getV2().getY());
	}

	private List<Segment> getSegmentsRightOf(Polygon p, double x) {
		List<Segment> segments = new ArrayList<Segment>();

		Iterator<Segment> segmentIterator = p.segmentIterator();
		while (segmentIterator.hasNext()) {
			Segment segment = (Segment) segmentIterator.next();

			if (segment.getV1().getX() >= x || segment.getV2().getX() >= x) {
				segments.add(segment);
			}
		}

		return segments;
	}

	public boolean pointInPolygon(Polygon polygon, Vertex point) {
		List<Segment> segmentsRightOf = getSegmentsRightOf(polygon, point.getX());

		int numIntersections = 0;
		for (Segment segment : segmentsRightOf) {
			IntersectResult segmentIntersectHalfLine = segmentIntersectHalfLine(segment, point);

			if (IntersectResult.EDGE_INTERSECTION_RIGHT == segmentIntersectHalfLine || IntersectResult.EDGE_INTERSECTION_LEFT == segmentIntersectHalfLine) {

				if (segment.getV1().getY() == point.getY() || segment.getV2().getY() == point.getY()) {

					// Half line intersect a vertex. Must be count only if it's
					// right
					if (IntersectResult.EDGE_INTERSECTION_RIGHT == segmentIntersectHalfLine) {
						numIntersections++;
					}
				}
			} else if (IntersectResult.PARTIAL_OVERLAP == segmentIntersectHalfLine || IntersectResult.OVERLAP == segmentIntersectHalfLine) {
				numIntersections += 2;
			} else if (IntersectResult.NO_INTERSECTION != segmentIntersectHalfLine) {
				numIntersections++;
			}

		}

		return (numIntersections % 2) == 1;
	}

	public Vertex middlePoint(Segment s) {
		double[] middlePoint = intersectUtils.middlePoint(s.getV1().getX(), s.getV1().getY(), s.getV2().getX(), s.getV2().getY());

		return new DefaultVertex(middlePoint[0], middlePoint[1]);
	}

	public double side(Segment s, Vertex v) {
		return intersectUtils.side(s.getV1().getX(), s.getV1().getY(), s.getV2().getX(), s.getV2().getY(), v.getX(), v.getY());
	}

	public IntersectResult segmentIntersectHalfLine(Segment s, Vertex v) {
		return intersectUtils.segmentIntersectHalfLine(s.getV1().getX(), s.getV1().getY(), s.getV2().getX(), s.getV2().getY(), v.getX(), v.getY());
	}

	public IntersectResult intersect(Segment s1, Segment s2) {
		return intersect(s1.getV1(), s1.getV2(), s2.getV1(), s2.getV2());
	}

	public IntersectResult intersect(Vertex s1v1, Vertex s1v2, Vertex s2v1, Vertex s2v2) {
		return intersectUtils.intersect(s1v1.getX(), s1v1.getY(), s1v2.getX(), s1v2.getY(), s2v1.getX(), s2v1.getY(), s2v2.getX(), s2v2.getY());
	}

	public double getPrecision() {
		return intersectUtils.getPrecision();
	}

	public enum PolygonIntersection {
		INSIDE, OUTSIDE,

		BORDER_INTERSECTION, EDGE_OVERLAP,

		INTERSECT
	}

}
