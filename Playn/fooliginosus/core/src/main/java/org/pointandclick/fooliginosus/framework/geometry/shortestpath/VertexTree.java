package org.pointandclick.fooliginosus.framework.geometry.shortestpath;

import org.pointandclick.fooliginosus.framework.geometry.Vertex;
import org.pointandclick.fooliginosus.framework.geometry.graph.impl.DefaultGraph;

public class VertexTree extends DefaultGraph<Vertex> {

	public VertexTree(Vertex root) {
		super(root);
	}

}
