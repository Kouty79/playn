package org.pointandclick.fooliginosus.framework.geometry.intersection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.pointandclick.fooliginosus.framework.geometry.intersection.SegmentIntersection;
import org.pointandclick.fooliginosus.framework.geometry.intersection.SegmentIntersection.IntersectResult;

public class SegmentIntersectionTest {

	private SegmentIntersection intersectUtils;

	@Before
	public void setUp() throws Exception {
		intersectUtils = new SegmentIntersection();
	}

	@Test
	public void testSideLeft() {
		double segx1 = 1;
		double segy1 = 1;
		double segx2 = 3;
		double segy2 = 3;

		double vx = 2;
		double vy = 2.5;

		Assert.assertTrue(intersectUtils.side(segx1, segy1, segx2, segy2, vx, vy) < 0);
	}

	@Test
	public void testSideRight() {
		double segx1 = 1;
		double segy1 = 1;
		double segx2 = 3;
		double segy2 = 3;

		double vx = 2;
		double vy = 1.5;

		Assert.assertTrue(intersectUtils.side(segx1, segy1, segx2, segy2, vx, vy) > 0);
	}

	@Test
	public void testOrizzontalLineSideLeft() {
		Assert.assertTrue(intersectUtils.orizzontaLineSide(3, 5) < 0);
	}

	@Test
	public void testOrizzontalLineSideRight() {
		Assert.assertTrue(intersectUtils.orizzontaLineSide(3, 1) > 0);
	}

	@Test
	public void testOrizzontalLineSideCoherence() {
		double segx1 = 1;
		double segy1 = 2;
		double segx2 = 3;
		double segy2 = 2;

		double vx = 2;
		double vy = 1.5;

		Assert.assertTrue(intersectUtils.side(segx1, segy1, segx2, segy2, vx, vy) > 0);
		Assert.assertTrue(intersectUtils.orizzontaLineSide(segy1, vy) > 0);
	}

	@Test
	public void testSideAligendPoint() {
		double segx1 = 1;
		double segy1 = 1;
		double segx2 = 3;
		double segy2 = 3;

		double vx = 4;
		double vy = 4;

		Assert.assertEquals(0, intersectUtils.side(segx1, segy1, segx2, segy2, vx, vy), intersectUtils.getPrecision());
	}

	@Test
	public void testIntersection() {
		double segAx1 = 1;
		double segAy1 = 2;
		double segAx2 = 2;
		double segAy2 = 1;

		double segBx1 = 1;
		double segBy1 = 1;
		double segBx2 = 2;
		double segBy2 = 2;

		IntersectResult intersect = intersectUtils.intersect(segAx1, segAy1, segAx2, segAy2, segBx1, segBy1, segBx2, segBy2);
		Assert.assertEquals(IntersectResult.INTERSECTION, intersect);
	}

	@Test
	public void testIntersectionOnEdge() {
		double segAx1 = 1;
		double segAy1 = 2;
		double segAx2 = 2;
		double segAy2 = 1;

		double segBx1 = 0.5;
		double segBy1 = 1.5;
		double segBx2 = 1.5;
		double segBy2 = 2.5;

		IntersectResult intersect = intersectUtils.intersect(segAx1, segAy1, segAx2, segAy2, segBx1, segBy1, segBx2, segBy2);
		Assert.assertEquals(IntersectResult.EDGE_INTERSECTION_RIGHT, intersect);
	}

	@Test
	public void testAlignedSegmentsNoIntersection() {
		double segAx1 = 1;
		double segAy1 = 2;
		double segAx2 = 4;
		double segAy2 = 2;

		double segBx1 = 5;
		double segBy1 = 2;
		double segBx2 = 8;
		double segBy2 = 2;

		IntersectResult intersect = intersectUtils.intersect(segAx1, segAy1, segAx2, segAy2, segBx1, segBy1, segBx2, segBy2);
		Assert.assertEquals(IntersectResult.NO_INTERSECTION, intersect);
	}

	@Test
	public void testAlignedSegmentsWithEdgeIntersection() {
		double segAx1 = 1;
		double segAy1 = 2;
		double segAx2 = 4;
		double segAy2 = 2;

		double segBx1 = 4;
		double segBy1 = 2;
		double segBx2 = 8;
		double segBy2 = 2;

		IntersectResult intersect = intersectUtils.intersect(segAx1, segAy1, segAx2, segAy2, segBx1, segBy1, segBx2, segBy2);
		Assert.assertEquals(IntersectResult.VERTEX_INTERSECTION_ALIGNED, intersect);
	}

	@Test
	public void testAlignedSegmentsWithIntersection() {
		double segAx1 = 1;
		double segAy1 = 2;
		double segAx2 = 4;
		double segAy2 = 2;

		double segBx1 = 3;
		double segBy1 = 2;
		double segBx2 = 8;
		double segBy2 = 2;

		IntersectResult intersect = intersectUtils.intersect(segAx1, segAy1, segAx2, segAy2, segBx1, segBy1, segBx2, segBy2);
		Assert.assertEquals(IntersectResult.PARTIAL_OVERLAP, intersect);
	}

	@Test
	public void testSegment1Inside2() {
		double segAx1 = 1;
		double segAy1 = 2;
		double segAx2 = 4;
		double segAy2 = 2;

		double segBx1 = 0;
		double segBy1 = 2;
		double segBx2 = 8;
		double segBy2 = 2;

		IntersectResult intersect = intersectUtils.intersect(segAx1, segAy1, segAx2, segAy2, segBx1, segBy1, segBx2, segBy2);
		Assert.assertEquals(IntersectResult.S1_INSIDE_S2, intersect);
	}

	@Test
	public void testSegment2Inside1() {
		double segAx1 = 0;
		double segAy1 = 2;
		double segAx2 = 8;
		double segAy2 = 2;

		double segBx1 = 1;
		double segBy1 = 2;
		double segBx2 = 4;
		double segBy2 = 2;

		IntersectResult intersect = intersectUtils.intersect(segAx1, segAy1, segAx2, segAy2, segBx1, segBy1, segBx2, segBy2);
		Assert.assertEquals(IntersectResult.S2_INSIDE_S1, intersect);
	}

	@Test
	public void testOverlappingSegments() {
		double segAx1 = 0;
		double segAy1 = 2;
		double segAx2 = 8;
		double segAy2 = 2;

		double segBx1 = 8;
		double segBy1 = 2;
		double segBx2 = 0;
		double segBy2 = 2;

		IntersectResult intersect = intersectUtils.intersect(segAx1, segAy1, segAx2, segAy2, segBx1, segBy1, segBx2, segBy2);
		Assert.assertEquals(IntersectResult.OVERLAP, intersect);
	}

	@Test
	public void testNoIntersection() {
		double segAx1 = 1;
		double segAy1 = 2;
		double segAx2 = 2;
		double segAy2 = 1;

		double segBx1 = 0.5;
		double segBy1 = 1.5;
		double segBx2 = 1.5;
		double segBy2 = 2.6;

		IntersectResult intersect = intersectUtils.intersect(segAx1, segAy1, segAx2, segAy2, segBx1, segBy1, segBx2, segBy2);
		Assert.assertEquals(IntersectResult.NO_INTERSECTION, intersect);
	}

	@Test
	public void testHalfLineNoIntersectionAligned() {
		double segAx1 = 1;
		double segAy1 = 2;
		double segAx2 = 4;
		double segAy2 = 2;

		double x = 10;
		double y = 2;

		IntersectResult intersect = intersectUtils.segmentIntersectHalfLine(segAx1, segAy1, segAx2, segAy2, x, y);
		Assert.assertEquals(IntersectResult.NO_INTERSECTION, intersect);
	}

	@Test
	public void testHalfLineIntersectionAligend() {
		double segAx1 = 1;
		double segAy1 = 2;
		double segAx2 = 4;
		double segAy2 = 2;

		double x = 3;
		double y = 2;

		IntersectResult intersect = intersectUtils.segmentIntersectHalfLine(segAx1, segAy1, segAx2, segAy2, x, y);
		Assert.assertEquals(IntersectResult.PARTIAL_OVERLAP, intersect);
	}

	@Test
	public void testHalfLineOverlap() {
		double segAx1 = 1;
		double segAy1 = 2;
		double segAx2 = 4;
		double segAy2 = 2;

		double x = 0;
		double y = 2;

		IntersectResult intersect = intersectUtils.segmentIntersectHalfLine(segAx1, segAy1, segAx2, segAy2, x, y);
		Assert.assertEquals(IntersectResult.PARTIAL_OVERLAP, intersect);
	}

	@Test
	public void testHalfLineNoIntersection() {
		double segAx1 = 1;
		double segAy1 = 3;
		double segAx2 = 4;
		double segAy2 = 3;

		double x = 0;
		double y = 2;

		IntersectResult intersect = intersectUtils.segmentIntersectHalfLine(segAx1, segAy1, segAx2, segAy2, x, y);
		Assert.assertEquals(IntersectResult.NO_INTERSECTION, intersect);
	}

	@Test
	public void testHalfLineNoIntersection2() {
		double segAx1 = 1;
		double segAy1 = 1;
		double segAx2 = 3;
		double segAy2 = 5;

		double x = 2;
		double y = 2;

		IntersectResult intersect = intersectUtils.segmentIntersectHalfLine(segAx1, segAy1, segAx2, segAy2, x, y);
		Assert.assertEquals(IntersectResult.NO_INTERSECTION, intersect);
	}

	@Test
	public void testHalfLineNoIntersection3() {
		double segAx2 = 1;
		double segAy2 = 1;
		double segAx1 = 3;
		double segAy1 = 5;

		double x = 2;
		double y = 2;

		IntersectResult intersect = intersectUtils.segmentIntersectHalfLine(segAx1, segAy1, segAx2, segAy2, x, y);
		Assert.assertEquals(IntersectResult.NO_INTERSECTION, intersect);
	}

	@Test
	public void testHalfLineIntersection() {
		double segAx1 = 1;
		double segAy1 = 3;
		double segAx2 = 4;
		double segAy2 = 1;

		double x = 1;
		double y = 2;

		IntersectResult intersect = intersectUtils.segmentIntersectHalfLine(segAx1, segAy1, segAx2, segAy2, x, y);
		Assert.assertEquals(IntersectResult.INTERSECTION, intersect);
	}

	@Test
	public void testHalfLineBorderIntersection() {
		double segAx1 = 10;
		double segAy1 = 2;
		double segAx2 = 40;
		double segAy2 = 1;

		double x = 1;
		double y = 2;

		IntersectResult intersect = intersectUtils.segmentIntersectHalfLine(segAx1, segAy1, segAx2, segAy2, x, y);
		Assert.assertEquals(IntersectResult.EDGE_INTERSECTION_RIGHT, intersect);
	}

}
