package org.pointandclick.fooliginosus.framework.geometry.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.pointandclick.fooliginosus.framework.geometry.impl.AreaBuilder;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultArea;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultPolygon;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;

public class AreaBuilderTest {

	private AreaBuilder areaBuilder;

	@Before
	public void setUp() throws Exception {
		areaBuilder = new AreaBuilder();
	}

	@Test
	public void test() {
		DefaultArea area = areaBuilder.buildBounds().addVertex(-10, -10).addVertex(-10, +10).addVertex(+10, +10).addVertex(+10, -10).done().addHole()
				.addVertex(-5, -5).addVertex(-5, +5).addVertex(+5, +5).addVertex(+5, -5).done().buildArea();

		List<DefaultVertex> boundVertices = new ArrayList<DefaultVertex>();
		boundVertices.add(new DefaultVertex(-10, -10));
		boundVertices.add(new DefaultVertex(-10, +10));
		boundVertices.add(new DefaultVertex(+10, +10));
		boundVertices.add(new DefaultVertex(+10, -10));
		DefaultPolygon bounds = new DefaultPolygon(boundVertices);

		Assert.assertEquals(bounds, area.getBounds());

		List<DefaultVertex> holeVertices = new ArrayList<DefaultVertex>();
		holeVertices.add(new DefaultVertex(-5, -5));
		holeVertices.add(new DefaultVertex(-5, +5));
		holeVertices.add(new DefaultVertex(+5, +5));
		holeVertices.add(new DefaultVertex(+5, -5));
		DefaultPolygon hole = new DefaultPolygon(holeVertices);

		Assert.assertEquals(hole, area.holesIterator().next());
	}

}
