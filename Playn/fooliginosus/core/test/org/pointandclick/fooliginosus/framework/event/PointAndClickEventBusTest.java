package org.pointandclick.fooliginosus.framework.event;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.google.common.eventbus.Subscribe;

public class PointAndClickEventBusTest {

	@Test
	public void test() {
		new TestEventHandlerImpl();

		TestEvent event = new TestEvent();
		event.setTest("test");
		PointAndClickEventBus.get().post(event);

		assertEquals("passato", event.getTest());
	}

	@Test
	public void test2() {

	}

	public abstract class TestEventHandler {

		{
			PointAndClickEventBus.get().register(this);
		}

		@Subscribe
		public final void onEvent(TestEvent e) {
			handleEvent(e);
		}

		public abstract void handleEvent(TestEvent e);

	}

	public class TestEventHandlerImpl extends TestEventHandler {

		@Override
		public void handleEvent(TestEvent e) {
			e.setTest("passato");
		}

	}

	private static class TestEvent {
		private String test;

		public String getTest() {
			return test;
		}

		public void setTest(String test) {
			this.test = test;
		}
	}

}
