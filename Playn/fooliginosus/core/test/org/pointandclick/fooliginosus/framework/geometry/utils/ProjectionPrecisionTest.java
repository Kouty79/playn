package org.pointandclick.fooliginosus.framework.geometry.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.pointandclick.fooliginosus.framework.geometry.Segment;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultSegment;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;
import org.pointandclick.fooliginosus.framework.geometry.intersection.GeometryIntersection;

public class ProjectionPrecisionTest {

	private GeometryUtils util;
	private GeometryIntersection geoUtil;
	private static final double PRECISION = 0.01;

	@Before
	public void setUp() throws Exception {
		util = new GeometryUtils(PRECISION);
		geoUtil = new GeometryIntersection(PRECISION);
	}

	/**
	 * Test on a precision problem, with data taken from game log. <br/>
	 * event coords: (110.0, 342.0) <br/>
	 * Character position: (49.0; 419.0) <br/>
	 * end: (107.96146788990826; 409.27155963302755) <br/>
	 * event coords: (219.0, 469.0) <br/>
	 * Character position: (107.96147155761719; 409.27154541015625) <br/>
	 * vend: (219.0; 469.0)
	 */
	@Test
	public void test() {
		Segment segment = new DefaultSegment(new DefaultVertex(0, 406), new DefaultVertex(165, 411));

		// This is where user clicked
		DefaultVertex clickPoint = new DefaultVertex(110.0, 342.0);
		// This is the point to reach (projection of the click point to the
		// bounds of walkable area)
		PointSegmentDistance projection = util.pointFromSegment(clickPoint, segment);

		// This is the real side with the right coordinate.
		double doublePrecisionSide = geoUtil.side(segment, projection.getProjection());

		// This is the charater new position after having reached the projection
		// point.
		DefaultVertex newCharPostion = new DefaultVertex(107.96147155761719, 409.27154541015625);
		// This is the precision in game, because of the float precision
		// coordinates.
		double side = geoUtil.side(segment, newCharPostion);

		// Here we can see that to use game layer float coordinate, precision
		// must grater or equals to 0.01
		Assert.assertEquals(doublePrecisionSide, side, PRECISION);
	}

}
