package org.pointandclick.fooliginosus.framework.geometry.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.pointandclick.fooliginosus.framework.geometry.Segment;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultPolygon;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultSegment;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;

public class PolygonTest {

	@Test
	public void testVertexIterator() {
		DefaultVertex v1 = new DefaultVertex(0, 0);
		DefaultVertex v2 = new DefaultVertex(10, 0);
		DefaultVertex v3 = new DefaultVertex(10, 10);
		DefaultVertex v4 = new DefaultVertex(0, 10);

		List<DefaultVertex> vertices = new ArrayList<DefaultVertex>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		DefaultPolygon p = new DefaultPolygon(vertices);

		Iterator<Vertex> iterator = p.verticesIterator();

		Assert.assertEquals(v1, iterator.next());
		Assert.assertEquals(v2, iterator.next());
		Assert.assertEquals(v3, iterator.next());
		Assert.assertEquals(v4, iterator.next());

		Assert.assertFalse("There are no more vertex", iterator.hasNext());
	}

	@Test
	public void testSegmentsIterator() {
		DefaultVertex v1 = new DefaultVertex(0, 0);
		DefaultVertex v2 = new DefaultVertex(10, 0);
		DefaultVertex v3 = new DefaultVertex(10, 10);
		DefaultVertex v4 = new DefaultVertex(0, 10);

		List<DefaultVertex> vertices = new ArrayList<DefaultVertex>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		DefaultPolygon p = new DefaultPolygon(vertices);

		Iterator<Segment> iterator = p.segmentIterator();

		Assert.assertEquals(new DefaultSegment(v1, v2), iterator.next());
		Assert.assertEquals(new DefaultSegment(v2, v3), iterator.next());
		Assert.assertEquals(new DefaultSegment(v3, v4), iterator.next());
		Assert.assertEquals(new DefaultSegment(v4, v1), iterator.next());

		Assert.assertFalse("There are no more segments", iterator.hasNext());
	}

	@Test
	public void testEquals() {
		DefaultVertex v1 = new DefaultVertex(0, 0);
		DefaultVertex v2 = new DefaultVertex(10, 0);
		DefaultVertex v3 = new DefaultVertex(10, 10);
		DefaultVertex v4 = new DefaultVertex(0, 10);

		List<DefaultVertex> vertices = new ArrayList<DefaultVertex>();
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);

		DefaultPolygon p1 = new DefaultPolygon(vertices);
		DefaultPolygon p2 = new DefaultPolygon(new ArrayList<Vertex>(vertices));

		Assert.assertEquals(p1, p2);
	}

}
