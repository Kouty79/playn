package org.pointandclick.fooliginosus.framework.geometry.utils;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;
import org.pointandclick.fooliginosus.framework.geometry.scenario.SimpleArea;

public class PointSegmenteDistanceTest {

	private GeometryUtils util;

	@Before
	public void setUp() throws Exception {
		util = new GeometryUtils();
	}

	@Test
	public void testBoundaries() {
		SimpleArea area = new SimpleArea();
		Vertex v = new DefaultVertex(-11, -11);

		Vertex nearestPoint = util.nearestPoint(area, v);

		Vertex expected = area.getBounds().verticesIterator().next();

		Assert.assertEquals(expected, nearestPoint);
	}

}
