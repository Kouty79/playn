package org.pointandclick.fooliginosus.framework.geometry.impl;

import org.junit.Assert;
import org.junit.Test;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;

public class VertexTest {

	private static final double PRECISION = 0.000001;

	@Test
	public void testSimple() throws Exception {
		double x = 10;
		double y = 20;

		DefaultVertex vertex = new DefaultVertex(x, y);

		Assert.assertEquals(x, vertex.getX(), PRECISION);
		Assert.assertEquals(y, vertex.getY(), PRECISION);
	}

	@Test
	public void testEquals() throws Exception {
		Integer x = 10;
		Integer y = 20;

		DefaultVertex v1 = new DefaultVertex(x, y);
		DefaultVertex v2 = new DefaultVertex(x, y);

		Assert.assertTrue(v1.equals(v2));
	}

}
