package org.pointandclick.fooliginosus.framework.geometry.utils;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.pointandclick.fooliginosus.framework.geometry.Rectangle;
import org.pointandclick.fooliginosus.framework.geometry.Vertex;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultRectangle;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;

public class GeometryUtilsTest {

	private GeometryUtils util;
	private static final double PRECISION = 0.01;

	@Before
	public void setUp() throws Exception {
		util = new GeometryUtils(PRECISION);
	}

	@Test
	public void testPointInRectangle() {
		Rectangle rectangle = new DefaultRectangle(new DefaultVertex(10, 15), new DefaultVertex(20, 25));
		Vertex testPoint = new DefaultVertex(12, 17);

		Assert.assertTrue(util.pointInRectangle(rectangle, testPoint));
	}

	@Test
	public void testPointOutsideRectangle() {
		Rectangle rectangle = new DefaultRectangle(new DefaultVertex(10, 15), new DefaultVertex(20, 25));
		Vertex testPoint = new DefaultVertex(22, 17);

		Assert.assertFalse(util.pointInRectangle(rectangle, testPoint));
	}

	@Test
	public void testPointInRectangleTollerance() {
		Rectangle rectangle = new DefaultRectangle(new DefaultVertex(10, 15), new DefaultVertex(20, 25));
		Double precision = util.getPrecision();
		Vertex testPoint = new DefaultVertex(10 + precision, 17);

		Assert.assertTrue(util.pointInRectangle(rectangle, testPoint));
	}

}
