package org.pointandclick.fooliginosus.framework.geometry.intersection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultPolygon;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultSegment;
import org.pointandclick.fooliginosus.framework.geometry.impl.DefaultVertex;
import org.pointandclick.fooliginosus.framework.geometry.impl.PolygonBuilder;
import org.pointandclick.fooliginosus.framework.geometry.intersection.GeometryIntersection;
import org.pointandclick.fooliginosus.framework.geometry.intersection.GeometryIntersection.PolygonIntersection;

public class GeometryIntersectionTest {

	private GeometryIntersection intersectUtil;

	@Before
	public void setUp() throws Exception {
		intersectUtil = new GeometryIntersection();
	}

	@Test
	public void testIntersect() {
		DefaultVertex v1 = new DefaultVertex(5, 5);
		DefaultVertex v2 = new DefaultVertex(15, 5);
		DefaultPolygon square = createSquare();

		Assert.assertEquals("Segment intersect square", PolygonIntersection.INTERSECT, intersectUtil.intersect(v1, v2, square));
	}

	@Test
	public void testInsideIntersect() {
		DefaultSegment insideSegment = new DefaultSegment(new DefaultVertex(2, 2), new DefaultVertex(9, 9));
		DefaultPolygon square = createSquare();

		Assert.assertEquals(PolygonIntersection.INSIDE, intersectUtil.intersect(insideSegment, square));
	}

	@Test
	public void testOutsideNoIntersect() {
		DefaultSegment outsideSegment = new DefaultSegment(new DefaultVertex(2, 11), new DefaultVertex(9, 11));
		DefaultPolygon square = createSquare();

		Assert.assertEquals(PolygonIntersection.OUTSIDE, intersectUtil.intersect(outsideSegment, square));
	}

	@Test
	public void testBorderIntersect() {
		DefaultSegment borderSegment = new DefaultSegment(new DefaultVertex(1, 2), new DefaultVertex(1, 9));
		DefaultPolygon square = createSquare();

		Assert.assertEquals(PolygonIntersection.EDGE_OVERLAP, intersectUtil.intersect(borderSegment, square));
	}

	@Test
	public void testBorderIntersectInside() {
		DefaultSegment borderSegment = new DefaultSegment(new DefaultVertex(1, 2), new DefaultVertex(1, 11));
		DefaultPolygon square = new PolygonBuilder().addVertex(1, 1).addVertex(1, 10).addVertex(0, 10).addVertex(0, 12).addVertex(10, 12).addVertex(10, 1)
				.build();

		Assert.assertEquals(PolygonIntersection.INTERSECT, intersectUtil.intersect(borderSegment, square));
	}

	@Test
	public void testOverlap() {
		DefaultSegment borderSegment = new DefaultSegment(new DefaultVertex(1, 10), new DefaultVertex(1, 1));
		DefaultPolygon square = createSquare();

		Assert.assertEquals(PolygonIntersection.EDGE_OVERLAP, intersectUtil.intersect(borderSegment, square));
	}

	@Test
	public void testContigous() {
		DefaultSegment borderSegment = new DefaultSegment(new DefaultVertex(10, 10), new DefaultVertex(11, 11));
		DefaultPolygon square = createSquare();

		Assert.assertEquals(PolygonIntersection.OUTSIDE, intersectUtil.intersect(borderSegment, square));
	}

	@Test
	public void testInsideVertexToAnother() {
		DefaultSegment borderSegment = new DefaultSegment(new DefaultVertex(10, 10), new DefaultVertex(1, 1));
		DefaultPolygon square = createSquare();

		Assert.assertEquals(PolygonIntersection.INSIDE, intersectUtil.intersect(borderSegment, square));
	}

	@Test
	public void testSegmentThroughVertexInside() {
		DefaultSegment borderSegment = new DefaultSegment(new DefaultVertex(5, 5), new DefaultVertex(11, 11));
		DefaultPolygon square = createSquare();

		Assert.assertEquals(PolygonIntersection.INTERSECT, intersectUtil.intersect(borderSegment, square));
	}

	@Test
	public void testSegmentThroughVertexIntersection() {
		DefaultSegment borderSegment = new DefaultSegment(new DefaultVertex(0, 0), new DefaultVertex(11, 11));
		DefaultPolygon square = createSquare();

		Assert.assertEquals(PolygonIntersection.INTERSECT, intersectUtil.intersect(borderSegment, square));
	}

	@Test
	public void testOutsideVertexToAnother() {
		DefaultSegment borderSegment = new DefaultSegment(new DefaultVertex(10, 10), new DefaultVertex(18, 10));
		DefaultPolygon polygon = new PolygonBuilder().addVertex(new DefaultVertex(8, 8)).addVertex(new DefaultVertex(10, 10))
				.addVertex(new DefaultVertex(12, 8)).addVertex(new DefaultVertex(16, 8)).addVertex(new DefaultVertex(18, 10))
				.addVertex(new DefaultVertex(20, 8)).addVertex(new DefaultVertex(20, 7)).addVertex(new DefaultVertex(8, 7)).build();

		Assert.assertEquals(PolygonIntersection.OUTSIDE, intersectUtil.intersect(borderSegment, polygon));
	}

	private DefaultPolygon createSquare() {
		PolygonBuilder builder = new PolygonBuilder();

		DefaultPolygon square = builder.addVertex(1, 1).addVertex(1, 10).addVertex(10, 10).addVertex(10, 1).build();

		return square;
	}
}
