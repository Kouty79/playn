package org.pointandclick.fooliginosus.android;

import playn.android.GameActivity;
import playn.core.PlayN;

import org.pointandclick.fooliginosus.core.Fooliginosus;

public class FooliginosusActivity extends GameActivity {

  @Override
  public void main(){
    platform().assets().setPathPrefix("org/pointandclick/fooliginosus/resources");
    PlayN.run(new Fooliginosus());
  }
}
