package it.orbonauts.skafander.java;

import it.orbonauts.skafander.core.Skafander;
import playn.core.PlayN;
import playn.java.JavaPlatform;

public class SkafanderJava {

	public static void main(String[] args) {
		JavaPlatform platform = JavaPlatform.register();
		platform.graphics().ctx().setSize(1020 / 2, 722 / 2);
		platform.setTitle("Skafander");
		PlayN.run(new Skafander());
	}
}
