package it.orbonauts.skafander.android;

import it.orbonauts.skafander.core.Skafander;
import playn.android.GameActivity;
import playn.core.PlayN;

public class SkafanderActivity extends GameActivity {

	@Override
	public void main() {
		// platform().assets().setPathPrefix("it/orbonauts/skafander/resources");
		PlayN.run(new Skafander());
	}
}
