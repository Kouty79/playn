package it.orbonauts.skafander.pcf.asset;

import static playn.core.PlayN.log;
import playn.core.Assets;
import playn.core.Image;
import playn.core.Sound;
import playn.core.WatchedAssets;
import playn.core.util.Callback;

public class ProgressAssets implements Assets {

	private final WatchedAssets delegate;
	private final int numImages;
	private int pendingCount;
	private final ProgressCallback progressCallback;
	private Callback<Object> callback;

	public ProgressAssets(Assets asset, int numberOfImages, ProgressCallback progressCallback) {
		this.delegate = new WatchedAssets(asset);
		this.numImages = numberOfImages;
		this.pendingCount = numberOfImages;
		this.progressCallback = progressCallback;
	}

	@Override
	public final Image getImageSync(String path) {
		Image result = delegate.getImageSync(path);
		result.addCallback(getCallBack());
		return result;
	}

	@Override
	public final Image getImage(String path) {
		Image result = delegate.getImage(path);
		result.addCallback(getCallBack());
		return result;
	}

	@Override
	public final Image getRemoteImage(String url) {
		Image result = delegate.getRemoteImage(url);
		result.addCallback(getCallBack());
		return result;
	}

	@Override
	public final Image getRemoteImage(String url, float width, float height) {
		Image result = delegate.getRemoteImage(url, width, height);
		result.addCallback(getCallBack());
		return result;
	}

	@Override
	public final Sound getSound(String path) {
		Sound result = delegate.getSound(path);
		result.addCallback(getCallBack());
		return result;
	}

	@Override
	public String getTextSync(String path) throws Exception {
		return delegate.getTextSync(path);
	}

	@Override
	public void getText(String path, Callback<String> callback) {
		delegate.getText(path, callback);
	}

	@Override
	public boolean isDone() {
		return pendingCount <= 0;
	}

	@Override
	public int getPendingRequestCount() {
		return pendingCount;
	}

	@Override
	public int hashCode() {
		return delegate.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}

	@Override
	public Sound getMusic(String path) {
		return delegate.getMusic(path);
	}

	@Override
	public String toString() {
		return delegate.toString();
	}

	private Callback<Object> getCallBack() {
		if (callback == null) {

			callback = new Callback<Object>() {

				@Override
				public void onSuccess(Object result) {
					--pendingCount;
					progressCallback.onProgress(getProgress(), getPendingRequestCount());
				}

				@Override
				public void onFailure(Throwable cause) {
					log().error("Error while loading image", cause);
					--pendingCount;
					progressCallback.onProgress(getProgress(), getPendingRequestCount());
				}

				private float getProgress() {
					return (float) (numImages - getPendingRequestCount()) / numImages;
				}

			};
		}

		return callback;
	}

	public static interface ProgressCallback {

		/**
		 * 
		 * @param progress
		 *            % done. Range is from 0 to 1 included.
		 * @param pendingRequestCount
		 *            number of remaining requests
		 */
		void onProgress(float progress, int pendingRequestCount);

	}
}
