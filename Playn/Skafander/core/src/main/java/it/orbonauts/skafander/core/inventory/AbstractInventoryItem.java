package it.orbonauts.skafander.core.inventory;

import it.orbonauts.skafander.pcf.impl.HelperFactory;

public abstract class AbstractInventoryItem implements InventoryItem {

	private float x;
	private float y;

	@Override
	public void onPositionSet(float x, float y) {
		this.x = x;
		this.y = y;
	}

	protected void moveBack() {
		HelperFactory.createMotionHelper(this.getLayer(), getMoveBackSpeed()).moveTo(x, y);
	}

	protected float getMoveBackSpeed() {
		return 5f;
	}
}
