package it.orbonauts.skafander.pcf.impl;

import it.orbonauts.skafander.pcf.GameModel;
import it.orbonauts.skafander.pcf.pointer.PointerManager;
import it.orbonauts.skafander.pcf.scaling.GameScaleHelper;
import playn.core.Game;

public class PointAndClickGame implements Game {

	private GameScaleHelper scaleHelper;
	private final GameModel gameModel;
	private AbstractRoom currentRoom;

	protected PointAndClickGame(GameModel gameModel) {
		this.gameModel = gameModel;
	}

	@Override
	public void init() {
		PointerManager.register();
		this.currentRoom = new EmptyRoom();
	}

	@Override
	public void update(float delta) {
		AnimationManager.get().update(delta);
		this.currentRoom.update(delta);
	}

	@Override
	public void paint(float alpha) {
		AnimationManager.get().paint(alpha);
		this.currentRoom.paint(alpha);
	}

	@Override
	public int updateRate() {
		return 25;
	}

	public void enterRoom(AbstractRoom room) {
		this.currentRoom.leaveRoom();
		room.setGame(this);
		room.enterRoom();
		this.currentRoom = room;
	}

	protected AbstractRoom getCurrentRoom() {
		return currentRoom;
	}

	public GameModel getGameModel() {
		return gameModel;
	}

	public float getScaleY() {
		return scaleHelper.scaleYComponent();
	}

	public float getScaleX() {
		return scaleHelper.scaleYComponent();
	}

	public final GameScaleHelper getScaleHelper() {
		if (scaleHelper == null) {
			scaleHelper = HelperFactory.createGameScaleHelper(getGameModel().getWidth(), getGameModel().getHeight());
		}

		return scaleHelper;
	}

}
