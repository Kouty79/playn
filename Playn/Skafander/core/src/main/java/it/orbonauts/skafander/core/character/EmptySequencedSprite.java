package it.orbonauts.skafander.core.character;

import it.orbonauts.skafander.pcf.sprite.SequencedSprite;
import it.orbonauts.skafander.pcf.sprite.SpriteSequence;
import playn.core.Image;

public class EmptySequencedSprite implements SequencedSprite {

	private int index;

	@Override
	public void setCurrentIndex(int index) {
		this.index = index;
	}

	@Override
	public int getCurrentIndex() {
		return this.index;
	}

	@Override
	public Image getCurrentImage() {
		return null;
	}

	@Override
	public int getSize() {
		return 0;
	}

	@Override
	public SpriteSequenceIterator iterator(SpriteSequence sequence) {
		this.index = sequence.from();
		return new NullSpriteSequenceIterator();
	}

	private static class NullSpriteSequenceIterator implements SpriteSequenceIterator {

		@Override
		public void next() {
		}

	}
}
