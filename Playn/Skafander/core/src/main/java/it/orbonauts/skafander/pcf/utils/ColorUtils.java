package it.orbonauts.skafander.pcf.utils;

import playn.core.Color;

public class ColorUtils {

	public static int adjustBrithness(int color, float brightness) {
		int r = (int) Math.min(Color.red(color) * brightness, 255);
		int g = (int) Math.min(Color.green(color) * brightness, 255);
		int b = (int) Math.min(Color.blue(color) * brightness, 255);
		int a = (int) Math.min(Color.alpha(color) * brightness, 255);

		return Color.argb(a, r, g, b);
	}

	public static int setAplha(int color, int alpha) {
		int r = Color.red(color);
		int g = Color.green(color);
		int b = Color.blue(color);

		return Color.argb(alpha, r, g, b);
	}

}
