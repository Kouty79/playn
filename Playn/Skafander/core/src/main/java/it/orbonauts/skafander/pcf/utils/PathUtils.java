package it.orbonauts.skafander.pcf.utils;

import it.orbonauts.skafander.pcf.geometry.Polygon;
import it.orbonauts.skafander.pcf.geometry.Vertex;

import java.util.Iterator;

import playn.core.Path;

public class PathUtils {

	public static Path createPath(Path path, Polygon polygon, float dx, float dy) {
		Iterator<Vertex> vIt = polygon.verticesIterator();

		Vertex first = null;
		if (vIt.hasNext()) {
			first = vIt.next();
			float x = (float) (first.getX() - dx);
			float y = (float) (first.getY() - dy);
			path.moveTo(x, y);
		}

		while (vIt.hasNext()) {
			Vertex vertex = vIt.next();
			float x = (float) (vertex.getX() - dx);
			float y = (float) (vertex.getY() - dy);
			path.lineTo(x, y);
		}

		if (first != null) {
			float x = (float) (first.getX() - dx);
			float y = (float) (first.getY() - dy);
			path.lineTo(x, y);
		}

		return path;
	}
}
