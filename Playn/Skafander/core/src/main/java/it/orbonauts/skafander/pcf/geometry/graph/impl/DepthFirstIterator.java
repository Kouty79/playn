package it.orbonauts.skafander.pcf.geometry.graph.impl;

import it.orbonauts.skafander.pcf.geometry.graph.Node;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;


class DepthFirstIterator<T> implements Iterator<Node<T>> {

	private Stack<Iterator<DefaultNode<T>>> visitStack;
	private Iterator<DefaultNode<T>> removeIt;

	public DepthFirstIterator(DefaultNode<T> root) {
		ArrayList<DefaultNode<T>> rootList = new ArrayList<DefaultNode<T>>();
		rootList.add(root);
		visitStack = new Stack<Iterator<DefaultNode<T>>>();
		visitStack.push(rootList.iterator());
	}

	@Override
	public boolean hasNext() {
		return !visitStack.isEmpty();
	}

	@Override
	public DefaultNode<T> next() {
		DefaultNode<T> retVal = null;

		Iterator<DefaultNode<T>> nodeIt = visitStack.peek();

		if (nodeIt.hasNext()) {
			retVal = nodeIt.next();
			removeIt = nodeIt;

			if (!nodeIt.hasNext()) {
				visitStack.pop();
			}

			if (retVal.hasChildren()) {
				visitStack.push(retVal.getChildrenIterator());
			}
		}

		return retVal;
	}

	@Override
	public void remove() {
		if (removeIt != null) {
			removeIt.remove();
		}
	}

}
