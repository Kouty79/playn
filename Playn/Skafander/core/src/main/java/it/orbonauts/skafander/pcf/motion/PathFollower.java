package it.orbonauts.skafander.pcf.motion;

import it.orbonauts.skafander.pcf.geometry.Path;

public interface PathFollower extends MotionHelper {

	void followPath(Path path);
}
