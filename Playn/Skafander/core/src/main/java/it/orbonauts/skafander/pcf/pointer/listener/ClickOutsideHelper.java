package it.orbonauts.skafander.pcf.pointer.listener;

import it.orbonauts.skafander.pcf.pointer.listener.GlobalClickHelper.OnClickListener;
import playn.core.Layer;
import playn.core.Layer.HasSize;
import playn.core.Pointer;
import playn.core.Pointer.Event;

public class ClickOutsideHelper extends PointerListenerDelegator {

	public ClickOutsideHelper(final HasSize layer, final OnClickOutsideListener listener) {
		super(new GlobalClickHelper(new Pointer.Adapter(), new OnClickListener() {

			@Override
			public void onClick(Event event) {
				if (!Layer.Util.hitTest(layer, event.x(), event.y())) {
					listener.onClickOutside(event);
				}
			}
		}));
	}

	public interface OnClickOutsideListener {

		void onClickOutside(Event event);
	}

}
