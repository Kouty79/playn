package it.orbonauts.skafander.core.newtonlevel;

import it.orbonauts.skafander.pcf.DynamicElement;
import playn.core.Layer;

class GrassAnimation implements DynamicElement {

	private float frequency;
	private static final float MAX_A = (float) (Math.PI / 48);
	private final Layer grassLayer;
	private float tx, newTx;
	private float time;
	private final float y, x;
	private float phase;

	GrassAnimation(Layer grassLayer, float frequency, float phase) {
		this.grassLayer = grassLayer;
		time = 0;
		y = grassLayer.ty();
		x = grassLayer.tx();
		this.frequency = frequency;
		this.phase = phase;
	}

	@Override
	public void update(float delta) {
		time += delta;
		newTx = (float) (MAX_A * Math.sin(frequency * time + phase));
	}

	@Override
	public void paint(float alpha) {
		tx = (newTx * (1f - alpha)) + (tx * alpha);
		grassLayer.transform().setTransform(1, 0, tx, -1, x, y);
	}
}
