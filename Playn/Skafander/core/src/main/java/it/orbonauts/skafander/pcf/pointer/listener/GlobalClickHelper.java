package it.orbonauts.skafander.pcf.pointer.listener;

import static playn.core.PlayN.graphics;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import playn.core.Pointer.Listener;
import pythagoras.f.Point;

public class GlobalClickHelper extends PointerListenerDelegator implements Pointer.Listener {

	private final OnClickListener clickListener;
	private boolean stopClick;

	public GlobalClickHelper(Listener delegate, OnClickListener clickListener) {
		super(delegate);
		this.clickListener = clickListener;
		this.stopClick = false;
	}

	@Override
	public void onPointerStart(Event event) {
		super.onPointerStart(event);
		stopClick = clickInLayer(event);
	}

	@Override
	public void onPointerEnd(Event event) {
		super.onPointerEnd(event);
		if (!stopClick && !clickInLayer(event)) {
			clickListener.onClick(event);
		}
	}

	private boolean clickInLayer(Event event) {
		return graphics().rootLayer().hitTest(new Point(event.x(), event.y())) != null;
	}

	public interface OnClickListener {

		void onClick(Event event);
	}
}
