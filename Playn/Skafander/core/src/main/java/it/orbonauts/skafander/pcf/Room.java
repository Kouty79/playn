package it.orbonauts.skafander.pcf;

public interface Room extends DynamicElement {

	void enterRoom();

	void leaveRoom();

}
