package it.orbonauts.skafander.core.character;

import static it.orbonauts.skafander.pcf.impl.HelperFactory.createMotionHelper;
import static it.orbonauts.skafander.pcf.impl.HelperFactory.createPathFollower;
import static playn.core.PlayN.log;
import it.orbonauts.skafander.core.Skafander;
import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Path;
import it.orbonauts.skafander.pcf.geometry.Vertex;
import it.orbonauts.skafander.pcf.geometry.impl.DefaultVertex;
import it.orbonauts.skafander.pcf.geometry.shortestpath.ShortestPath;
import it.orbonauts.skafander.pcf.motion.PathFollower;
import it.orbonauts.skafander.pcf.sprite.SequencedSprite;

import java.util.ArrayList;
import java.util.List;

import playn.core.Layer;
import playn.core.util.Callback;

public final class Character {

	private final List<Callback<Character>> loadedCallback;
	private static final float CHARACTER_SPEED = 0.3f;
	private PathFollower motionHelper;
	private Area area;
	private SequencedSprite characterSprite;
	private final CharacterMotionListener motionListener;
	private CharacterRenderer renderer;

	public Character() {
		loadedCallback = new ArrayList<Callback<Character>>();
		renderer = new DefaultCharacterRenderer();
		renderer.init();

		characterSprite = new EmptySequencedSprite();
		new CharacterSpriteLoader().load(new Callback<SequencedSprite>() {

			@Override
			public void onSuccess(SequencedSprite result) {
				SequencedSprite oldSprite = characterSprite;
				characterSprite = result;
				characterSprite.setCurrentIndex(oldSprite.getCurrentIndex());
				renderer.update(characterSprite.getCurrentImage());

				for (Callback<Character> callBack : loadedCallback) {
					callBack.onSuccess(Character.this);
				}
				loadedCallback.clear();
			}

			@Override
			public void onFailure(Throwable cause) {
				log().error("Cannot load character sprite!", cause);
				for (Callback<Character> callBack : loadedCallback) {
					callBack.onFailure(cause);
				}
			}

		});

		motionListener = new CharacterMotionListener(this, IdentityPerspectiveScaler.identityPerspectiveScaler());
		initMotionHelper();
	}

	private void initMotionHelper() {
		motionHelper = createPathFollower(createMotionHelper(renderer.getLayer(), CHARACTER_SPEED));
		motionHelper.setMotionListener(motionListener);
	}

	public Layer getLayer() {
		return renderer.getLayer();
	}

	public void placeTo(float x, float y, WalkingState direction) {
		setDirection(direction);
		getLayer().setTranslation(x, y);
		getMotionHelper().moveTo(x, y);
	}

	private void setDirection(WalkingState direction) {
		getCharacterSprite().iterator(direction).next();
		updateSprite();
	}

	SequencedSprite getCharacterSprite() {
		return characterSprite;
	}

	void updateSprite() {
		renderer.update(getCharacterSprite().getCurrentImage());
	}

	public float getX() {
		return getLayer().tx();
	}

	public float getY() {
		return getLayer().ty();
	}

	public void moveTo(float x, float y) {
		if (getArea() != null) {
			Vertex start = new DefaultVertex(getX(), getY());
			Vertex end = new DefaultVertex(x, y);
			ShortestPath shortestPath = new ShortestPath(getArea(), Skafander.getPrecision());
			Path path = shortestPath.shortestPath(start, end);
			if (path != null) {
				getMotionHelper().followPath(path);
			}
		} else {
			getMotionHelper().moveTo(x, y);
		}
	}

	public void setWalkableArea(Area area) {
		this.area = area;
	}

	public boolean isMoving() {
		return getMotionHelper().isMoving();
	}

	public void setPerspectiveScaler(PerspectiveScaler perspectiveScaler) {
		motionListener.setPerspectiveScaler(perspectiveScaler);
	}

	public PerspectiveScaler getPerspectiveScaler() {
		return motionListener.getPerspectiveScaler();
	}

	public void addLoadedCallback(Callback<Character> callBack) {
		if (characterSprite != null) {
			callBack.onSuccess(this);
		} else {
			loadedCallback.add(callBack);
		}
	}

	public void setRenderer(CharacterRenderer renderer) {
		if (renderer == null) {
			renderer = new DefaultCharacterRenderer();
		}

		motionHelper.destroy();
		renderer.init();
		renderer.getLayer().setTranslation(this.renderer.getLayer().tx(), this.renderer.getLayer().ty());
		this.renderer = renderer;
		initMotionHelper();
	}

	private Area getArea() {
		return area;
	}

	PathFollower getMotionHelper() {
		return motionHelper;
	}

}
