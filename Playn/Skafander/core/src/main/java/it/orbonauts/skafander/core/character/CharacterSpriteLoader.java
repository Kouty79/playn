package it.orbonauts.skafander.core.character;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.log;
import it.orbonauts.skafander.core.resources.Resources;
import it.orbonauts.skafander.core.resources.SpriteResource;
import it.orbonauts.skafander.pcf.sprite.DefaultSequencedSprite;
import it.orbonauts.skafander.pcf.sprite.DefaultSprite;
import it.orbonauts.skafander.pcf.sprite.SequencedSprite;
import it.orbonauts.skafander.pcf.sprite.SpriteSheetLoader;

import java.util.ArrayList;
import java.util.List;

import playn.core.Image;
import playn.core.util.Callback;

class CharacterSpriteLoader {

	private SequencedSprite characterSprite;

	CharacterSpriteLoader() {
	}

	void load(final Callback<SequencedSprite> callback) {
		if (characterSprite != null) {
			callback.onSuccess(characterSprite);
		} else {
			new InternalLoader().load(new Callback<List<Image>>() {

				@Override
				public void onSuccess(List<Image> result) {
					characterSprite = new DefaultSequencedSprite(new DefaultSprite(result));
					callback.onSuccess(characterSprite);
				}

				@Override
				public void onFailure(Throwable cause) {
					log().error("Cannot load character sprite", cause);
					callback.onFailure(cause);
				}
			});
		}
	}

	private static class InternalLoader {

		private static final int RES_COUNT = 3;
		private final SpriteResource leftRightRes = Resources.Character.LR_SPRITE_SHEET;
		private final SpriteResource behindRes = Resources.Character.BH_SPRITE_SHEET;
		private final SpriteResource frontRes = Resources.Character.FR_SPRITE_SHEET;

		private int count;
		private List<Image> lrImages;
		private List<Image> bhImages;
		private List<Image> frImages;
		private Callback<List<Image>> callBack;

		private InternalLoader() {
			count = RES_COUNT;
		}

		void load(Callback<List<Image>> callBack) {
			this.callBack = callBack;
			loadLeftRightWalking();
			loadBehindWalking();
			loadFrontWalking();
		}

		private void loadLeftRightWalking() {
			new SpriteSheetLoader(
					assets().getImage(leftRightRes.getURI()),
					leftRightRes.getSpriteWidth(),
					leftRightRes.getSpriteHeight(),
					leftRightRes.getNumImages())
					.load(new Callback<List<Image>>() {

						@Override
						public void onSuccess(List<Image> result) {
							lrImages = result;
							count--;
							checkDone();
						}

						@Override
						public void onFailure(Throwable cause) {
							callBack.onFailure(cause);
						}

					});
		}

		private void loadBehindWalking() {
			new SpriteSheetLoader(assets().getImage(behindRes.getURI()),
					behindRes.getSpriteWidth(),
					behindRes.getSpriteHeight(),
					behindRes.getNumImages())
					.load(new Callback<List<Image>>() {

						@Override
						public void onSuccess(List<Image> result) {
							bhImages = result;
							count--;
							checkDone();
						}

						@Override
						public void onFailure(Throwable cause) {
							callBack.onFailure(cause);
						}

					});
		}

		private void loadFrontWalking() {
			new SpriteSheetLoader(assets().getImage(frontRes.getURI()),
					frontRes.getSpriteWidth(),
					frontRes.getSpriteHeight(),
					frontRes.getNumImages())
					.load(new Callback<List<Image>>() {

						@Override
						public void onSuccess(List<Image> result) {
							frImages = result;
							count--;
							checkDone();
						}

						@Override
						public void onFailure(Throwable cause) {
							callBack.onFailure(cause);
						}

					});
		}

		private void checkDone() {
			if (count <= 0) {
				callBack.onSuccess(mergeImages());
			}
		}

		private List<Image> mergeImages() {
			ArrayList<Image> images = new ArrayList<Image>();

			images.addAll(lrImages);
			images.addAll(bhImages);
			images.addAll(frImages);

			return images;
		}
	}
}
