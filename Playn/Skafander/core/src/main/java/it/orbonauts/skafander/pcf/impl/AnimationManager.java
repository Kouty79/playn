package it.orbonauts.skafander.pcf.impl;

import it.orbonauts.skafander.pcf.DynamicElement;

import java.util.ArrayList;
import java.util.List;

class AnimationManager implements DynamicElement {

	private static final AnimationManager INSTANCE = new AnimationManager();
	private final List<DynamicElement> dynamicElements;
	private final List<DynamicElement> toRemove;
	private final List<DynamicElement> toAdd;

	private AnimationManager() {
		dynamicElements = new ArrayList<DynamicElement>();
		toRemove = new ArrayList<DynamicElement>();
		toAdd = new ArrayList<DynamicElement>();
	}

	public static AnimationManager get() {
		return INSTANCE;
	}

	void registerElement(DynamicElement element) {
		toAdd.add(element);
	}

	boolean unregisterElement(DynamicElement element) {
		return toRemove.add(element);
	}

	@Override
	public void update(float delta) {
		for (DynamicElement element : dynamicElements) {
			element.update(delta);
		}

		dynamicElements.removeAll(toRemove);
		toRemove.clear();

		dynamicElements.addAll(toAdd);
		toAdd.clear();
	}

	@Override
	public void paint(float alpha) {
		for (DynamicElement element : dynamicElements) {
			element.paint(alpha);
		}

	}

}
