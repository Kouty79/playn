package it.orbonauts.skafander.pcf.nullutils;

public interface Nullable {

	boolean isNull();
}
