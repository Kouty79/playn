package it.orbonauts.skafander.core.loadinglayer;

import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import playn.core.CanvasImage;
import playn.core.Color;
import playn.core.Font;
import playn.core.ImmediateLayer;
import playn.core.Layer;
import playn.core.Surface;
import playn.core.TextFormat;
import playn.core.TextLayout;

public class LoadingLayerHelper {

	private static final int PROGRESS_BAR_WIDTH = (int) (graphics().width() * 0.5);
	private static final int PROGRESS_BAR_HEIGHT = 10;

	private final float start;
	private final float end;
	private float currentValue;
	private Layer waitLayer;

	public LoadingLayerHelper(float start, float end) {
		this.start = start;
		this.end = end;
	}

	public LoadingLayerHelper(float start, float end, float initValue) {
		this(start, end);
		this.currentValue = initValue;
	}

	public void update(float value) {
		log().debug("update,  progress: " + value);
		currentValue = value;
	}

	public Layer getLayer() {
		if (waitLayer == null) {
			waitLayer = graphics().createImmediateLayer(
					graphics().width(), graphics().height(),
					new ImmediateLayer.Renderer() {

						private CanvasImage image;

						{
							Font font = graphics().createFont("Courier", Font.Style.BOLD, 22);
							TextLayout layout = graphics().layoutText("Loading...", new TextFormat().withFont(font));

							image = graphics().createImage(layout.width(), layout.height());
							image.canvas()
									.setFillColor(Color.rgb(0, 0, 0))
									.fillText(layout, 0, 0);
						}

						@Override
						public void render(Surface surface) {
							float x = (surface.width() - PROGRESS_BAR_WIDTH) / 2;
							float y = (surface.height() - PROGRESS_BAR_HEIGHT) / 2;
							float w = currentValue / (end - start) * PROGRESS_BAR_WIDTH;

							surface
									.setFillColor(Color.rgb(128, 128, 128))
									.fillRect(0, 0, surface.width(), surface.height())
									.drawImage(image, x, y - image.height())
									.setFillColor(Color.rgb(128, 255, 128))
									.fillRect(x, y, w, PROGRESS_BAR_HEIGHT);
						}
					});
		}

		return waitLayer;
	}
}
