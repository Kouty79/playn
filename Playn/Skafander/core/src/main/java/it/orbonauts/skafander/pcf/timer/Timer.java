package it.orbonauts.skafander.pcf.timer;

public interface Timer {

	void start();

	void stop();

}
