package it.orbonauts.skafander.pcf.pointer;


public interface DragDropManager {

	void registerElement(Draggable element);

	void registerElement(DroppableTarget element);

	void registerElement(DragAndDroppable element);

	void unregisterElement(HasBoundedLayer element);

}
