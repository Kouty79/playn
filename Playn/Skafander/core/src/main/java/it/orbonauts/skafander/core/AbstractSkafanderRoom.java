package it.orbonauts.skafander.core;

import it.orbonauts.skafander.core.loadinglayer.LoadingAssetBuilder;
import it.orbonauts.skafander.pcf.asset.ProgressAssets;
import it.orbonauts.skafander.pcf.geometry.utils.GeometryUtils;
import it.orbonauts.skafander.pcf.impl.AbstractRoom;
import playn.core.GroupLayer;

public abstract class AbstractSkafanderRoom extends AbstractRoom {

	private final GeometryUtils geometryUtils;

	protected AbstractSkafanderRoom() {
		geometryUtils = new GeometryUtils(Skafander.getPrecision());
	}

	protected abstract void initRoom(GroupLayer gameLayer);

	protected abstract void destroyRoom(GroupLayer gameLayer);

	@Override
	public final void enterRoom() {
		initRoom(getGameLayer());
	}

	@Override
	public final void leaveRoom() {
		destroyRoom(getGameLayer());
	}

	protected GroupLayer getGameLayer() {
		return GameLayers.getGameLayers().getGameLayer();
	}

	@Override
	public Skafander getGame() {
		return (Skafander) super.getGame();
	}

	@Override
	public void update(float delta) {
		super.update(delta);
	}

	@Override
	public void paint(float alpha) {
		super.paint(alpha);
	}

	protected ProgressAssets buildProgressAssets(int numOfImages) {
		return LoadingAssetBuilder.newLoadingLayerBuilder().numberOfImages(numOfImages).build();
	}

	protected GeometryUtils getGeometryUtils() {
		return geometryUtils;
	}
}
