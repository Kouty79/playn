package it.orbonauts.skafander.pcf.geometry.impl;

import it.orbonauts.skafander.pcf.geometry.Segment;
import it.orbonauts.skafander.pcf.geometry.Vertex;

public class DefaultSegment implements Segment {

	private Vertex vertex1;
	private Vertex vertex2;

	public DefaultSegment(Vertex vertex1, Vertex vertex2) {
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
	}

	@Override
	public Vertex getV1() {
		return vertex1;
	}

	@Override
	public Vertex getV2() {
		return vertex2;
	}

	@Override
	public String toString() {
		return "[" + vertex1 + "; " + vertex2 + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vertex1 == null) ? 0 : vertex1.hashCode());
		result = prime * result + ((vertex2 == null) ? 0 : vertex2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DefaultSegment)) {
			return false;
		}
		DefaultSegment other = (DefaultSegment) obj;
		if (vertex1 == null) {
			if (other.vertex1 != null) {
				return false;
			}
		} else if (!vertex1.equals(other.vertex1)) {
			return false;
		}
		if (vertex2 == null) {
			if (other.vertex2 != null) {
				return false;
			}
		} else if (!vertex2.equals(other.vertex2)) {
			return false;
		}
		return true;
	}

}
