package it.orbonauts.skafander.pcf.timer;

public interface TimerTask {

	void run();
}
