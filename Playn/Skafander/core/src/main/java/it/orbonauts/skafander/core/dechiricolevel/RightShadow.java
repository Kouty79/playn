package it.orbonauts.skafander.core.dechiricolevel;

import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Polygon;
import it.orbonauts.skafander.pcf.geometry.impl.AreaBuilder;

import java.util.Iterator;

class RightShadow implements Area {

	private final Area area;

	public RightShadow() {
		area = new AreaBuilder()
				.buildBounds()
				.addVertex(694, 720)
				.addVertex(929, 380)
				.addVertex(1020, 380)
				.addVertex(1020, 720)
				.done().buildArea();
	}

	@Override
	public Polygon getBounds() {
		return area.getBounds();
	}

	@Override
	public Iterator<Polygon> holesIterator() {
		return area.holesIterator();
	}

}
