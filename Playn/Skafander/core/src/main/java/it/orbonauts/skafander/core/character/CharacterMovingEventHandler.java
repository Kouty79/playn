package it.orbonauts.skafander.core.character;

public interface CharacterMovingEventHandler {

	void onCharacterMoving(CharacterMovingEvent event);

}
