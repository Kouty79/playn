package it.orbonauts.skafander.core.inventory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Inventory implements Iterable<InventoryItem> {

	private final List<InventoryItem> items;
	private final InventoryObserver observer;

	Inventory(InventoryObserver observer) {
		items = new ArrayList<InventoryItem>();
		this.observer = observer;
	}

	@Override
	public Iterator<InventoryItem> iterator() {
		return items.iterator();
	}

	public void addItem(InventoryItem item) {
		addItem(item, true);
	}

	public void addItem(InventoryItem item, boolean updateView) {
		items.add(item);
		this.observer.onItemAdded(items, item, updateView);
	}

	public int size() {
		return items.size();
	}

	public void clear() {
		items.clear();
	}

	public boolean removeItem(InventoryItem item) {
		return removeItem(item, true);
	}

	public boolean removeItem(InventoryItem item, boolean updateView) {
		boolean removed = items.remove(item);
		if (removed) {
			this.observer.onItemRemoved(items, item, updateView);
		}
		return removed;
	}

	interface InventoryObserver {

		void onItemAdded(List<InventoryItem> newList, InventoryItem item, boolean updateView);

		void onItemRemoved(List<InventoryItem> newList, InventoryItem item, boolean updateView);
	}

}
