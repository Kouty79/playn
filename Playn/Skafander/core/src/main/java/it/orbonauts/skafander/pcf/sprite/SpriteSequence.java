package it.orbonauts.skafander.pcf.sprite;

public interface SpriteSequence {

	int from();

	int to();

}
