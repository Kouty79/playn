package it.orbonauts.skafander.core.dechiricolevel;

import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Polygon;
import it.orbonauts.skafander.pcf.geometry.impl.AreaBuilder;

import java.util.Iterator;

public class LeftShadow implements Area {

	private final Area area;

	public LeftShadow() {
		area = new AreaBuilder()
				.buildBounds()
				.addVertex(0, 608)
				.addVertex(148, 656)
				.addVertex(0, 702)
				.done().buildArea();
	}

	@Override
	public Polygon getBounds() {
		return area.getBounds();
	}

	@Override
	public Iterator<Polygon> holesIterator() {
		return area.holesIterator();
	}

}
