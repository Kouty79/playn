package it.orbonauts.skafander.pcf.nullutils;

import playn.core.Connection;
import playn.core.GroupLayer;
import playn.core.Layer;
import playn.core.Layer.HasSize;
import playn.core.Mouse.LayerListener;
import playn.core.Pointer.Listener;
import playn.core.gl.GLShader;
import pythagoras.f.Point;
import pythagoras.f.Transform;

public class NullLayer implements HasSize, Nullable {

	private static final NullLayer INSTANCE = new NullLayer();

	private NullLayer() {
	}

	public static NullLayer get() {
		return INSTANCE;
	}

	@Override
	public void destroy() {
	}

	@Override
	public boolean destroyed() {
		return false;
	}

	@Override
	public GroupLayer parent() {
		return null;
	}

	@Override
	public Transform transform() {
		return null;
	}

	@Override
	public boolean visible() {
		return false;
	}

	@Override
	public Layer setVisible(boolean visible) {
		return this;
	}

	@Override
	public boolean interactive() {
		return false;
	}

	@Override
	public Layer setInteractive(boolean interactive) {
		return this;
	}

	@Override
	public float alpha() {
		return 0;
	}

	@Override
	public Layer setAlpha(float alpha) {
		return this;
	}

	@Override
	public float originX() {
		return 0;
	}

	@Override
	public float originY() {
		return 0;
	}

	@Override
	public Layer setOrigin(float x, float y) {
		return this;
	}

	@Override
	public float depth() {
		return 0;
	}

	@Override
	public Layer setDepth(float depth) {
		return this;
	}

	@Override
	public float tx() {
		return 0;
	}

	@Override
	public float ty() {
		return 0;
	}

	@Override
	public Layer setTx(float tx) {
		return this;
	}

	@Override
	public Layer setTy(float ty) {
		return this;
	}

	@Override
	public Layer setTranslation(float x, float y) {
		return this;
	}

	@Override
	public float scaleX() {
		return 1;
	}

	@Override
	public float scaleY() {
		return 1;
	}

	@Override
	public Layer setScale(float scale) {
		return this;
	}

	@Override
	public Layer setScaleX(float scaleX) {
		return this;
	}

	@Override
	public Layer setScaleY(float scaleY) {
		return this;
	}

	@Override
	public Layer setScale(float scaleX, float scaleY) {
		return this;
	}

	@Override
	public float rotation() {
		return 0;
	}

	@Override
	public Layer setRotation(float angle) {
		return this;
	}

	@Override
	public Layer hitTest(Point p) {
		return null;
	}

	@Override
	public Layer hitTestDefault(Point p) {
		return null;
	}

	@Override
	public Layer setHitTester(HitTester tester) {
		return this;
	}

	@Override
	public Connection addListener(Listener pointerListener) {
		return null;
	}

	@Override
	public Connection addListener(LayerListener touchListener) {
		return null;
	}

	@Override
	public Connection addListener(playn.core.Touch.LayerListener touchListener) {
		return null;
	}

	@Override
	public Layer setShader(GLShader shader) {
		return this;
	}

	@Override
	public float width() {
		return 0;
	}

	@Override
	public float height() {
		return 0;
	}

	@Override
	public float scaledWidth() {
		return 0;
	}

	@Override
	public float scaledHeight() {
		return 0;
	}

	@Override
	public boolean isNull() {
		return true;
	}

	@Override
	public Layer setTint(int tint) {
		return this;
	}

	@Override
	public int tint() {
		return 0;
	}
}
