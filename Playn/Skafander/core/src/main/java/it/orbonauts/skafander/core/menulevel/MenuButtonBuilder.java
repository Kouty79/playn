package it.orbonauts.skafander.core.menulevel;

import static playn.core.PlayN.graphics;
import it.orbonauts.skafander.pcf.utils.ColorUtils;
import playn.core.Canvas;
import playn.core.CanvasImage;
import playn.core.Layer;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import playn.core.TextLayout;

public class MenuButtonBuilder {

	private TextLayout textLayout;
	private int color;
	private int bkgColor;
	private int w;
	private int h;

	public MenuButtonBuilder setText(TextLayout textLayout) {
		this.textLayout = textLayout;
		return this;
	}

	public MenuButtonBuilder setColor(int color) {
		this.color = color;
		return this;
	}

	public MenuButtonBuilder setBkgColor(int bkgColor) {
		this.bkgColor = bkgColor;
		return this;
	}

	public MenuButtonBuilder setWidth(int w) {
		this.w = w;
		return this;
	}

	public MenuButtonBuilder setHeight(int h) {
		this.h = h;
		return this;
	}

	public Layer build() {
		return createButtonLayer();
	}

	private Layer createButtonLayer() {
		final CanvasImage image = graphics().createImage(w, h);
		Layer layer = graphics().createImageLayer(image);
		drawButton(image.canvas(), 1);

		layer.addListener(new Pointer.Adapter() {

			@Override
			public void onPointerEnd(Event event) {
				drawButton(image.canvas(), 1);
			}

			@Override
			public void onPointerStart(Event event) {
				drawButton(image.canvas(), 0.8f);
			}
		});

		return layer;
	}

	private void drawButton(Canvas canvas, float brightness) {
		float txtDx = (w - textLayout.width()) / 2;
		float txtDy = (h - textLayout.height()) / 2;

		int borderColor = ColorUtils.adjustBrithness(bkgColor, 0.9f);
		int fillColor = ColorUtils.adjustBrithness(bkgColor, brightness);

		float radius = 8;

		canvas
				.setFillColor(borderColor)
				.fillRoundRect(0, 0, w, h, radius)
				.setFillColor(fillColor)
				.fillRoundRect(2, 2, w - 4, h - 4, radius)
				.setFillColor(color)
				.translate(txtDx, txtDy)
				.fillText(textLayout, 0, 0)
				.translate(-txtDx, -txtDy);
	}

}
