package it.orbonauts.skafander.core.character;

public class IdentityPerspectiveScaler implements PerspectiveScaler {

	private static final IdentityPerspectiveScaler INSTANCE = new IdentityPerspectiveScaler();

	private IdentityPerspectiveScaler() {
	}

	public static IdentityPerspectiveScaler identityPerspectiveScaler() {
		return INSTANCE;
	}

	@Override
	public void updatePosition(float x, float y) {
	}

	@Override
	public float getScaleW() {
		return 1;
	}

	@Override
	public float getScaleH() {
		return 1;
	}

	@Override
	public float getScaleSpeed() {
		return 1;
	}

}
