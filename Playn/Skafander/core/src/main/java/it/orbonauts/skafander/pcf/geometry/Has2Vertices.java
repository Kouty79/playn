package it.orbonauts.skafander.pcf.geometry;

public interface Has2Vertices {

	Vertex getV1();

	Vertex getV2();

}
