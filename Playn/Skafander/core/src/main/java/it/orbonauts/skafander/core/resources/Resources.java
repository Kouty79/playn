package it.orbonauts.skafander.core.resources;

public class Resources {

	private Resources() {
	}

	public static final class Menu {
		private Menu() {
		}

		public static Resource MENU_ICON = new Resource() {

			@Override
			public int getWidth() {
				return 256;
			}

			@Override
			public int getHeight() {
				return 256;
			}

			@Override
			public String getURI() {
				return "images/gear.png";
			}
		};

	}

	public static final class Character {
		private Character() {
		}

		public static SpriteResource LR_SPRITE_SHEET = new SpriteResource() {

			@Override
			public int getNumImages() {
				return 14;
			}

			@Override
			public int getWidth() {
				return 624;
			}

			@Override
			public int getHeight() {
				return 450;
			}

			@Override
			public int getSpriteHeight() {
				return 150;
			}

			@Override
			public int getSpriteWidth() {
				return 104;
			}

			@Override
			public String getURI() {
				return "images/gb_walk.png";
			}
		};

		public static SpriteResource BH_SPRITE_SHEET = new SpriteResource() {

			@Override
			public int getNumImages() {
				return 6;
			}

			@Override
			public int getWidth() {
				return 486;
			}

			@Override
			public int getHeight() {
				return 144;
			}

			@Override
			public int getSpriteHeight() {
				return 144;
			}

			@Override
			public int getSpriteWidth() {
				return 80;
			}

			@Override
			public String getURI() {
				return "images/gb_behind_2.png";
			}
		};

		public static SpriteResource FR_SPRITE_SHEET = new SpriteResource() {

			@Override
			public int getNumImages() {
				return 8;
			}

			@Override
			public int getWidth() {
				return 700;
			}

			@Override
			public int getHeight() {
				return 141;
			}

			@Override
			public int getSpriteHeight() {
				return 141;
			}

			@Override
			public int getSpriteWidth() {
				return 85;
			}

			@Override
			public String getURI() {
				return "images/gb_front_2.png";
			}
		};

	}

	public static final class Inventory {
		private Inventory() {
		}

		public static Resource MENU_ICON = new Resource() {

			@Override
			public int getWidth() {
				return 110;
			}

			@Override
			public int getHeight() {
				return 180;
			}

			@Override
			public String getURI() {
				return "images/SkafanderIco.png";
			}
		};

	}

	public static final class NewtonLevel {
		private NewtonLevel() {
		}

		public static Resource BACKGROUND_IMG = new Resource() {

			@Override
			public int getWidth() {
				return 1019;
			}

			@Override
			public String getURI() {
				return "images/schema_newton.png";
			}

			@Override
			public int getHeight() {
				return 722;
			}
		};

		public static Resource CLOUDS = new Resource() {

			@Override
			public int getWidth() {
				return 318;
			}

			@Override
			public String getURI() {
				return "images/cartoon_cloud.png";
			}

			@Override
			public int getHeight() {
				return 162;
			}
		};

		public static Resource GRASS_BACK = new Resource() {

			@Override
			public int getWidth() {
				return 2238;
			}

			@Override
			public String getURI() {
				return "images/erba1.png";
			}

			@Override
			public int getHeight() {
				return 435;
			}
		};

		public static Resource GRASS_FRONT = new Resource() {

			@Override
			public int getWidth() {
				return 2238;
			}

			@Override
			public String getURI() {
				return "images/erba2.png";
			}

			@Override
			public int getHeight() {
				return 435;
			}
		};

	}

	public static final class DeChiricoLevel {
		private DeChiricoLevel() {
		}

		public static Resource BACKGROUND_IMG = new Resource() {

			@Override
			public int getWidth() {
				return 1545;
			}

			@Override
			public String getURI() {
				return "images/DeChirico.jpg";
			}

			@Override
			public int getHeight() {
				return 1231;
			}
		};

		public static Resource HANDSHAKE_IMG = new Resource() {

			@Override
			public int getWidth() {
				return 121;
			}

			@Override
			public String getURI() {
				return "images/handshake.png";
			}

			@Override
			public int getHeight() {
				return 201;
			}
		};
	}

}
