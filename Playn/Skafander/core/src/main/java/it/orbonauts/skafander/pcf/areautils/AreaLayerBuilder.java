package it.orbonauts.skafander.pcf.areautils;

import static playn.core.PlayN.graphics;
import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Rectangle;
import it.orbonauts.skafander.pcf.geometry.utils.GeometryUtils;
import it.orbonauts.skafander.pcf.nullutils.NullRenderer;
import playn.core.ImmediateLayer;
import playn.core.ImmediateLayer.Renderer;

public class AreaLayerBuilder {

	public static StepA areaLayerBuilder() {
		return new InternalBuilder();
	}

	public interface StepA {

		StepB setArea(Area area);

	}

	public interface StepB {

		StepC withNoRenderer();

		StepC withRenderer(ImmediateLayer.Renderer renderer);

		StepRenderer withAreaRenderer(int fill);
	}

	public interface StepRenderer {

		StepB setStrokeColor(int stroke);

		StepC done();
	}

	public interface StepC {

		StepC setClipped(boolean clipped);

		StepC setCustomClip(Rectangle bounds);

		StepC withAreaHitTester(float precision);

		Result build();

	}

	public interface Result {

		ImmediateLayer getLayer();

		ImmediateLayer.Renderer getRenderer();

		Rectangle getBounds();

	}

	private static class InternalBuilder implements StepA, StepB, StepRenderer, StepC {

		private Area area;
		private Renderer renderer;
		private int fill;
		private Integer stroke;
		private boolean useAreaRenderer;
		private Rectangle bounds;
		private boolean clipped;
		private boolean useAreaHitTester;
		private float precision;

		@Override
		public StepB setArea(Area area) {
			this.area = area;
			bounds = new GeometryUtils().bounds(area);
			clipped = true;

			return this;
		}

		@Override
		public StepC withRenderer(Renderer renderer) {
			this.renderer = renderer;
			return this;
		}

		@Override
		public StepRenderer withAreaRenderer(int fill) {
			useAreaRenderer = true;
			this.fill = fill;
			return this;
		}

		@Override
		public StepC withNoRenderer() {
			renderer = NullRenderer.nullRenderer();
			return this;
		}

		@Override
		public StepB setStrokeColor(int stroke) {
			this.stroke = stroke;
			return this;
		}

		@Override
		public StepC done() {
			return this;
		}

		@Override
		public StepC setClipped(boolean clipped) {
			this.clipped = clipped;
			if (clipped) {
				bounds = new GeometryUtils().bounds(area);
			} else {
				bounds = null;
			}

			return this;
		}

		@Override
		public StepC setCustomClip(Rectangle bounds) {
			this.bounds = bounds;
			return this;
		}

		@Override
		public StepC withAreaHitTester(float precision) {
			this.useAreaHitTester = true;
			this.precision = precision;
			return this;
		}

		@Override
		public Result build() {
			if (useAreaRenderer) {
				renderer = new AreaRenderer(area, fill, stroke, bounds);
			}

			ImmediateLayer layer;
			if (clipped) {
				layer = graphics().createImmediateLayer((int) bounds.getWidth(), (int) bounds.getHeight(), renderer);
				float dx = (float) bounds.getV1().getX();
				float dy = (float) bounds.getV1().getY();
				layer.setTranslation(dx, dy);
			} else {
				layer = graphics().createImmediateLayer(renderer);
			}

			if (useAreaHitTester) {
				layer.setHitTester(new AreaHitTester(area, new GeometryUtils(precision)));
			}

			return new ResultImpl(layer, renderer, bounds);
		}

	}
}
