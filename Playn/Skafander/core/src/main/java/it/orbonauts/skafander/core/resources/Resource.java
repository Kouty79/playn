package it.orbonauts.skafander.core.resources;

public interface Resource {

	String getURI();

	int getWidth();

	int getHeight();
}
