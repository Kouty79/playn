package it.orbonauts.skafander.pcf.pointer;

public class PointerManager {

	public static void register() {
		PointerManagerImpl.pointerManager().register();
	}

	public static GlobalPointerManager globalPointerManager() {
		return PointerManagerImpl.pointerManager();
	}

	public static DragDropManager dragDropManager() {
		return PointerManagerImpl.pointerManager();
	}
}
