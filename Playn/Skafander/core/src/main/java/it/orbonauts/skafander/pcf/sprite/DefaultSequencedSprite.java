package it.orbonauts.skafander.pcf.sprite;

import playn.core.Image;

public class DefaultSequencedSprite implements SequencedSprite {

	private final Sprite delegate;

	public DefaultSequencedSprite(Sprite delegate) {
		this.delegate = delegate;
	}

	@Override
	public SpriteSequenceIterator iterator(SpriteSequence sequence) {
		return this.new SpriteSequenceIteratorImpl(sequence);
	}

	@Override
	public void setCurrentIndex(int index) {
		delegate.setCurrentIndex(index);
	}

	@Override
	public int getCurrentIndex() {
		return delegate.getCurrentIndex();
	}

	@Override
	public Image getCurrentImage() {
		return delegate.getCurrentImage();
	}

	@Override
	public int getSize() {
		return delegate.getSize();
	}

	private class SpriteSequenceIteratorImpl implements SpriteSequenceIterator {

		private final SpriteSequence sequence;
		private int index;

		private SpriteSequenceIteratorImpl(SpriteSequence sequence) {
			this.sequence = sequence;
			this.index = sequence.from() - 1;
		}

		@Override
		public void next() {
			index = getValueInBounds(index + 1);
			setCurrentIndex(index);
		}

		private int getValueInBounds(int value) {
			if (value > sequence.to()) {
				return sequence.from();
			} else if (value < sequence.from()) {
				return sequence.to();
			}

			return value;
		}

	}
}
