package it.orbonauts.skafander.pcf.pointer;

import playn.core.Pointer.Event;

public interface Draggable extends HasBoundedLayer {

	void onDragEnter(Event event);

	void onDragLeave(Event event);

}
