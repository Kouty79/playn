package it.orbonauts.skafander.pcf.geometry.graph.impl;

import it.orbonauts.skafander.pcf.geometry.graph.Node;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


class BreadthFirstIterator<T> implements Iterator<Node<T>> {

	private Iterator<DefaultNode<T>> currentIterator;
	private List<DefaultNode<T>> nextLevel;

	public BreadthFirstIterator(DefaultNode<T> root) {
		ArrayList<DefaultNode<T>> rootList = new ArrayList<DefaultNode<T>>();
		rootList.add(root);
		currentIterator = rootList.iterator();
		nextLevel = new ArrayList<DefaultNode<T>>();
	}

	@Override
	public boolean hasNext() {
		return currentIterator.hasNext() || !nextLevel.isEmpty();
	}

	@Override
	public DefaultNode<T> next() {
		DefaultNode<T> retNode = null;

		if (currentIterator.hasNext()) {
			retNode = currentIterator.next();
			nextLevel.addAll(retNode.getChildren());
		} else if (!nextLevel.isEmpty()) {
			currentIterator = nextLevel.iterator();
			nextLevel = new ArrayList<DefaultNode<T>>();

			return next();
		}

		return retNode;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Cannot remove with BreadthFirstIterator, use DepthFirstIterator");
	}

}
