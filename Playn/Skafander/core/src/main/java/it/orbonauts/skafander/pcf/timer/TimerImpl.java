package it.orbonauts.skafander.pcf.timer;

import it.orbonauts.skafander.pcf.DynamicElement;
import it.orbonauts.skafander.pcf.impl.AbstractAnimation;

class TimerImpl extends AbstractAnimation implements DynamicElement, Timer {

	private final int delay;
	private boolean started;
	private boolean stopped;
	private long startTime;
	private final TimerTask task;

	public TimerImpl(TimerTask task, int delayMillis) {
		this.delay = delayMillis;
		this.task = task;
		started = false;
		stopped = false;
	}

	@Override
	public void start() {
		startTime = System.currentTimeMillis();
		started = true;
	}

	@Override
	public void stop() {
		stopped = true;
		destroy();
	}

	@Override
	public void update(float delta) {
		if (started && !stopped && (System.currentTimeMillis() - startTime > delay)) {
			task.run();
			this.destroy();
		}
	}

	@Override
	public void paint(float alpha) {
	}

}
