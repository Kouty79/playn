package it.orbonauts.skafander.core.character;

import it.orbonauts.skafander.pcf.sprite.SpriteSequence;

public enum WalkingState implements SpriteSequence {

	MOVING_RIGHT(0, 5),
	MOVING_LEFT(6, 11),
	IDLE_RIGHT(12, 12),
	IDLE_LEFT(13, 13),
	MOVING_BEHIND(14, 19),
	MOVING_FRONT(20, 27);

	private int from;
	private int to;

	private WalkingState(int from, int to) {
		this.from = from;
		this.to = to;
	}

	@Override
	public int from() {
		return from;
	}

	@Override
	public int to() {
		return to;
	}

}
