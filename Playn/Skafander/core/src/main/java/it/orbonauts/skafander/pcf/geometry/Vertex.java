package it.orbonauts.skafander.pcf.geometry;

public interface Vertex {

	double getX();

	double getY();

}
