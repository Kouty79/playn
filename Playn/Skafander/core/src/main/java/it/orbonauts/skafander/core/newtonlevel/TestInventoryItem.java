package it.orbonauts.skafander.core.newtonlevel;

import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import it.orbonauts.skafander.core.inventory.AbstractInventoryItem;
import it.orbonauts.skafander.core.inventory.Inventory;
import it.orbonauts.skafander.core.inventory.InventoryItem;
import it.orbonauts.skafander.pcf.pointer.Draggable;
import it.orbonauts.skafander.pcf.pointer.PointerManager;

import java.util.Random;

import playn.core.Color;
import playn.core.Layer.HasSize;
import playn.core.Pointer.Event;
import playn.core.SurfaceLayer;

public class TestInventoryItem extends AbstractInventoryItem implements InventoryItem {

	private final SurfaceLayer layer;
	private final Random random;
	private final String label;
	private final Inventory inventory;
	private int color;
	private final int w = 100;
	private final int h = 150;

	public TestInventoryItem(String label, Inventory inventory) {
		this.label = label;
		this.inventory = inventory;
		random = new Random();

		color = Color.rgb(random.nextInt(255), random.nextInt(255), random.nextInt(255));
		layer = graphics().createSurfaceLayer(w, h);
		redraw();

		PointerManager.dragDropManager().registerElement(this);
	}

	@Override
	public void onDragEnter(Event event) {
		log().debug("onDragEnter");
	}

	@Override
	public void onDragLeave(Event event) {
		log().debug("onDragLeave ");
		moveBack();
	}

	@Override
	public boolean acceptsDraggable(Draggable toBeDropped) {
		return toBeDropped instanceof TestInventoryItem;
	}

	@Override
	public void onDrop(Event event, Draggable dropped) {
		log().debug(this + " onDrop: " + dropped);

		TestInventoryItem droppedItem = (TestInventoryItem) dropped;
		inventory.removeItem(droppedItem);

		int da = Color.alpha(droppedItem.color);
		int dr = Color.red(droppedItem.color);
		int dg = Color.green(droppedItem.color);
		int db = Color.blue(droppedItem.color);

		int ca = Color.alpha(this.color);
		int cr = Color.red(this.color);
		int cg = Color.green(this.color);
		int cb = Color.blue(this.color);

		int a = (da + ca) / 2;
		int r = (dr + cr) / 2;
		int g = (dg + cg) / 2;
		int b = (db + cb) / 2;

		this.color = Color.argb(a, r, g, b);
		redraw();
	}

	private void redraw() {
		layer.surface()
				.clear()
				.setFillColor(color)
				.fillRect(0, 0, w, h);
	}

	@Override
	public HasSize getLayer() {
		return layer;
	}

	@Override
	public String toString() {
		return label;
	}
}
