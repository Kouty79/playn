package it.orbonauts.skafander.pcf.pointer.listener;

import playn.core.Pointer.Event;
import playn.core.Pointer.Listener;

class PointerListenerDelegator implements Listener {

	private Listener delegate;

	public void onPointerStart(Event event) {
		delegate.onPointerStart(event);
	}

	public void onPointerEnd(Event event) {
		delegate.onPointerEnd(event);
	}

	public void onPointerDrag(Event event) {
		delegate.onPointerDrag(event);
	}

	public void onPointerCancel(Event event) {
		delegate.onPointerCancel(event);
	}

	public PointerListenerDelegator(Listener delegate) {
		this.delegate = delegate;
	}

}
