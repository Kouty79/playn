package it.orbonauts.skafander.pcf.utils;

import playn.core.Image;
import playn.core.Pattern;
import playn.core.Surface;
import playn.core.gl.GLShader;

public class VectorSurface implements Surface {

	private Surface original;
	private float scaleX;
	private float scaleY;

	public VectorSurface() {
		scaleX = 1;
		scaleY = 1;
	}

	public Surface init(Surface original) {
		this.original = original;
		return this;
	}

	@Override
	public float width() {
		return original.width() / scaleX;
	}

	@Override
	public float height() {
		return original.height() / scaleY;
	}

	@Override
	public Surface save() {
		original.save();
		return this;
	}

	@Override
	public Surface restore() {
		original.restore();
		return this;
	}

	@Override
	public Surface translate(float x, float y) {
		original.translate(floatScaleX(x), floatScaleY(y));
		return this;
	}

	@Override
	public Surface scale(float sx, float sy) {
		this.scaleX = sx;
		this.scaleY = sy;
		return this;
	}

	@Override
	public Surface rotate(float radians) {
		original.rotate(radians);
		return this;
	}

	@Override
	public Surface transform(float m11, float m12, float m21, float m22, float dx, float dy) {
		throw new UnsupportedOperationException("Not yet implemented.");
	}

	@Override
	public Surface setTransform(float m11, float m12, float m21, float m22, float dx, float dy) {
		throw new UnsupportedOperationException("Not yet implemented.");
	}

	@Override
	public Surface setAlpha(float alpha) {
		original.setAlpha(alpha);
		return this;
	}

	@Override
	public Surface setFillColor(int color) {
		original.setFillColor(color);
		return this;
	}

	@Override
	public Surface setFillPattern(Pattern pattern) {
		original.setFillPattern(pattern);
		return this;
	}

	@Override
	public Surface setShader(GLShader shader) {
		original.setShader(shader);
		return this;
	}

	@Override
	public Surface clear() {
		original.clear();
		return this;
	}

	@Override
	public Surface drawImage(Image image, float dx, float dy) {
		original.drawImage(image, floatScaleX(dx), floatScaleY(dy));
		return this;
	}

	@Override
	public Surface drawImage(Image image, float dx, float dy, float dw, float dh) {
		original.drawImage(image, floatScaleX(dx), floatScaleY(dy), floatScaleX(dw), floatScaleY(dh));
		return this;
	}

	@Override
	public Surface drawImage(Image image, float dx, float dy, float dw, float dh, float sx, float sy, float sw, float sh) {
		return this;
	}

	@Override
	public Surface drawImageCentered(Image image, float dx, float dy) {
		original.drawImageCentered(image, floatScaleX(dx), floatScaleY(dy));
		return this;
	}

	@Override
	public Surface drawLine(float x0, float y0, float x1, float y1, float width) {
		original.drawLine(floatScaleX(x0), floatScaleY(y0), floatScaleX(x1), floatScaleY(y1), floatScaleW(width));
		return this;
	}

	@Override
	public Surface fillRect(float x, float y, float width, float height) {
		original.fillRect(floatScaleX(x), floatScaleY(y), floatScaleW(width), floatScaleH(height));
		return this;
	}

	@Override
	public Surface fillTriangles(float[] xys, int[] indices) {
		throw new UnsupportedOperationException("Not yet implemented.");
	}

	@Override
	public Surface fillTriangles(float[] xys, float[] sxys, int[] indices) {
		throw new UnsupportedOperationException("Not yet implemented.");
	}

	@Override
	public Surface setTint(int tint) {
		return original.setTint(tint);
	}

	private float floatScale(float coordinate, float scale) {
		return coordinate * scale;
	}

	private float floatScale(float coordinate, float scale, float min) {
		float result = floatScale(coordinate, scale);
		if (result < min) {
			result = min;
		}

		return result;
	}

	private float floatScaleH(float y) {
		return floatScale(y, scaleY, 1);
	}

	private float floatScaleW(float x) {
		return floatScale(x, scaleX, 1);
	}

	private float floatScaleX(float x) {
		return floatScale(x, scaleX);
	}

	private float floatScaleY(float y) {
		return floatScale(y, scaleY);
	}

}
