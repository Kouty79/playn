package it.orbonauts.skafander.pcf.pointer;

import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import static playn.core.PlayN.pointer;
import it.orbonauts.skafander.pcf.nullutils.Nullable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import playn.core.Layer;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import playn.core.Pointer.Listener;
import pythagoras.f.Point;

class PointerManagerImpl implements GlobalPointerManager, DragDropManager {

	private static final PointerManagerImpl INSTANCE = new PointerManagerImpl();
	private final List<DroppableTarget> targets;
	private final List<Draggable> draggables;
	private final List<Pointer.Listener> listeners;

	private PointerManagerImpl() {
		targets = new ArrayList<DroppableTarget>();
		draggables = new ArrayList<Draggable>();
		listeners = new ArrayList<Pointer.Listener>();
	}

	static PointerManagerImpl pointerManager() {
		return INSTANCE;
	}

	void register() {
		pointer().setListener(pointerManager().new InternalListener());
	}

	@Override
	public void addGlobalListener(Pointer.Listener listener) {
		if (listener == null) {
			throw new NullPointerException("Listener cannot be null.");
		}
		pointerManager().listeners.add(listener);
	}

	@Override
	public boolean removeGlobalListener(Pointer.Listener listener) {
		return pointerManager().listeners.remove(listener);
	}

	@Override
	public void registerElement(Draggable element) {
		draggables.add(element);
	}

	@Override
	public void registerElement(DroppableTarget element) {
		targets.add(element);
	}

	@Override
	public void registerElement(DragAndDroppable element) {
		draggables.add(element);
		targets.add(element);
	}

	@Override
	public void unregisterElement(HasBoundedLayer element) {
		draggables.remove(element);
		targets.remove(element);
	}

	private Draggable findDraggable(Layer layer) {
		removeDestroyed(draggables);
		return findByLayer(draggables, layer, NullDraggable.get());
	}

	private DroppableTarget findTarget(Layer layer) {
		removeDestroyed(targets);
		return findByLayer(targets, layer, NullDroppableTarget.get());
	}

	private <T extends HasBoundedLayer> T findByLayer(List<T> elements, Layer layer, T notFoundReplace) {
		for (T interactive : elements) {
			if (interactive.getLayer().equals(layer)) {
				return interactive;
			}
		}

		return notFoundReplace;
	}

	private void removeDestroyed(List<? extends HasBoundedLayer> elements) {
		Iterator<? extends HasBoundedLayer> elementsIt = elements.iterator();
		while (elementsIt.hasNext()) {
			HasBoundedLayer element = elementsIt.next();
			if (element.getLayer().destroyed()) {
				log().debug("Unregistering " + element);
				elementsIt.remove();
			}
		}

	}

	private class InternalListener implements Pointer.Listener {

		private Draggable dragged = NullDraggable.get();

		@Override
		public void onPointerStart(Event event) {
			for (Listener listener : getListeners()) {
				listener.onPointerStart(event);
			}
		}

		@Override
		public void onPointerCancel(Event event) {
			for (Listener listener : getListeners()) {
				listener.onPointerCancel(event);
			}
		}

		@Override
		public void onPointerDrag(Event event) {
			for (Listener listener : getListeners()) {
				listener.onPointerDrag(event);
			}

			if (dragged instanceof Nullable) {
				dragged = findDraggable(hitTest(event));
				dragged.onDragEnter(event);
			}
		}

		@Override
		public void onPointerEnd(Event event) {
			for (Listener listener : getListeners()) {
				listener.onPointerEnd(event);
			}

			dragged.onDragLeave(event);

			DroppableTarget target = findTarget(hitTest(event));
			if (target.getLayer() == dragged.getLayer()) {
				dragged.getLayer().setVisible(false);
				target = findTarget(hitTest(event));
				dragged.getLayer().setVisible(true);
			}

			if (target.acceptsDraggable(dragged)) {
				target.onDrop(event, dragged);
			}

			dragged = NullDraggable.get();

		}

		private Layer hitTest(Event event) {
			return graphics().rootLayer().hitTest(new Point(event.x(), event.y()));
		}

		private List<Listener> getListeners() {
			return new ArrayList<Pointer.Listener>(listeners);
		}

	}

}
