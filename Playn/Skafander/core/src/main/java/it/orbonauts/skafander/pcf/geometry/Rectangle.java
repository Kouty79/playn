package it.orbonauts.skafander.pcf.geometry;

public interface Rectangle extends Has2Vertices {

	double getWidth();

	double getHeight();
}
