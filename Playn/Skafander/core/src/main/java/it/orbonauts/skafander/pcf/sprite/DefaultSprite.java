package it.orbonauts.skafander.pcf.sprite;

import java.util.ArrayList;
import java.util.List;

import playn.core.Image;

public class DefaultSprite implements Sprite {

	private final List<Image> spriteImages;
	private int currentIndex;

	public DefaultSprite(List<Image> spriteImages) {
		this.spriteImages = new ArrayList<Image>(spriteImages);
	}

	@Override
	public void setCurrentIndex(int index) {
		int result = index % spriteImages.size();

		if (result < 0) {
			result = spriteImages.size() + result;
		}

		this.currentIndex = result;
	}

	@Override
	public int getCurrentIndex() {
		return currentIndex;
	}

	@Override
	public Image getCurrentImage() {
		return spriteImages.get(currentIndex);
	}

	@Override
	public int getSize() {
		return getSpriteImages().size();
	}

	protected List<Image> getSpriteImages() {
		return spriteImages;
	}

}
