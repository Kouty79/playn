package it.orbonauts.skafander.core.newtonlevel;

import static it.orbonauts.skafander.core.storage.SchemaManager.schemaManager;
import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import it.orbonauts.skafander.core.AbstractSkafanderRoom;
import it.orbonauts.skafander.core.Skafander;
import it.orbonauts.skafander.core.character.Character;
import it.orbonauts.skafander.core.character.CharacterMovingEvent;
import it.orbonauts.skafander.core.character.CharacterMovingEventHandler;
import it.orbonauts.skafander.core.character.WalkingState;
import it.orbonauts.skafander.core.event.GameEventBus;
import it.orbonauts.skafander.core.inventory.Inventory;
import it.orbonauts.skafander.core.newtonlevel.cloudriddle.CloudRiddle;
import it.orbonauts.skafander.core.newtonlevel.cloudriddle.ResultListener;
import it.orbonauts.skafander.core.pointer.InAreaWalkingClickListener;
import it.orbonauts.skafander.core.resources.Resources;
import it.orbonauts.skafander.pcf.animation.Animation;
import it.orbonauts.skafander.pcf.areautils.AreaLayerBuilder;
import it.orbonauts.skafander.pcf.areautils.AreaLayerBuilder.Result;
import it.orbonauts.skafander.pcf.areautils.AreaRenderer;
import it.orbonauts.skafander.pcf.asset.ProgressAssets;
import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Vertex;
import it.orbonauts.skafander.pcf.geometry.impl.DefaultVertex;
import it.orbonauts.skafander.pcf.impl.HelperFactory;
import it.orbonauts.skafander.pcf.pointer.PointerManager;
import it.orbonauts.skafander.pcf.pointer.listener.GlobalClickHelper;
import it.orbonauts.skafander.pcf.timer.TimerManager;
import it.orbonauts.skafander.pcf.timer.TimerTask;
import it.orbonauts.skafander.pcf.utils.ColorUtils;
import playn.core.Assets;
import playn.core.Color;
import playn.core.GroupLayer;
import playn.core.Image;
import playn.core.ImageLayer;
import playn.core.ImmediateLayer;
import playn.core.Layer;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import playn.core.Pointer.Listener;

import com.google.web.bindery.event.shared.HandlerRegistration;

public class NewtonLevel extends AbstractSkafanderRoom {

	private static final int IMAGE_COUNT = 4 - 2;

	private GroupLayer roomLayer;
	private GroupLayer backLayer;
	private GroupLayer midLayer;
	private GroupLayer frontLayer;
	private ProgressAssets progressAssets;
	private Listener globalPointerListener;
	private Area walkableArea;
	private HandlerRegistration movingRegistration;
	private Character character;
	private Animation grassAnimation1;
	private Animation grassAnimation2;
	private NewtonLevelState state;

	@Override
	public void initRoom(GroupLayer gameLayer) {
		walkableArea = new NewtonWalkableArea();
		progressAssets = buildProgressAssets(IMAGE_COUNT);

		initRoomLayers();
		initBackgroundLayer();
		initDynamicLayer();
		// initFrontLayer();
		gameLayer.add(roomLayer);
		initPointer();
		inventoryTest();

		getGame().getMenu().setIconVisible(true);
	}

	private void inventoryTest() {
		Inventory inventory = getGame().getInventoryPresenter().getInventory();
		if (inventory.size() != 3) {
			inventory.clear();
			String[] labels = { "A", "B", "C" };
			for (int i = 0; i < 3; i++) {
				inventory.addItem(new TestInventoryItem(labels[i], inventory), i == 2);
			}
		}
	}

	private void initDynamicLayer() {
		ImmediateLayer layer = AreaLayerBuilder.areaLayerBuilder()
				.setArea(walkableArea)
				.withAreaRenderer(Color.argb(64, 128, 128, 255))
				.done()
				.setClipped(true)
				.build().getLayer();

		midLayer.add(layer);

		initHighlightingTestArea();

		character = new Character();
		character.setWalkableArea(walkableArea);
		midLayer.add(character.getLayer());
		character.placeTo(20, 580, WalkingState.IDLE_RIGHT);

		initClouds();
	}

	private void initHighlightingTestArea() {
		final Area highlightingTestArea = new HighlightingTestArea();
		Result result = AreaLayerBuilder.areaLayerBuilder()
				.setArea(highlightingTestArea)
				.withAreaRenderer(Color.argb(0, 128, 255, 128))
				.done()
				.setClipped(true)
				.withAreaHitTester(Skafander.getPrecision())
				.build();

		final AreaRenderer areaRenderer = (AreaRenderer) result.getRenderer();

		midLayer.add(result.getLayer());
		result.getLayer().addListener(new Pointer.Adapter() {

			@Override
			public void onPointerStart(Event event) {
				log().debug("onPointerStart");
			}

		});

		movingRegistration = GameEventBus.get().addHandler(CharacterMovingEvent.TYPE,
				new CharacterMovingEventHandler() {

					@Override
					public void onCharacterMoving(CharacterMovingEvent event) {
						Vertex v1 = new DefaultVertex(event.getX(), event.getY());
						Vertex v2 = getGeometryUtils().nearestPoint(highlightingTestArea, v1);

						final float nearestDinstance = 150f;
						final double distance = Math.max(getGeometryUtils().distance(v1, v2) - nearestDinstance, 0);

						int alpha = (int) (Math.max(50 - distance, 0) / 50 * 200);

						int fillColor = ColorUtils.setAplha(areaRenderer.getFillColor(), alpha);
						areaRenderer.setFillColor(fillColor);
						areaRenderer.redraw();
					}
				});
	}

	private void initClouds() {
		GroupLayer cloudsLayer = graphics().createGroupLayer();
		Image cloud = getProgressAssets().getImage(Resources.NewtonLevel.CLOUDS.getURI());

		ImageLayer[] clouds = new ImageLayer[3];
		for (int i = 0; i < clouds.length; i++) {
			clouds[i] = graphics().createImageLayer(cloud);
			clouds[i].setScale(0.5f);
			cloudsLayer.addAt(clouds[i], 0, i * 50);
		}
		midLayer.addAt(cloudsLayer, 50, 50);

		new CloudRiddle(clouds[0], clouds[1], clouds[2], getState().isCloudRiddleSolved(), new ResultListener() {

			@Override
			public void onWin(CloudRiddle riddle) {
				getState().setCloudRiddleSolved(true);
				getState().save();
			}

			@Override
			public void onFail(final CloudRiddle riddle) {
				TimerManager.get().scheduleDelayed(1000, new TimerTask() {

					@Override
					public void run() {
						riddle.reset();
					}
				}).start();
			}
		});
	}

	private void initBackgroundLayer() {
		Image bgImage = getProgressAssets().getImage(Resources.NewtonLevel.BACKGROUND_IMG.getURI());
		backLayer.add(createImageLayer(bgImage));
	}

	private void initFrontLayer() {
		Layer grassBack = GrassLayerHelper
				.create(Resources.NewtonLevel.GRASS_BACK, getProgressAssets(), getGame().getGameModel());
		frontLayer.addAt(grassBack, 0, getGame().getGameModel().getHeight());
		grassAnimation1 = HelperFactory.createAnimantion(new GrassAnimation(grassBack, 0.0010f, 0f));

		Layer grassFront = GrassLayerHelper
				.create(Resources.NewtonLevel.GRASS_FRONT, getProgressAssets(), getGame().getGameModel());
		frontLayer.addAt(grassFront, 0, getGame().getGameModel().getHeight());
		grassAnimation2 = HelperFactory.createAnimantion(new GrassAnimation(grassFront, 0.0010f, 20f));
	}

	private void initRoomLayers() {
		roomLayer = graphics().createGroupLayer();
		getGame().getScaleHelper().scaleLayer(roomLayer);

		backLayer = graphics().createGroupLayer();
		roomLayer.add(backLayer);

		midLayer = graphics().createGroupLayer();
		roomLayer.add(midLayer);

		frontLayer = graphics().createGroupLayer();
		roomLayer.add(frontLayer);
	}

	private void initPointer() {
		globalPointerListener = new GlobalClickHelper(
				new Pointer.Adapter(),
				new InAreaWalkingClickListener(character, walkableArea, getGame().getScaleHelper(), getGeometryUtils()));

		PointerManager.globalPointerManager().addGlobalListener(globalPointerListener);
	}

	private ImageLayer createImageLayer(Image bgImage) {
		return graphics().createImageLayer(bgImage);
	}

	private Assets getProgressAssets() {
		return progressAssets;
	}

	@Override
	protected void destroyRoom(GroupLayer gamLayer) {
		PointerManager.globalPointerManager().removeGlobalListener(globalPointerListener);
		roomLayer.destroy();
		movingRegistration.removeHandler();
		// grassAnimation1.destroy();
		// grassAnimation2.destroy();
	}

	private NewtonLevelState getState() {
		if (state == null) {
			state = schemaManager().getCurrentStorage().read(new NewtonLevelState());
		}

		return state;
	}

}
