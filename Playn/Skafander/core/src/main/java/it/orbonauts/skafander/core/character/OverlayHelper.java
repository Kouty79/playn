package it.orbonauts.skafander.core.character;

import static playn.core.PlayN.graphics;
import it.orbonauts.skafander.core.resources.Resources;
import playn.core.Canvas.Composite;
import playn.core.CanvasImage;
import playn.core.Color;
import playn.core.Image;
import playn.core.ImageLayer;
import playn.core.Layer.HasSize;

public class OverlayHelper implements CharacterRenderer {

	private static final int TRANSPARENT = Color.argb(0, 0, 0, 0);

	private ImageLayer layer;
	private CanvasImage canvasImage;
	private Image sprite;
	private int overlayColor;
	private int currentColor;

	public OverlayHelper(int overlayColor) {
		this.overlayColor = overlayColor;
		this.currentColor = overlayColor;
	}

	@Override
	public void init() {
		float w = Resources.Character.LR_SPRITE_SHEET.getSpriteWidth();
		float h = Resources.Character.LR_SPRITE_SHEET.getSpriteHeight();
		float dx = w / 2;
		float dy = h;

		canvasImage = graphics().createImage(w, h);
		layer = graphics().createImageLayer(canvasImage);

		layer.setOrigin(dx, dy);
	}

	@Override
	public HasSize getLayer() {
		return layer;
	}

	@Override
	public void update(Image currentSprite) {
		sprite = currentSprite;
		redraw();
	}

	public void setOverlayColor(int overlayColor) {
		this.currentColor = overlayColor;
		this.overlayColor = overlayColor;
	}

	public void setOverlayVisible(boolean overlayVisible) {
		currentColor = (overlayVisible) ? overlayColor : TRANSPARENT;
		redraw();
	}

	private void redraw() {
		if (sprite != null) {
			canvasImage.canvas()
					.clear()
					.setCompositeOperation(Composite.DST_ATOP)
					.setFillColor(currentColor)
					.fillRect(0, 0,
							sprite.width() - 1,
							sprite.height() - 1)
					.drawImage(sprite, 0, 0);

		}
	}
}
