package it.orbonauts.skafander.pcf.areautils;

import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Vertex;
import it.orbonauts.skafander.pcf.geometry.impl.DefaultVertex;
import it.orbonauts.skafander.pcf.geometry.utils.GeometryUtils;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import playn.core.Pointer.Listener;

public class AreaPointerListener implements Pointer.Listener {

	private final Area area;
	private final GeometryUtils geometryUtils;
	private final Listener listener;

	public AreaPointerListener(Pointer.Listener listener, Area area, GeometryUtils geometryUtils) {
		this.area = area;
		this.geometryUtils = geometryUtils;
		this.listener = listener;
	}

	@Override
	public void onPointerStart(Event event) {
		if (eventInArea(event)) {
			listener.onPointerStart(event);
		}
	}

	@Override
	public void onPointerEnd(Event event) {
		if (eventInArea(event)) {
			listener.onPointerEnd(event);
		}
	}

	@Override
	public void onPointerDrag(Event event) {
		if (eventInArea(event)) {
			listener.onPointerDrag(event);
		}

	}

	@Override
	public void onPointerCancel(Event event) {
		if (eventInArea(event)) {
			listener.onPointerCancel(event);
		}
	}

	private Vertex createVertex(Event event) {
		return new DefaultVertex(event.x(), event.y());
	}

	private boolean eventInArea(Event event) {
		return geometryUtils.pointInArea(area, createVertex(event));
	}

}
