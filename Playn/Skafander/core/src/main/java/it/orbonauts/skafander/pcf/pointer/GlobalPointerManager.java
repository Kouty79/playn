package it.orbonauts.skafander.pcf.pointer;

import playn.core.Pointer;

public interface GlobalPointerManager {

	void addGlobalListener(Pointer.Listener listener);

	boolean removeGlobalListener(Pointer.Listener listener);

}
