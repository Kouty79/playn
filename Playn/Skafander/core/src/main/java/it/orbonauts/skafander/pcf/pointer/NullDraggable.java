package it.orbonauts.skafander.pcf.pointer;

import it.orbonauts.skafander.pcf.nullutils.NullLayer;
import it.orbonauts.skafander.pcf.nullutils.Nullable;
import playn.core.Layer.HasSize;
import playn.core.Pointer.Event;

class NullDraggable implements Draggable, Nullable {

	private static final NullDraggable INSTANCE = new NullDraggable();

	public static NullDraggable get() {
		return INSTANCE;
	}

	private NullDraggable() {
	}

	@Override
	public boolean isNull() {
		return true;
	}

	@Override
	public HasSize getLayer() {
		return NullLayer.get();
	}

	@Override
	public void onDragEnter(Event event) {
	}

	@Override
	public void onDragLeave(Event event) {
	}

	@Override
	public String toString() {
		return this.getClass().getName();
	}

}
