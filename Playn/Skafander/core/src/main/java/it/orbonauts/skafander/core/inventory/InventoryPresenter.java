package it.orbonauts.skafander.core.inventory;

import playn.core.Layer;
import playn.core.Layer.HasSize;
import playn.core.Pointer;

public interface InventoryPresenter {

	Layer getInventoryLayer();

	Inventory getInventory();

	void setVisible(boolean visible);

	boolean isVisible();

	HasSize getMenuButtonLayer();

	void setMenuButtonListener(Pointer.Listener listener);

	void addVisibilityChangeListener(OnVisibilityChangeListener listener);

	boolean removeVisibilityChangeListener(OnVisibilityChangeListener listener);

	interface OnVisibilityChangeListener {

		void onVisivilityChanged(InventoryPresenter presenter, boolean newVisibility);
	}
}
