package it.orbonauts.skafander.core.inventory;

import static it.orbonauts.skafander.pcf.impl.HelperFactory.createMotionHelper;
import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import it.orbonauts.skafander.core.resources.Resource;
import it.orbonauts.skafander.core.resources.Resources;
import it.orbonauts.skafander.pcf.motion.MotionHelper;
import it.orbonauts.skafander.pcf.pointer.PointerManager;
import it.orbonauts.skafander.pcf.pointer.listener.ClickOutsideHelper;
import it.orbonauts.skafander.pcf.scaling.GameScaleHelper;

import java.util.List;

import playn.core.Color;
import playn.core.Connection;
import playn.core.GroupLayer;
import playn.core.Image;
import playn.core.Layer;
import playn.core.Layer.HasSize;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import playn.core.Pointer.Listener;
import playn.core.Surface;
import playn.core.SurfaceLayer;

class InventoryView implements InventoryPresenterImpl.Display {

	private boolean visible;
	private final GroupLayer itemsLayer;
	private final GroupLayer inventoryLayer;
	private final MotionHelper motionHelper;
	private final float INVENTORY_HEIGHT;
	private final float INVENTORY_WIDTH;
	private final GameScaleHelper scaleHelper;
	private Connection menuButtonConnection;
	private HasSize menuButtonLayer;

	private final Listener clickOutsideListener;

	InventoryView(GameScaleHelper scaleHelper, final InventoryPresenter presenter) {
		inventoryLayer = graphics().createGroupLayer();
		itemsLayer = graphics().createGroupLayer();
		scaleHelper.scaleLayer(inventoryLayer);
		this.scaleHelper = scaleHelper;

		visible = false;
		INVENTORY_HEIGHT = scaleHelper.gameHeight();
		INVENTORY_WIDTH = scaleHelper.gameWidth() / 10;

		SurfaceLayer backgroundLayer = graphics().createSurfaceLayer(INVENTORY_WIDTH, INVENTORY_HEIGHT);
		drawInvetory(backgroundLayer.surface());

		inventoryLayer.add(backgroundLayer);
		inventoryLayer.setTranslation(scaleHelper.scaledWidth(), 0);
		inventoryLayer.add(itemsLayer);
		initMenuButton();

		motionHelper = createMotionHelper(inventoryLayer, 0.8f);

		backgroundLayer.addListener(new Pointer.Adapter());
		clickOutsideListener = new ClickOutsideHelper(backgroundLayer, new ClickOutsideHelper.OnClickOutsideListener() {

			@Override
			public void onClickOutside(Event event) {
				log().debug("onClickOutside");
				presenter.setVisible(false);
			}
		});
	}

	@Override
	public Layer getLayer() {
		return this.inventoryLayer;
	}

	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			float x = scaleHelper.scaleX(scaleHelper.gameWidth() - INVENTORY_WIDTH);
			motionHelper.moveTo(x, 0);
			PointerManager.globalPointerManager().addGlobalListener(clickOutsideListener);
		} else {
			motionHelper.moveTo(scaleHelper.scaledWidth(), 0);
			PointerManager.globalPointerManager().removeGlobalListener(clickOutsideListener);
		}
		this.visible = visible;
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public void setMenuButtonListener(Listener listener) {
		if (menuButtonConnection != null) {
			menuButtonConnection.disconnect();
		}
		menuButtonConnection = getMenuButtonLayer().addListener(listener);
	}

	private void drawInvetory(Surface surface) {
		surface
				.setFillColor(Color.argb(192, 0, 0, 0))
				.fillRect(0, 0, INVENTORY_WIDTH, INVENTORY_HEIGHT);
	}

	@Override
	public void setItems(List<HasSize> items) {
		log().debug("setItems");

		itemsLayer.clear();
		float padding = 3;
		float shift = 0;
		for (HasSize hasSize : items) {
			float scale = INVENTORY_WIDTH / Math.max(hasSize.width(), hasSize.height());
			hasSize.transform().setScale(scale, scale);

			float tx = (INVENTORY_WIDTH - hasSize.width() * scale) / 2;
			hasSize.setTranslation(tx, shift);

			itemsLayer.add(hasSize);
			shift += INVENTORY_WIDTH + padding;
		}
	}

	private void initMenuButton() {
		Resource resource = Resources.Inventory.MENU_ICON;
		Image image = assets().getImage(resource.getURI());
		menuButtonLayer = graphics().createImageLayer(image);

		float width = INVENTORY_WIDTH * 0.6f;
		float height = width * 1.64f;
		float scaleX = width / resource.getWidth();
		float scaleY = height / resource.getHeight();
		menuButtonLayer.setScale(scaleX, scaleY);

		inventoryLayer.addAt(menuButtonLayer, (INVENTORY_WIDTH - width) / 2, INVENTORY_HEIGHT - height);
	}

	@Override
	public HasSize getMenuButtonLayer() {
		return menuButtonLayer;
	}
}
