package it.orbonauts.skafander.core.storage;

import static playn.core.PlayN.json;
import static playn.core.PlayN.storage;

import java.util.ArrayList;
import java.util.List;

import playn.core.Json.Array;

public class SchemaManager {

	private static final SchemaManager INSTANCE = new SchemaManager();
	private static final String SCHEMAS_KEY = "schemas";
	private static final String CURRENT_SCHEMA_KEY = "currentSchema";

	private String currentSchema;
	private StorageManager currentManager;

	private SchemaManager() {
		currentSchema = retrieveCurrentSchema();
		currentManager = new StorageManager(currentSchema);
	}

	public static SchemaManager schemaManager() {
		return INSTANCE;
	}

	public List<String> listSchemas() {
		String json = storage().getItem(SCHEMAS_KEY);
		Array schemas = json().parseArray(json);
		return arrayToList(schemas);
	}

	public String getCurrentSchema() {
		return currentSchema;
	}

	public void setCurrentSchema(String schema) {
		currentSchema = schema;
		saveCurrentSchema();
		currentManager = new StorageManager(currentSchema);
	}

	public StorageManager getCurrentStorage() {
		return currentManager;
	}

	public boolean dropSchema(String schema) {
		return false;
	}

	private List<String> arrayToList(Array array) {
		List<String> list = new ArrayList<String>();

		for (int i = 0; i < array.length(); i++) {
			String element = array.getString(i);
			list.add(element);
		}

		return list;
	}

	private String retrieveCurrentSchema() {
		String currentSchema = storage().getItem(CURRENT_SCHEMA_KEY);
		return (currentSchema == null) ? "" : currentSchema;
	}

	private void saveCurrentSchema() {
		storage().setItem(CURRENT_SCHEMA_KEY, currentSchema);
	}
}
