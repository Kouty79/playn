package it.orbonauts.skafander.core.event;

import com.google.web.bindery.event.shared.SimpleEventBus;

public class GameEventBus extends SimpleEventBus {

	private static final GameEventBus INSTANCE = new GameEventBus();

	private GameEventBus() {
	}

	public static GameEventBus get() {
		return INSTANCE;
	}
}
