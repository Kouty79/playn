package it.orbonauts.skafander.core.newtonlevel;

import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Polygon;
import it.orbonauts.skafander.pcf.geometry.impl.AreaBuilder;

import java.util.Iterator;

class NewtonWalkableArea implements Area {

	private Area area;

	public NewtonWalkableArea() {
		area = new AreaBuilder()
				.buildBounds()
				.addVertex(0, 274 * 2)
				.addVertex(76 * 2, 285 * 2)
				.addVertex(194 * 2, 283 * 2)
				.addVertex(248 * 2, 300 * 2)
				.addVertex(248 * 2, 332 * 2)
				.addVertex(140 * 2, 313 * 2)
				.addVertex(126 * 2, 310 * 2)
				.addVertex(4 * 2, 309 * 2)
				.done()
				.buildArea();
	}

	@Override
	public Polygon getBounds() {
		return area.getBounds();
	}

	@Override
	public Iterator<Polygon> holesIterator() {
		return area.holesIterator();
	}

}
