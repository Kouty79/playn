package it.orbonauts.skafander.pcf;

public interface DynamicElement {

	void update(float delta);

	void paint(float alpha);

}
