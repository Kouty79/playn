package it.orbonauts.skafander.pcf.scaling;

import playn.core.Layer;

public interface ScaleHelper {

	void scaleLayer(Layer layer);

	float scaleXComponent();

	float scaleYComponent();

	float scaleX(double x);

	float scaleY(double y);

	float unscaleX(double x);

	float unscaleY(double y);

	float scaleW(double w);

	float scaleH(double h);

}
