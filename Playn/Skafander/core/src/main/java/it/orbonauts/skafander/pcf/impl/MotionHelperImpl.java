package it.orbonauts.skafander.pcf.impl;

import it.orbonauts.skafander.pcf.motion.MotionHelper;
import playn.core.Layer;

class MotionHelperImpl extends AbstractAnimation implements MotionHelper {

	private static final float PRECISION = 0.4f;

	private final Layer layer;
	private MotionListener motionListener;

	private float speed;
	private float x;
	private float y;
	private float toX;
	private float toY;
	private float endX;
	private float endY;
	private float vxPrj;
	private float vyPrj;
	private float dySignum;
	private float dxSignum;
	private boolean stopUpdate;
	private boolean mustMove;

	MotionHelperImpl(Layer layer, float speed) {
		this.layer = layer;
		this.mustMove = false;
		this.stopUpdate = false;
		this.speed = speed;
		this.motionListener = new NullMotionListener();
	}

	@Override
	public void stop() {
		updatePosition();
		mustMove = false;
	}

	@Override
	public void moveTo(float x, float y) {
		updatePosition();
		endX = x;
		endY = y;
		double atan = Math.atan2(y - this.y, x - this.x);
		vxPrj = (float) (Math.cos(atan));
		vyPrj = (float) (Math.sin(atan));
		dxSignum = Math.signum(nearZero(this.x - endX));
		dySignum = Math.signum(nearZero(this.y - endY));

		mustMove = true;
		stopUpdate = false;
	}

	@Override
	public void update(float delta) {
		if (mustMove && !stopUpdate) {
			toX = this.x + getSpeed() * vxPrj * delta;
			toY = this.y + getSpeed() * vyPrj * delta;

			float newDxSignum = Math.signum(nearZero(toX - endX));
			float newDySignum = Math.signum(nearZero(toY - endY));

			if (newDxSignum != dxSignum || newDySignum != dySignum) {
				// log().debug("Finished dxs " + dxSignum + ", dys" + dySignum + ", ndxs: " + newDxSignum + ", ndys: "
				// + newDySignum);
				toX = endX;
				toY = endY;
				stopUpdate = true;
			}

			motionListener.onMoving(x, y);
		}
	}

	@Override
	public void paint(float alpha) {
		if (mustMove) {
			float x = (toX * (1f - alpha)) + (this.x * alpha);
			float y = (toY * (1f - alpha)) + (this.y * alpha);
			translate(x, y);
		}
	}

	private static float nearZero(float x) {
		if (Math.abs(x) <= PRECISION) {
			return 0f;
		}

		return x;
	}

	private void translate(float x, float y) {
		mustMove = !floatEquals(x, endX) || !floatEquals(y, endY);
		if (!mustMove) {
			layer.setTranslation(endX, endY);
			this.x = endX;
			this.y = endY;
			this.motionListener.onMoving(endX, endY);
		} else {
			layer.setTranslation(x, y);
			this.x = x;
			this.y = y;
		}
	}

	private boolean floatEquals(float actual, float expected) {
		return nearZero(actual - expected) == 0f;
	}

	private void updatePosition() {
		this.x = layer.tx();
		this.y = layer.ty();
	}

	@Override
	public void setMotionListener(MotionListener listener) {
		this.motionListener = listener;
	}

	@Override
	public void setSpeed(float speed) {
		this.speed = speed;
	}

	@Override
	public float getSpeed() {
		return this.speed;
	}

	@Override
	public boolean isMoving() {
		return mustMove;
	}

}
