package it.orbonauts.skafander.pcf.impl;

import it.orbonauts.skafander.pcf.geometry.Path;
import it.orbonauts.skafander.pcf.geometry.Vertex;
import it.orbonauts.skafander.pcf.motion.MotionHelper;
import it.orbonauts.skafander.pcf.motion.PathFollower;

import java.util.Collections;
import java.util.Iterator;

class PathFolloweImpl implements PathFollower {

	private final MotionHelper delegate;
	private Iterator<Vertex> verticesIterator;

	public PathFolloweImpl(MotionHelper delegate) {
		this.delegate = delegate;
		this.verticesIterator = Collections.<Vertex> emptyList().iterator();
		AnimationManager.get().unregisterElement(delegate);
		AnimationManager.get().registerElement(this);
	}

	@Override
	public void followPath(Path path) {
		verticesIterator = path.verticesIterator();
		moveToNextVertex();
	}

	private void moveToNextVertex() {
		if (verticesIterator.hasNext()) {
			Vertex v = verticesIterator.next();
			moveTo((float) v.getX(), (float) v.getY());
		}
	}

	@Override
	public boolean isMoving() {
		return delegate.isMoving() || verticesIterator.hasNext();
	}

	@Override
	public void update(float delta) {
		delegate.update(delta);
	}

	@Override
	public void paint(float alpha) {
		delegate.paint(alpha);
		if (!delegate.isMoving()) {
			moveToNextVertex();
		}
	}

	@Override
	public void stop() {
		delegate.stop();
	}

	@Override
	public void moveTo(float x, float y) {
		delegate.moveTo(x, y);
	}

	@Override
	public void setSpeed(float speed) {
		delegate.setSpeed(speed);
	}

	@Override
	public float getSpeed() {
		return delegate.getSpeed();
	}

	@Override
	public void destroy() {
		delegate.destroy();
	}

	@Override
	public void setMotionListener(MotionListener listener) {
		delegate.setMotionListener(listener);
	}
}
