package it.orbonauts.skafander.pcf.pointer;

import playn.core.Pointer.Event;

public interface DroppableTarget extends HasBoundedLayer {

	boolean acceptsDraggable(Draggable toBeDropped);

	void onDrop(Event event, Draggable dropped);
}
