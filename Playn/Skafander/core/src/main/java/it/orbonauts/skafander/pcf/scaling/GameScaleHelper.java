package it.orbonauts.skafander.pcf.scaling;

public interface GameScaleHelper extends ScaleHelper {

	float scaledWidth();

	float scaledHeight();

	int gameWidth();

	int gameHeight();

}
