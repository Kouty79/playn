package it.orbonauts.skafander.pcf.impl;

import static playn.core.PlayN.graphics;
import it.orbonauts.skafander.pcf.DynamicElement;
import it.orbonauts.skafander.pcf.animation.Animation;
import it.orbonauts.skafander.pcf.motion.MotionHelper;
import it.orbonauts.skafander.pcf.motion.PathFollower;
import it.orbonauts.skafander.pcf.scaling.GameScaleHelper;
import it.orbonauts.skafander.pcf.scaling.ScaleHelper;
import playn.core.Layer;

public final class HelperFactory {

	public static Animation createAnimantion(DynamicElement element) {
		return new AnimationImpl(element);
	}

	public static MotionHelper createMotionHelper(Layer layer, float initialSpeed) {
		return new MotionHelperImpl(layer, initialSpeed);
	}

	public static PathFollower createPathFollower(MotionHelper delegate) {
		return new PathFolloweImpl(delegate);
	}

	public static ScaleHelper createScaleHelper(float sx, float sy) {
		return new ScaleHelperImpl(sx, sy);
	}

	public static GameScaleHelper createGameScaleHelper(int gameWidth, int gameHeight) {
		float scaleX = (float) graphics().width() / gameWidth;
		float scaleY = (float) graphics().height() / gameHeight;

		return new GameScaleHelperImpl(createScaleHelper(scaleX, scaleY), gameWidth, gameHeight);
	}

}
