package it.orbonauts.skafander.pcf.pointer.listener;

import playn.core.Layer;
import playn.core.Layer.HasSize;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import playn.core.Pointer.Listener;

public class LayerClickHelper extends PointerListenerDelegator implements Pointer.Listener {

	private final OnClickListener clickListener;
	private final HasSize layer;

	public LayerClickHelper(Listener delegate, HasSize layer, OnClickListener clickListener) {
		super(delegate);
		this.clickListener = clickListener;
		this.layer = layer;
	}

	@Override
	public void onPointerEnd(Event event) {
		super.onPointerEnd(event);
		if (isClick(event)) {
			clickListener.onClick(event);
		}
	}

	private boolean isClick(Event event) {
		return Layer.Util.hitTest(layer, event.x(), event.y());
	}

	public interface OnClickListener {

		void onClick(Event event);
	}
}
