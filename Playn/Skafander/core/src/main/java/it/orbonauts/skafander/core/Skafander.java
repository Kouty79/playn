package it.orbonauts.skafander.core;

import it.orbonauts.skafander.core.gamemenu.Menu;
import it.orbonauts.skafander.core.gamemenu.MenuImpl;
import it.orbonauts.skafander.core.inventory.InventoryPresenter;
import it.orbonauts.skafander.core.inventory.InventoryPresenterImpl;
import it.orbonauts.skafander.core.menulevel.MenuLevel;
import it.orbonauts.skafander.pcf.impl.PointAndClickGame;
import it.orbonauts.skafander.pcf.pointer.listener.LayerClickHelper;
import playn.core.Game;
import playn.core.Layer.HasSize;
import playn.core.Pointer;
import playn.core.Pointer.Event;

public class Skafander extends PointAndClickGame implements Game {

	private static final int RATE_INTERVAL = 5000;
	private static final float PRECISION = 0.1f;

	private long paintTimer;
	private long updateTimer;
	private int paintCount;
	private int updateCount;

	private InventoryPresenter inventoryPresenter;
	private Menu menu;

	public Skafander() {
		super(new SkafanderGameModel());
	}

	@Override
	public void init() {
		super.init();
		GameLayers.getGameLayers();
		initInventory();
		enterRoom(new MenuLevel());

		paintCount = 0;
		updateCount = 0;
		paintTimer = System.currentTimeMillis();
		updateTimer = System.currentTimeMillis();
	}

	@Override
	public void paint(float alpha) {
		super.paint(alpha);

		paintCount++;
		boolean reset = printRate(paintTimer, paintCount, "Paint rate: ");
		if (reset) {
			paintCount = 0;
			paintTimer = System.currentTimeMillis();
		}
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		updateCount++;
		boolean reset = printRate(updateTimer, updateCount, "Update rate: ");
		if (reset) {
			updateCount = 0;
			updateTimer = System.currentTimeMillis();
		}
	}

	@Override
	public int updateRate() {
		return 25;
	}

	private boolean printRate(long timer, long count, String msg) {
		long now = System.currentTimeMillis();
		long deltaT = (now - timer);
		if (deltaT >= RATE_INTERVAL) {
			// log().debug(msg + ((double) count / (deltaT / 1000)));
			return true;
		}

		return false;
	}

	private void initInventory() {
		inventoryPresenter = new InventoryPresenterImpl(getScaleHelper());
		inventoryPresenter.setMenuButtonListener(createMenuButtonClickHandler());
		GameLayers.getGameLayers().getInventoryLayer().add(inventoryPresenter.getInventoryLayer());
	}

	private LayerClickHelper createMenuButtonClickHandler() {
		HasSize menuButtonLayer = getInventoryPresenter().getMenuButtonLayer();

		return new LayerClickHelper(new Pointer.Adapter(), menuButtonLayer, new LayerClickHelper.OnClickListener() {

			@Override
			public void onClick(Event event) {
				getInventoryPresenter().setVisible(false);
				getMenu().setIconVisible(false);
				Skafander.this.enterRoom(new MenuLevel());
			}
		});
	}

	public InventoryPresenter getInventoryPresenter() {
		return inventoryPresenter;
	}

	public Menu getMenu() {
		if (menu == null) {
			menu = new MenuImpl(getScaleHelper(), getInventoryPresenter());
		}

		return menu;
	}

	public static float getPrecision() {
		return PRECISION;
	}

}
