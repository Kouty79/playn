package it.orbonauts.skafander.pcf.motion;

import it.orbonauts.skafander.pcf.animation.Animation;

public interface MotionHelper extends Animation {

	void stop();

	void moveTo(float x, float y);

	void setSpeed(float speed);

	float getSpeed();

	boolean isMoving();

	void setMotionListener(MotionListener listener);

	interface MotionListener {

		void onMoving(float x, float y);

	}
}
