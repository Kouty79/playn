package it.orbonauts.skafander.pcf.geometry.graph;

import java.util.Iterator;

public interface Graph<T> extends Iterable<Node<T>> {

	public Node<T> getRoot();

	public Iterator<Node<T>> depthFirstIterator();

	public Iterator<Node<T>> breadthFirstIterator();

	/**
	 * @return depthFirstIterator()
	 */
	@Override
	public Iterator<Node<T>> iterator();

	public Node<T> find(T element);

}
