package it.orbonauts.skafander.core.inventory;

import it.orbonauts.skafander.core.inventory.Inventory.InventoryObserver;
import it.orbonauts.skafander.pcf.pointer.listener.BringToFrontHelper;
import it.orbonauts.skafander.pcf.pointer.listener.DragHelper;
import it.orbonauts.skafander.pcf.scaling.GameScaleHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import playn.core.Connection;
import playn.core.Layer;
import playn.core.Layer.HasSize;
import playn.core.Pointer;
import playn.core.Pointer.Listener;

public class InventoryPresenterImpl implements InventoryPresenter, InventoryObserver {

	private final Inventory inventory;
	private final Display inventoryView;
	private final Map<Layer, Connection> listeners;
	private final GameScaleHelper scaleHelper;
	private final List<OnVisibilityChangeListener> visibilityChangeListeners;

	public InventoryPresenterImpl(GameScaleHelper scaleHelper) {
		this.scaleHelper = scaleHelper;
		listeners = new HashMap<Layer, Connection>();
		inventory = new Inventory(this);
		inventoryView = new InventoryView(scaleHelper, this);
		visibilityChangeListeners = new ArrayList<InventoryPresenter.OnVisibilityChangeListener>();
	}

	@Override
	public Layer getInventoryLayer() {
		return inventoryView.getLayer();
	}

	@Override
	public void setVisible(boolean visible) {
		inventoryView.setVisible(visible);
		notifyVisibilityChanged();
	}

	@Override
	public boolean isVisible() {
		return inventoryView.isVisible();
	}

	@Override
	public Inventory getInventory() {
		return inventory;
	}

	@Override
	public void onItemAdded(List<InventoryItem> newList, InventoryItem item, boolean updateView) {
		if (!listeners.containsKey(item.getLayer())) {
			DragHelper listener = new DragHelper(scaleHelper, new BringToFrontHelper(new Pointer.Adapter()));
			Connection listenerConn = item.getLayer().addListener(listener);
			listeners.put(item.getLayer(), listenerConn);
		}

		if (updateView) {
			updateView(newList);
		}
	}

	@Override
	public void onItemRemoved(List<InventoryItem> newList, InventoryItem item, boolean updateView) {
		if (listeners.containsKey(item.getLayer())) {
			listeners.get(item.getLayer()).disconnect();
			listeners.remove(item.getLayer());
		}

		if (updateView) {
			updateView(newList);
		}
	}

	@Override
	public void addVisibilityChangeListener(OnVisibilityChangeListener listener) {
		this.visibilityChangeListeners.add(listener);
	}

	@Override
	public boolean removeVisibilityChangeListener(OnVisibilityChangeListener listener) {
		return this.visibilityChangeListeners.remove(listener);
	}

	@Override
	public void setMenuButtonListener(Listener listener) {
		inventoryView.setMenuButtonListener(listener);
	}

	@Override
	public HasSize getMenuButtonLayer() {
		return inventoryView.getMenuButtonLayer();
	}

	private void updateView(List<InventoryItem> items) {
		List<HasSize> layers = new ArrayList<Layer.HasSize>();
		for (InventoryItem inventoryItem : items) {
			layers.add(inventoryItem.getLayer());
		}
		inventoryView.setItems(layers);
		for (InventoryItem item : items) {
			item.onPositionSet(item.getLayer().tx(), item.getLayer().ty());
		}
	}

	private void notifyVisibilityChanged() {
		for (OnVisibilityChangeListener listener : visibilityChangeListeners) {
			listener.onVisivilityChanged(this, isVisible());
		}
	}

	interface Display {

		void setVisible(boolean visible);

		boolean isVisible();

		Layer getLayer();

		void setItems(List<HasSize> items);

		HasSize getMenuButtonLayer();

		void setMenuButtonListener(Pointer.Listener listener);

	}

}
