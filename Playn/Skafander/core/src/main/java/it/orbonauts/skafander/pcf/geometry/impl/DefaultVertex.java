package it.orbonauts.skafander.pcf.geometry.impl;

import it.orbonauts.skafander.pcf.geometry.Vertex;

public final class DefaultVertex implements Vertex {

	private double x;
	private double y;

	public DefaultVertex(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public double getX() {
		return x;
	}

	@Override
	public double getY() {
		return y;
	}

	@Override
	public String toString() {
		return "(" + x + "; " + y + ")";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DefaultVertex other = (DefaultVertex) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

}
