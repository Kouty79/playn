package it.orbonauts.skafander.core.newtonlevel.cloudriddle;

import it.orbonauts.skafander.core.resources.Resources;
import playn.core.Layer;
import playn.core.Pointer;

public class CloudRiddle {

	private static enum State {
		PLAYING,
		WON,
		FAIL
	}

	private static final int UNIT = Resources.NewtonLevel.CLOUDS.getWidth() / 5;
	private final Block[] blocks;
	private State state;
	private int numMove;
	private final ResultListener listener;

	public CloudRiddle(Layer cloud1, Layer cloud2, Layer cloud3, boolean won, ResultListener listener) {
		this.listener = listener;
		blocks = new Block[3];
		blocks[0] = new Block(cloud1, 1, 0, 0, 3, UNIT);
		blocks[1] = new Block(cloud2, 2, 3, 1, 4, UNIT);
		blocks[2] = new Block(cloud3, 1, 3, 1, 4, UNIT);

		if (!won) {
			init();
		} else {
			initWon();
		}
	}

	private void init() {
		for (int i = 0; i < this.blocks.length; i++) {
			Block block = this.blocks[i];
			block.getLayer().addListener(new CloudDragListener(this, i));
			block.init();
		}
		this.state = State.PLAYING;
	}

	private void initWon() {
		for (int i = 0; i < this.blocks.length; i++) {
			Block block = this.blocks[i];
			block.getLayer().addListener(new Pointer.Adapter());
			block.win();
		}
		this.state = State.PLAYING;
	}

	public void reset() {
		for (int i = 0; i < this.blocks.length; i++) {
			this.blocks[i].reset();
		}
		this.numMove = 0;
		this.state = State.PLAYING;
	}

	boolean isPlayingState() {
		return this.state == State.PLAYING;
	}

	void win() {
		this.state = State.WON;
		listener.onWin(this);
	}

	void fail() {
		this.state = State.FAIL;
		listener.onFail(this);
	}

	boolean hasFailed() {
		return this.numMove >= 3;
	}

	boolean hasWon() {
		for (int i = 0; i < this.blocks.length; i++) {
			if (this.blocks[i].getPos() >= 1 && this.blocks[i].getPos() <= 2) {
				return false;
			}
		}

		return true;
	}

	void moveBlock(int blockIndex, int direction) {
		if (this.blocks[blockIndex].canMove(direction)) {
			this.numMove++;
			if (blockIndex - 1 >= 0) {
				this.blocks[blockIndex - 1].move(direction);
			}
			this.blocks[blockIndex].move(direction);
			if (blockIndex + 1 < this.blocks.length) {
				this.blocks[blockIndex + 1].move(direction);
			}
		}
	}

}
