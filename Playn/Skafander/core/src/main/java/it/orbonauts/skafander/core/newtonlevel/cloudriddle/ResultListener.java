package it.orbonauts.skafander.core.newtonlevel.cloudriddle;

public interface ResultListener {

	void onWin(CloudRiddle riddle);

	void onFail(CloudRiddle riddle);
}
