package it.orbonauts.skafander.core;

import static playn.core.PlayN.graphics;
import playn.core.GroupLayer;

public class GameLayers {

	private GroupLayer GAME_LAYER;
	private GroupLayer INVENTORY_LAYER;
	private GroupLayer WAIT_LAYER;
	private GroupLayer MENU_LAYER;

	private static GameLayers instance;

	private GameLayers() {
		init();
	}

	private void init() {
		GAME_LAYER = graphics().createGroupLayer();
		INVENTORY_LAYER = graphics().createGroupLayer();
		WAIT_LAYER = graphics().createGroupLayer();
		MENU_LAYER = graphics().createGroupLayer();

		graphics().rootLayer().add(GAME_LAYER);
		graphics().rootLayer().add(INVENTORY_LAYER);
		graphics().rootLayer().add(MENU_LAYER);
		graphics().rootLayer().add(WAIT_LAYER);
	}

	public static GameLayers getGameLayers() {
		if (instance == null) {
			instance = new GameLayers();
		}

		return instance;
	}

	public GroupLayer getGameLayer() {
		return GAME_LAYER;
	}

	public GroupLayer getInventoryLayer() {
		return INVENTORY_LAYER;
	}

	public GroupLayer getWaitLayer() {
		return WAIT_LAYER;
	}

	public GroupLayer getMenuLayer() {
		return MENU_LAYER;
	}

}
