package it.orbonauts.skafander.pcf.geometry.impl;

import it.orbonauts.skafander.pcf.geometry.Path;
import it.orbonauts.skafander.pcf.geometry.Segment;
import it.orbonauts.skafander.pcf.geometry.Vertex;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


public class DefaultPath implements Path {

	private List<Vertex> vertices;
	private double length;

	public DefaultPath(Collection<? extends Vertex> vertices, double length) {
		this.vertices = new ArrayList<Vertex>(vertices);
		this.length = length;
	}

	@Override
	public Iterator<Vertex> verticesIterator() {
		return new ImmutableIterator<Vertex>(vertices.iterator());
	}

	@Override
	public Iterator<Segment> segmentIterator() {
		return new SegmentIterator(verticesIterator());
	}

	private class SegmentIterator implements Iterator<Segment> {

		private Iterator<Vertex> vertexIterator;
		private Vertex lastVertex;

		public SegmentIterator(Iterator<Vertex> vertexIterator) {
			this.vertexIterator = vertexIterator;
			if (vertexIterator.hasNext()) {
				lastVertex = vertexIterator.next();
			}
		}

		@Override
		public boolean hasNext() {
			return vertexIterator.hasNext();
		}

		@Override
		public DefaultSegment next() {
			DefaultSegment segment = null;

			if (vertexIterator.hasNext()) {
				Vertex next = vertexIterator.next();
				segment = new DefaultSegment(lastVertex, next);
				lastVertex = next;
			}

			return segment;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Removing a segment is not supported.");
		}

	}

	@Override
	public double length() {
		return length;
	}

}
