package it.orbonauts.skafander.pcf.pointer;

import playn.core.Layer.HasSize;

public interface HasBoundedLayer {

	HasSize getLayer();

}
