package it.orbonauts.skafander.core;

import it.orbonauts.skafander.pcf.GameModel;

public class SkafanderGameModel implements GameModel {

	public SkafanderGameModel() {
	}

	@Override
	public int getWidth() {
		return 1019;
	}

	@Override
	public int getHeight() {
		return 722;
	}

}
