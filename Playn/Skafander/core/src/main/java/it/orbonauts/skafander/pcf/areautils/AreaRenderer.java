package it.orbonauts.skafander.pcf.areautils;

import static playn.core.PlayN.graphics;
import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Rectangle;
import it.orbonauts.skafander.pcf.utils.PathUtils;
import playn.core.Canvas;
import playn.core.CanvasImage;
import playn.core.ImmediateLayer;
import playn.core.Path;
import playn.core.Surface;

public class AreaRenderer implements ImmediateLayer.Renderer {

	private final Area area;
	private final Rectangle areaBounds;
	private final CanvasImage canvasImage;
	private int fillColor;
	private Integer strokeColor;

	public AreaRenderer(Area area, int fillColor, Integer strokeColor, Rectangle areaBounds) {
		this.area = area;
		this.areaBounds = areaBounds;
		this.fillColor = fillColor;
		this.strokeColor = strokeColor;

		canvasImage = graphics().createImage((float) areaBounds.getWidth(), (float) areaBounds.getHeight());
		drawArea(canvasImage);
	}

	@Override
	public void render(Surface surface) {
		surface.drawImage(canvasImage, 0, 0);
	}

	public int getFillColor() {
		return fillColor;
	}

	public Integer getStrokeColor() {
		return strokeColor;
	}

	public void setFillColor(int fillColor) {
		this.fillColor = fillColor;
	}

	public void setStrokeColor(Integer strokeColor) {
		this.strokeColor = strokeColor;
	}

	public void redraw() {
		drawArea(canvasImage);
	}

	public Rectangle getAreaBounds() {
		return areaBounds;
	}

	private void drawArea(CanvasImage canvasImage) {
		Canvas canvas = canvasImage.canvas();

		float dx = (float) areaBounds.getV1().getX();
		float dy = (float) areaBounds.getV1().getY();

		Path bounds = PathUtils.createPath(canvas.createPath(), area.getBounds(), dx, dy);
		canvas
				.clear()
				.setStrokeWidth(2);

		if (getStrokeColor() != null) {
			canvas
					.setStrokeColor(getStrokeColor())
					.strokePath(bounds);
		}
		canvas
				.setFillColor(fillColor)
				.fillPath(bounds);
	}
}
