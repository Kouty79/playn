package it.orbonauts.skafander.pcf.pointer;

import it.orbonauts.skafander.pcf.nullutils.NullLayer;
import it.orbonauts.skafander.pcf.nullutils.Nullable;
import playn.core.Layer.HasSize;
import playn.core.Pointer.Event;

class NullDroppableTarget implements DroppableTarget, Nullable {

	private static final NullDroppableTarget INSTANCE = new NullDroppableTarget();

	public static final NullDroppableTarget get() {
		return INSTANCE;
	}

	private NullDroppableTarget() {
	}

	@Override
	public boolean isNull() {
		return true;
	}

	@Override
	public HasSize getLayer() {
		return NullLayer.get();
	}

	@Override
	public void onDrop(Event event, Draggable dropped) {
	}

	@Override
	public String toString() {
		return this.getClass().getName();
	}

	@Override
	public boolean acceptsDraggable(Draggable toBeDropped) {
		return false;
	}

}
