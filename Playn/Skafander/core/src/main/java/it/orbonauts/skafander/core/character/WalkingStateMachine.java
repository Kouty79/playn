package it.orbonauts.skafander.core.character;

public class WalkingStateMachine {

	private static final float MIN_DS = 3;
	private WalkingState previousState;
	private WalkingState currentState;
	private float lx;
	private float ly;

	public WalkingStateMachine(WalkingState initialState, float x, float y) {
		currentState = initialState;
		previousState = initialState;
		this.lx = x;
		this.ly = y;
	}

	public WalkingState update(float x, float y, boolean moving) {
		WalkingState nextState;
		nextState = (moving) ? getNextState(x, y) : getNextIdleState(x);

		updateCurrentState(nextState);
		updatePosition(x, y);

		return nextState;
	}

	private void updatePosition(float x, float y) {
		lx = x;
		ly = y;
	}

	private void updateCurrentState(WalkingState nextState) {
		previousState = currentState;
		currentState = nextState;
	}

	private WalkingState getNextIdleState(float x) {
		WalkingState nextState;

		switch (currentState) {
		case IDLE_LEFT:
			nextState = WalkingState.IDLE_LEFT;
			break;

		case IDLE_RIGHT:
			nextState = WalkingState.IDLE_RIGHT;
			break;

		case MOVING_LEFT:
			nextState = WalkingState.IDLE_LEFT;
			break;

		case MOVING_RIGHT:
			nextState = WalkingState.IDLE_RIGHT;
			break;

		case MOVING_FRONT:
		case MOVING_BEHIND:
			nextState = (x >= lx) ? WalkingState.IDLE_RIGHT : WalkingState.IDLE_LEFT;
			break;

		default:
			throw new RuntimeException("Case " + currentState + " is not yet implemented.");
		}

		return nextState;
	}

	private WalkingState getNextState(float x, float y) {
		if (!hasMoved(x, y)) {
			return currentState;
		}

		WalkingState result;

		if (Math.abs(x - lx) >= Math.abs(y - ly)) {

			if (x > lx) {
				result = WalkingState.MOVING_RIGHT;
			} else {
				result = WalkingState.MOVING_LEFT;
			}

		} else {
			if (y >= ly) {
				result = WalkingState.MOVING_FRONT;
			} else {
				result = WalkingState.MOVING_BEHIND;
			}
		}

		return result;
	}

	private boolean hasMoved(float x, float y) {
		return Math.abs(x - lx) > MIN_DS || Math.abs(y - ly) > MIN_DS;
	}

	public WalkingState getCurrentState() {
		return currentState;
	}

	public boolean isInState(WalkingState state) {
		return currentState == state;
	}

	public boolean stateChanged() {
		return currentState != previousState;
	}

}
