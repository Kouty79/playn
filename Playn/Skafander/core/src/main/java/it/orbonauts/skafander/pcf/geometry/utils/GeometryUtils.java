package it.orbonauts.skafander.pcf.geometry.utils;

import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Polygon;
import it.orbonauts.skafander.pcf.geometry.Rectangle;
import it.orbonauts.skafander.pcf.geometry.Segment;
import it.orbonauts.skafander.pcf.geometry.Vertex;
import it.orbonauts.skafander.pcf.geometry.impl.DefaultRectangle;
import it.orbonauts.skafander.pcf.geometry.impl.DefaultVertex;
import it.orbonauts.skafander.pcf.geometry.intersection.GeometryIntersection;

import java.util.Iterator;

public class GeometryUtils {

	private GeometryIntersection intersectUtil;

	public GeometryUtils() {
		intersectUtil = new GeometryIntersection();
	}

	public GeometryUtils(double precision) {
		intersectUtil = new GeometryIntersection(precision);
	}

	public double distance(Vertex v1, Vertex v2) {
		double dx = v1.getX() - v2.getX();
		double dy = v1.getY() - v2.getY();

		return Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * Let the point be C (Cx,Cy) and the line be AB (Ax,Ay) to (Bx,By). The length of the line segment AB is L: <br/>
	 * L= sqrt( (Bx-Ax)^2 + (By-Ay)^2 ) . <br/>
	 * Let P be the point of perpendicular projection of C onto AB. Let r be a parameter to indicate P's location along
	 * the line containing AB, with the following meaning: <br/>
	 * <ul>
	 * <li>r=0 P = A</li>
	 * <li>r=1 P = B</li>
	 * <li>r<0 P is on the backward extension of AB</li>
	 * <li>r>1 P is on the forward extension of AB</li>
	 * <li>0<r<1 P is interior to AB</li>
	 * </ul>
	 * Compute r with this: <br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Ay-Cy)(Ay-By)-(Ax-Cx)(Bx-Ax) <br/>
	 * r = ----------------------------- <br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L^2 <br/>
	 * The point P can then be found: <br/>
	 * <ul>
	 * <li>Px = Ax + r(Bx-Ax)</li>
	 * <li>Py = Ay + r(By-Ay)</li>
	 * </ul>
	 * And the distance from A to P = r*L.
	 * 
	 * @see <a href= 'http://www.exaflop.org/docs/cgafaq/cga1.html'>Comp.Graphics.Algorit h m s < / a >
	 * @param v
	 *            vertex
	 * @param s
	 *            segment
	 * @return I
	 */
	public PointSegmentDistance pointFromSegment(Vertex v, Segment segment) {
		double ay = segment.getV1().getY();
		double ax = segment.getV1().getX();
		double by = segment.getV2().getY();
		double bx = segment.getV2().getX();
		double cx = v.getX();
		double cy = v.getY();

		double l = distance(segment.getV1(), segment.getV2());
		double r = ((ay - cy) * (ay - by) - (ax - cx) * (bx - ax)) / (l * l);

		Vertex p = null;
		double px = ax + r * (bx - ax);
		double py = ay + r * (by - ay);

		p = new DefaultVertex(px, py);
		double distance = distance(p, v);

		return new PointSegmentDistance(distance, p, r);
	}

	public Vertex nearestPoint(Area area, Vertex point) {
		if (pointOutsideBounds(area, point)) {
			return nearestPoint(area.getBounds(), point);
		}

		Polygon hole = pointInHole(area, point);
		if (hole != null) {
			return nearestPoint(hole, point);
		}

		return point;
	}

	public Vertex nearestPoint(Polygon p, Vertex v) {
		Iterator<Segment> segmentIterator = p.segmentIterator();

		double minDist = Double.MAX_VALUE;
		Vertex nearestPolyPoint = null;
		while (segmentIterator.hasNext()) {
			Segment segment = segmentIterator.next();

			PointSegmentDistance pointFromSegment = pointFromSegment(v, segment);
			double r = pointFromSegment.getpLocation();
			double distance;
			if (r < 0) {
				distance = distance(segment.getV1(), v);
			} else if (r > 1) {
				distance = distance(segment.getV2(), v);
			} else {
				distance = pointFromSegment.getDistance();
			}

			if (distance < minDist) {
				if (r < 0) {
					nearestPolyPoint = segment.getV1();
				} else if (r > 1) {
					nearestPolyPoint = segment.getV2();
				} else {
					nearestPolyPoint = pointFromSegment.getProjection();
				}
				minDist = distance;
			}

		}

		return nearestPolyPoint;
	}

	public Rectangle bounds(Area area) {
		double minX = Double.MAX_VALUE;
		double minY = Double.MAX_VALUE;

		double maxX = -Double.MAX_VALUE;
		double maxY = -Double.MAX_VALUE;

		Iterator<Vertex> vIt = area.getBounds().verticesIterator();
		while (vIt.hasNext()) {
			Vertex vertex = vIt.next();

			minX = Math.min(minX, vertex.getX());
			maxX = Math.max(maxX, vertex.getX());

			minY = Math.min(minY, vertex.getY());
			maxY = Math.max(maxY, vertex.getY());
		}

		DefaultVertex v1 = new DefaultVertex(minX, minY);
		DefaultVertex v2 = new DefaultVertex(maxX, maxY);
		return new DefaultRectangle(v1, v2);
	}

	protected GeometryIntersection getIntersectUtil() {
		return intersectUtil;
	}

	public boolean pointInPolygon(Polygon polygon, Vertex point) {
		return getIntersectUtil().pointInPolygon(polygon, point);
	}

	public boolean pointInRectangle(Rectangle rectangle, Vertex point) {
		boolean inside = rectangle.getV1().getX() - getPrecision() <= point.getX()
				&& rectangle.getV2().getX() + getPrecision() >= point.getX()
				&& rectangle.getV1().getY() - getPrecision() <= point.getY()
				&& rectangle.getV2().getY() + getPrecision() >= point.getY();

		return inside;
	}

	public boolean pointInArea(Area area, Vertex point) {
		return !pointOutsideBounds(area, point) && pointInHole(area, point) == null;
	}

	public boolean pointOutsideBounds(Area area, Vertex point) {
		return !pointInPolygon(area.getBounds(), point);
	}

	public Polygon pointInHole(Area area, Vertex point) {
		Iterator<Polygon> holesIterator = area.holesIterator();
		while (holesIterator.hasNext()) {
			Polygon polygon = holesIterator.next();
			if (pointInPolygon(polygon, point)) {
				return polygon;
			}
		}

		return null;
	}

	public Double getPrecision() {
		return getIntersectUtil().getPrecision();
	}

}
