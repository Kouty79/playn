package it.orbonauts.skafander.pcf.impl;

class EmptyRoom extends AbstractRoom {

	@Override
	public void enterRoom() {
	}

	@Override
	public void leaveRoom() {
	}

	@Override
	public void update(float delta) {
	}

	@Override
	public void paint(float alpha) {
	}

}
