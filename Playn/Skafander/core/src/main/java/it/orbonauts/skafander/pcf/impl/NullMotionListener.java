package it.orbonauts.skafander.pcf.impl;

import it.orbonauts.skafander.pcf.motion.MotionHelper;

class NullMotionListener implements MotionHelper.MotionListener {

	@Override
	public void onMoving(float x, float y) {
	}

}
