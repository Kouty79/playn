package it.orbonauts.skafander.core.inventory;

import it.orbonauts.skafander.pcf.pointer.DragAndDroppable;

public interface InventoryItem extends DragAndDroppable {

	void onPositionSet(float x, float y);

}
