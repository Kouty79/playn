package it.orbonauts.skafander.pcf.geometry.shortestpath;

import it.orbonauts.skafander.pcf.geometry.Vertex;
import it.orbonauts.skafander.pcf.geometry.graph.impl.DefaultGraph;

public class VertexTree extends DefaultGraph<Vertex> {

	public VertexTree(Vertex root) {
		super(root);
	}

}
