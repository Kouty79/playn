package it.orbonauts.skafander.pcf.impl;

import it.orbonauts.skafander.pcf.DynamicElement;

public class AnimationImpl extends AbstractAnimation {

	private final DynamicElement element;

	public AnimationImpl(DynamicElement element) {
		this.element = element;
	}

	@Override
	public void update(float delta) {
		element.update(delta);
	}

	@Override
	public void paint(float alpha) {
		element.paint(alpha);
	}

}
