package it.orbonauts.skafander.pcf.geometry;

public interface Segment extends Has2Vertices {

	@Override
	Vertex getV1();

	@Override
	Vertex getV2();
}
