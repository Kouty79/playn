package it.orbonauts.skafander.core.character;

import static playn.core.PlayN.log;
import it.orbonauts.skafander.core.event.GameEventBus;
import it.orbonauts.skafander.pcf.motion.MotionHelper;
import it.orbonauts.skafander.pcf.sprite.SequencedSprite.SpriteSequenceIterator;

class CharacterMotionListener implements MotionHelper.MotionListener {

	private static final int STEP_WIDTH = 20;
	private final Character character;
	private float lx, ly;
	private WalkingStateMachine walkingStateMachine;
	private SpriteSequenceIterator iterator;
	private PerspectiveScaler perspectiveScaler;
	private float initialSpeed;

	public CharacterMotionListener(Character character, PerspectiveScaler perspectiveScaler) {
		this.character = character;
		this.perspectiveScaler = perspectiveScaler;
	}

	@Override
	public void onMoving(float x, float y) {
		if (walkingStateMachine == null) {
			init();
		}

		walkingStateMachine.update(x, y, character.isMoving());

		applyPerspective(x, y);

		if (!character.isMoving()) {
			log().debug("Character stopped");

			iterator = character.getCharacterSprite().iterator(walkingStateMachine.getCurrentState());
			lx = x;
			ly = y;
			iterator.next();
			character.updateSprite();
		} else {

			if (walkingStateMachine.stateChanged() || moveToNextStep(x, y)) {
				if (walkingStateMachine.stateChanged()) {
					iterator = character.getCharacterSprite().iterator(walkingStateMachine.getCurrentState());
				}

				if (moveToNextStep(x, y)) {
					lx = x;
					ly = y;
				}

				iterator.next();
				character.updateSprite();
			}
		}

		GameEventBus.get().fireEvent(new CharacterMovingEvent(x, y, character));
	}

	PerspectiveScaler getPerspectiveScaler() {
		return perspectiveScaler;
	}

	void setPerspectiveScaler(PerspectiveScaler perspectiveScaler) {
		this.perspectiveScaler = perspectiveScaler;
	}

	private void applyPerspective(float x, float y) {
		perspectiveScaler.updatePosition(x, y);
		character.getLayer().setScale(perspectiveScaler.getScaleW(), perspectiveScaler.getScaleH());
		character.getMotionHelper().setSpeed(initialSpeed * perspectiveScaler.getScaleSpeed());
	}

	private void init() {
		walkingStateMachine = new WalkingStateMachine(WalkingState.IDLE_RIGHT, character.getX(), character.getY());
		lx = character.getX();
		ly = character.getY();
		initialSpeed = character.getMotionHelper().getSpeed();
	}

	private boolean moveToNextStep(float x, float y) {
		return Math.abs(x - lx) > STEP_WIDTH || Math.abs(y - ly) > STEP_WIDTH;
	}
}
