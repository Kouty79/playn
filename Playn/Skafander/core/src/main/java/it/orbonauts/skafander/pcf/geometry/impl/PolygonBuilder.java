package it.orbonauts.skafander.pcf.geometry.impl;

import it.orbonauts.skafander.pcf.geometry.Vertex;

import java.util.ArrayList;
import java.util.List;


public class PolygonBuilder {

	private List<Vertex> vertices;

	public PolygonBuilder() {
		vertices = new ArrayList<Vertex>();
	}

	public PolygonBuilder addVertex(double x, double y) {
		return addVertex(new DefaultVertex(x, y));
	}

	public PolygonBuilder addVertex(Vertex v) {
		vertices.add(v);
		return this;
	}

	public DefaultPolygon build() {
		return new DefaultPolygon(vertices);
	}

}
