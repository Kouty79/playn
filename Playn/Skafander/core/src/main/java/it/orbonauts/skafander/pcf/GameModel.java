package it.orbonauts.skafander.pcf;

public interface GameModel {

	int getWidth();

	int getHeight();

}
