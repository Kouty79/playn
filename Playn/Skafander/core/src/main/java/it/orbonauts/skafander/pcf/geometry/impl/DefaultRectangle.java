package it.orbonauts.skafander.pcf.geometry.impl;

import it.orbonauts.skafander.pcf.geometry.Rectangle;
import it.orbonauts.skafander.pcf.geometry.Vertex;

public class DefaultRectangle extends DefaultSegment implements Rectangle {

	public DefaultRectangle(Vertex top, Vertex bottom) {
		super(top, bottom);
	}

	public DefaultRectangle(Vertex top, double width, double height) {
		super(top, new DefaultVertex(top.getX() + width, top.getY() + height));
	}

	public DefaultRectangle(double x, double y, double width, double height) {
		super(new DefaultVertex(x, y), new DefaultVertex(x + width, y + height));
	}

	@Override
	public double getWidth() {
		return Math.abs(getV1().getX() - getV2().getX());
	}

	@Override
	public double getHeight() {
		return Math.abs(getV1().getY() - getV2().getY());
	}

}
