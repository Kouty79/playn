package it.orbonauts.skafander.core.dechiricolevel;

import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import it.orbonauts.skafander.core.AbstractSkafanderRoom;
import it.orbonauts.skafander.core.character.Character;
import it.orbonauts.skafander.core.character.CharacterMovingEvent;
import it.orbonauts.skafander.core.character.CharacterMovingEventHandler;
import it.orbonauts.skafander.core.character.OverlayHelper;
import it.orbonauts.skafander.core.character.PerspectiveScaler;
import it.orbonauts.skafander.core.character.WalkingState;
import it.orbonauts.skafander.core.event.GameEventBus;
import it.orbonauts.skafander.core.newtonlevel.TestInventoryItem;
import it.orbonauts.skafander.core.pointer.InAreaWalkingClickListener;
import it.orbonauts.skafander.core.resources.Resources;
import it.orbonauts.skafander.pcf.areautils.AreaLayerBuilder;
import it.orbonauts.skafander.pcf.asset.ProgressAssets;
import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.impl.DefaultVertex;
import it.orbonauts.skafander.pcf.pointer.Draggable;
import it.orbonauts.skafander.pcf.pointer.DroppableTarget;
import it.orbonauts.skafander.pcf.pointer.PointerManager;
import it.orbonauts.skafander.pcf.pointer.listener.GlobalClickHelper;
import it.orbonauts.skafander.pcf.walkbehind.WalkBehindAreaHelper;

import java.util.ArrayList;
import java.util.List;

import playn.core.Assets;
import playn.core.Color;
import playn.core.GroupLayer;
import playn.core.Image;
import playn.core.ImageLayer;
import playn.core.Layer.HasSize;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import playn.core.gl.ImmediateLayerGL.Clipped;
import playn.core.util.Callback;

import com.google.web.bindery.event.shared.HandlerRegistration;

public class DeChiricoLevel extends AbstractSkafanderRoom {

	private static final int IMAGE_COUNT = 2;

	private GroupLayer roomLayer;
	private GroupLayer backLayer;
	private GroupLayer midLayer;
	private GroupLayer walkBehindLayer;

	private ProgressAssets progressAssets;
	private Character character;
	private GlobalClickHelper globalPointerListener;
	private WalkBehindAreaHelper behindAreaHelper;

	private Area walkableArea;
	private HandlerRegistration movingRegistration;

	private ImageLayer handShakeLayer;
	private OverlayHelper overlayHelper;

	@Override
	protected void initRoom(GroupLayer gameLayer) {
		walkableArea = new DeChiricoWalkableArea();
		createProgressAssets();

		initRoomLayers();
		initWalkBehindLayers();
		initBackgroundLayer();
		initMidLayer();

		gameLayer.clear();
		gameLayer.add(roomLayer);

		initPointer();
		initHandlers();

		createDropTargetTestArea();

		getGame().getMenu().setIconVisible(true);
	}

	private void initRoomLayers() {
		roomLayer = graphics().createGroupLayer();
		getGame().getScaleHelper().scaleLayer(roomLayer);

		backLayer = graphics().createGroupLayer();
		roomLayer.add(backLayer);

		midLayer = graphics().createGroupLayer();
		roomLayer.add(midLayer);

		walkBehindLayer = graphics().createGroupLayer();
		roomLayer.add(walkBehindLayer);
		behindAreaHelper = new WalkBehindAreaHelper(walkBehindLayer);
	}

	private void initBackgroundLayer() {
		Image bgImage = getProgressAssets().getImage(Resources.DeChiricoLevel.BACKGROUND_IMG.getURI());
		bgImage.addCallback(new Callback<Image>() {

			@Override
			public void onSuccess(Image result) {
				log().debug("onSuccess");

				float bkgScaleX = getGame().getGameModel().getWidth() / result.width();
				float bkgScaleY = getGame().getGameModel().getHeight() / result.height();

				backLayer.setScale(bkgScaleX, bkgScaleY);
				backLayer.add(graphics().createImageLayer(result));

				handShakeLayer.setScale(bkgScaleX, bkgScaleY);
			}

			@Override
			public void onFailure(Throwable cause) {
				log().error("initBackgroundLayer()", cause);
			}

		});
	}

	private void initMidLayer() {
		character = new Character();
		character.setWalkableArea(walkableArea);
		character.setPerspectiveScaler(new PerspectiveScaler() {

			private final int gameHeight = getGame().getGameModel().getHeight();
			private float scale;

			@Override
			public void updatePosition(float x, float y) {
				float dy = gameHeight - y;
				scale = (gameHeight - dy) / gameHeight;
			}

			@Override
			public float getScaleW() {
				return scale;
			}

			@Override
			public float getScaleSpeed() {
				return scale;
			}

			@Override
			public float getScaleH() {
				return scale;
			}
		});

		character.addLoadedCallback(new Callback<Character>() {

			@Override
			public void onSuccess(Character character) {
				overlayHelper = new OverlayHelper(Color.argb(128, 0, 0, 0));
				overlayHelper.setOverlayVisible(false);
				character.setRenderer(overlayHelper);

				midLayer.add(character.getLayer());
				character.placeTo(20, 630, WalkingState.IDLE_RIGHT);
			}

			@Override
			public void onFailure(Throwable cause) {
				log().error("Cannot load character", cause);
			}

		});

	}

	private void initWalkBehindLayers() {
		Image handShakeImg = getProgressAssets().getImage(Resources.DeChiricoLevel.HANDSHAKE_IMG.getURI());
		handShakeLayer = graphics().createImageLayer(handShakeImg);

		handShakeImg.addCallback(new Callback<Image>() {

			@Override
			public void onSuccess(Image result) {
				handShakeLayer.setOrigin(0, result.height());
				walkBehindLayer.addAt(handShakeLayer, 693, 460.5f);
			}

			@Override
			public void onFailure(Throwable cause) {
				log().error("initWalkBehindLayers()", cause);
			}

		});

	}

	private void initPointer() {
		globalPointerListener = new GlobalClickHelper(
				new Pointer.Adapter(),
				new InAreaWalkingClickListener(character, walkableArea, getGame().getScaleHelper(), getGeometryUtils()));

		PointerManager.globalPointerManager().addGlobalListener(globalPointerListener);
	}

	private void initHandlers() {
		final List<Area> shadows = new ArrayList<Area>();
		shadows.add(new RightShadow());
		shadows.add(new TopShadow());
		shadows.add(new LeftShadow());

		movingRegistration = GameEventBus.get().addHandler(CharacterMovingEvent.TYPE,
				new CharacterMovingEventHandler() {

					@Override
					public void onCharacterMoving(CharacterMovingEvent event) {
						behindAreaHelper.update(event.getY());
						for (Area shadow : shadows) {
							boolean pointInArea = getGeometryUtils().pointInArea(
									shadow,
									new DefaultVertex(event.getX(), event.getY()));

							overlayHelper.setOverlayVisible(pointInArea);
							if (pointInArea) {
								break;
							}
						}
					}

				});
	}

	private void createDropTargetTestArea() {
		DroppableTarget droppableArea = new DroppableTarget() {

			private HasSize layer;

			{
				layer = (Clipped) AreaLayerBuilder.areaLayerBuilder()
						.setArea(new TopShadow())
						.withNoRenderer()
						.setClipped(true)
						.build()
						.getLayer();
			}

			@Override
			public boolean acceptsDraggable(Draggable toBeDropped) {
				log().info("acceptsDraggable " + toBeDropped);
				return (toBeDropped instanceof TestInventoryItem);
			}

			@Override
			public void onDrop(Event event, Draggable dropped) {
				log().info("Element dropped: " + dropped);
			}

			@Override
			public HasSize getLayer() {
				return layer;
			}

		};

		// droppableElement.getLayer().addListener(new Pointer.Adapter());
		droppableArea.getLayer().setInteractive(true);

		PointerManager.dragDropManager().registerElement(droppableArea);

		midLayer.add(droppableArea.getLayer());
	}

	private Assets getProgressAssets() {
		return progressAssets;
	}

	private void createProgressAssets() {
		progressAssets = buildProgressAssets(IMAGE_COUNT);
	}

	@Override
	protected void destroyRoom(GroupLayer gameLayer) {
		movingRegistration.removeHandler();
		PointerManager.globalPointerManager().removeGlobalListener(globalPointerListener);
		roomLayer.destroy();
		character.setRenderer(null);
	}

}
