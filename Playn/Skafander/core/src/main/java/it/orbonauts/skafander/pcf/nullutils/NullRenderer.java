package it.orbonauts.skafander.pcf.nullutils;

import playn.core.ImmediateLayer;
import playn.core.Surface;

public class NullRenderer implements ImmediateLayer.Renderer, Nullable {

	private static final NullRenderer INSTANCE = new NullRenderer();

	private NullRenderer() {
	}

	public static NullRenderer nullRenderer() {
		return INSTANCE;
	}

	@Override
	public void render(Surface surface) {
	}

	@Override
	public boolean isNull() {
		return true;
	}

}
