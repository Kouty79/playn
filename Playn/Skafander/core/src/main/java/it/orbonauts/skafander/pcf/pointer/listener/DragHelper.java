package it.orbonauts.skafander.pcf.pointer.listener;

import it.orbonauts.skafander.pcf.scaling.GameScaleHelper;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import playn.core.Pointer.Listener;

public class DragHelper extends PointerListenerDelegator implements Pointer.Listener {

	private float ox;
	private float oy;
	private float tx;
	private float ty;

	private final GameScaleHelper scaleHelper;

	public DragHelper(GameScaleHelper scaleHelper, Listener delegate) {
		super(delegate);
		this.scaleHelper = scaleHelper;
	}

	@Override
	public void onPointerStart(Event event) {
		super.onPointerStart(event);
		ox = event.x();
		oy = event.y();
		tx = event.hit().tx();
		ty = event.hit().ty();
	}

	@Override
	public void onPointerDrag(Event event) {
		super.onPointerDrag(event);

		if (event.hit() != null) {
			float x = scaleHelper.unscaleX(event.x() - ox);
			float y = scaleHelper.unscaleY(event.y() - oy);

			event.hit().setTranslation(x + tx, y + ty);
		}
	}
}
