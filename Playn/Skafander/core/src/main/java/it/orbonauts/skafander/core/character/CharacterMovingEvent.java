package it.orbonauts.skafander.core.character;

import com.google.web.bindery.event.shared.Event;

public class CharacterMovingEvent extends Event<CharacterMovingEventHandler> {

	public static final Type<CharacterMovingEventHandler> TYPE = new Type<CharacterMovingEventHandler>();

	private final float x, y;
	private final Character character;

	CharacterMovingEvent(float x, float y, Character character) {
		super();
		this.x = x;
		this.y = y;
		this.character = character;
	}

	@Override
	protected void dispatch(CharacterMovingEventHandler handler) {
		handler.onCharacterMoving(this);
	}

	@Override
	public Type<CharacterMovingEventHandler> getAssociatedType() {
		return TYPE;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public Character getCharacter() {
		return character;
	}

}
