package it.orbonauts.skafander.core.newtonlevel;

import static it.orbonauts.skafander.core.storage.SchemaManager.schemaManager;
import it.orbonauts.skafander.core.storage.JsonPersistable;
import playn.core.Json;

public class NewtonLevelState implements JsonPersistable {

	private static final String CLOUD_RIDDLE_SOLVED = "cloudRiddleSolved";
	private Json.Object jsonState;

	NewtonLevelState() {
	}

	public boolean isCloudRiddleSolved() {
		return jsonState.getBoolean(CLOUD_RIDDLE_SOLVED, false);
	}

	public void setCloudRiddleSolved(boolean cloudRiddleSolved) {
		jsonState.put(CLOUD_RIDDLE_SOLVED, cloudRiddleSolved);
	}

	@Override
	public Json.Object getState() {
		return jsonState;
	}

	@Override
	public void setState(Json.Object state) {
		this.jsonState = state;
	}

	protected void save() {
		schemaManager().getCurrentStorage().save(this);
	}
}
