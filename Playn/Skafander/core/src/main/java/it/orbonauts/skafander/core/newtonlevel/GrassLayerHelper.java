package it.orbonauts.skafander.core.newtonlevel;

import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import it.orbonauts.skafander.core.resources.Resource;
import it.orbonauts.skafander.pcf.GameModel;
import playn.core.Assets;
import playn.core.Image;
import playn.core.Layer;
import playn.core.SurfaceLayer;
import playn.core.util.Callback;

class GrassLayerHelper {

	public static Layer create(final Resource grassResource, Assets assets, final GameModel model) {
		Image grassImage = assets.getImage(grassResource.getURI());
		final SurfaceLayer grassLayer = graphics().createSurfaceLayer(grassResource.getWidth(),
				grassResource.getHeight());

		grassImage.addCallback(new Callback<Image>() {

			@Override
			public void onSuccess(Image result) {
				final int HEIGHT = grassResource.getHeight();
				float scale = (float) model.getWidth() / grassResource.getWidth();
				grassLayer.surface()
						.clear()
						.scale(scale, -scale)
						.drawImage(result, 0, -HEIGHT);
			}

			@Override
			public void onFailure(Throwable cause) {
				log().error("Error while loading grass image.", cause);
			}

		});

		return grassLayer;
	}

}
