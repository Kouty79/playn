package it.orbonauts.skafander.core.character;

import static playn.core.PlayN.graphics;
import it.orbonauts.skafander.core.resources.Resources;
import playn.core.Image;
import playn.core.ImageLayer;
import playn.core.Layer.HasSize;

public class DefaultCharacterRenderer implements CharacterRenderer {

	private ImageLayer imageLayer;

	@Override
	public void init() {
		imageLayer = graphics().createImageLayer();
		float dx = Resources.Character.LR_SPRITE_SHEET.getSpriteWidth() / 2;
		float dy = Resources.Character.LR_SPRITE_SHEET.getSpriteHeight();
		imageLayer.setOrigin(dx, dy);
	}

	@Override
	public void update(Image currentSprite) {
		imageLayer.setImage(currentSprite);
	}

	@Override
	public HasSize getLayer() {
		return imageLayer;
	}

}
