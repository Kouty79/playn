package it.orbonauts.skafander.pcf.pointer;


public interface DragAndDroppable extends Draggable, DroppableTarget {

}
