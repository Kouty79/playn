package it.orbonauts.skafander.core.menulevel;

import static it.orbonauts.skafander.core.storage.SchemaManager.schemaManager;
import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import it.orbonauts.skafander.core.GameLayers;
import it.orbonauts.skafander.core.dechiricolevel.DeChiricoLevel;
import it.orbonauts.skafander.core.newtonlevel.NewtonLevel;
import it.orbonauts.skafander.core.newtonlevel.NewtonLevelState;
import it.orbonauts.skafander.pcf.impl.AbstractRoom;
import playn.core.Color;
import playn.core.Font;
import playn.core.GroupLayer;
import playn.core.ImmediateLayer;
import playn.core.Layer;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import playn.core.Surface;
import playn.core.TextFormat;
import playn.core.TextLayout;
import pythagoras.f.Point;

public class MenuLevel extends AbstractRoom {

	private static final int FONT_SIZE = 30;
	private static final int BUTTON_HEIGHT = 80;
	private static final int BUTTON_WIDTH = 240;
	private GroupLayer menuLayer;

	@Override
	public void enterRoom() {
		menuLayer = graphics().createGroupLayer();
		getGame().getScaleHelper().scaleLayer(menuLayer);
		menuLayer.add(graphics().createImmediateLayer(new ImmediateLayer.Renderer() {

			@Override
			public void render(Surface surface) {
				float w = getGame().getScaleHelper().unscaleX(surface.width());
				float h = getGame().getScaleHelper().unscaleY(surface.height());
				surface
						.clear()
						.setFillColor(Color.rgb(200, 200, 255))
						.fillRect(0, 0, w, h);

			}
		}));

		Layer newtonButton = createButton("Newton");
		newtonButton.transform().translate(250, 250);
		newtonButton.addListener(new Pointer.Adapter() {

			@Override
			public void onPointerEnd(Event event) {
				boolean inside =
						event.hit().hitTest(new Point(event.localX(), event.localY())) != null;

				if (inside) {
					log().info("Newton click");
					getGame().enterRoom(new NewtonLevel());
				}
			}

		});
		menuLayer.add(newtonButton);

		Layer newtonReset = createButton("Nw. Reset");
		newtonReset.transform().translate(250, 350);
		newtonReset.addListener(new Pointer.Adapter() {

			@Override
			public void onPointerEnd(Event event) {
				log().info("Newton Reset click");
				schemaManager().getCurrentStorage().delete(NewtonLevelState.class);
			}

		});
		menuLayer.add(newtonReset);

		Layer deChiricoButton = createButton("De Chirico");
		deChiricoButton.transform().translate(500, 250);
		deChiricoButton.addListener(new Pointer.Adapter() {

			@Override
			public void onPointerEnd(Event event) {
				log().info("De Chirico click");
				getGame().enterRoom(new DeChiricoLevel());
			}

		});
		menuLayer.add(deChiricoButton);

		GameLayers.getGameLayers().getGameLayer().add(menuLayer);
	}

	@Override
	public void leaveRoom() {
		menuLayer.destroy();
	}

	private Layer createButton(String label) {
		Font font = graphics().createFont("Courier", Font.Style.BOLD, FONT_SIZE);
		TextLayout layout = graphics().layoutText(label, new TextFormat().withFont(font).withWrapWidth(200));

		return new MenuButtonBuilder()
				.setText(layout)
				.setColor(Color.rgb(255, 255, 255))
				.setBkgColor(Color.rgb(72, 137, 240))
				.setWidth(BUTTON_WIDTH)
				.setHeight(BUTTON_HEIGHT)
				.build();
	}

}
