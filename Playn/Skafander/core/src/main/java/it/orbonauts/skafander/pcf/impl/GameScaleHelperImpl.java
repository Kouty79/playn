package it.orbonauts.skafander.pcf.impl;

import it.orbonauts.skafander.pcf.scaling.GameScaleHelper;
import it.orbonauts.skafander.pcf.scaling.ScaleHelper;
import playn.core.Layer;

public class GameScaleHelperImpl implements GameScaleHelper {

	private ScaleHelper delegate;
	private int gameWidth;
	private int gameHeight;

	public GameScaleHelperImpl(ScaleHelper delegate, int gameWidth, int gameHeight) {
		this.delegate = delegate;
		this.gameWidth = gameWidth;
		this.gameHeight = gameHeight;
	}

	@Override
	public void scaleLayer(Layer layer) {
		delegate.scaleLayer(layer);
	}

	@Override
	public float scaleX(double x) {
		return delegate.scaleX(x);
	}

	@Override
	public float scaleY(double y) {
		return delegate.scaleY(y);
	}

	@Override
	public float unscaleX(double x) {
		return delegate.unscaleX(x);
	}

	@Override
	public float unscaleY(double y) {
		return delegate.unscaleY(y);
	}

	@Override
	public float scaleW(double w) {
		return delegate.scaleW(w);
	}

	@Override
	public float scaleH(double h) {
		return delegate.scaleH(h);
	}

	@Override
	public float scaleXComponent() {
		return delegate.scaleXComponent();
	}

	@Override
	public float scaleYComponent() {
		return delegate.scaleYComponent();
	}

	@Override
	public float scaledWidth() {
		return scaleW(gameWidth());
	}

	@Override
	public float scaledHeight() {
		return scaleH(gameHeight());
	}

	@Override
	public int gameWidth() {
		return gameWidth;
	}

	@Override
	public int gameHeight() {
		return gameHeight;
	}

}
