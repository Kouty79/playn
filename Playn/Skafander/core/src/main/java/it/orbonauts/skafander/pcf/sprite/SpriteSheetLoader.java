package it.orbonauts.skafander.pcf.sprite;

import java.util.ArrayList;
import java.util.List;

import playn.core.Image;
import playn.core.util.Callback;

public class SpriteSheetLoader {

	private final Image spriteSheet;
	private final int w;
	private final int h;
	private final int numImages;

	public SpriteSheetLoader(Image spriteSheet, int w, int h, int numImages) {
		this.spriteSheet = spriteSheet;
		this.w = w;
		this.h = h;
		this.numImages = numImages;
	}

	public void load(final Callback<List<Image>> callback) {
		spriteSheet.addCallback(new Callback<Image>() {

			@Override
			public void onSuccess(Image image) {
				float imgW = image.width();
				float imgH = image.height();

				int cols = (int) Math.floor(imgW / w);
				int rows = (int) Math.floor(imgH / h);

				List<Image> images = new ArrayList<Image>();
				for (int r = 0; r < rows; r++) {
					for (int c = 0; c < cols; c++) {
						images.add(image.subImage(c * w + 1, r * h + 1, w - 1, h - 1));
						if (r * cols + c >= numImages - 1) {
							break;
						}
					}
				}

				callback.onSuccess(images);
			}

			@Override
			public void onFailure(Throwable cause) {
				callback.onFailure(cause);
			}

		});
	}
}
