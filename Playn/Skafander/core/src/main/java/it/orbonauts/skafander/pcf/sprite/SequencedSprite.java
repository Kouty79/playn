package it.orbonauts.skafander.pcf.sprite;

public interface SequencedSprite extends Sprite {

	SpriteSequenceIterator iterator(SpriteSequence sequence);

	interface SpriteSequenceIterator {
		void next();
	}

}
