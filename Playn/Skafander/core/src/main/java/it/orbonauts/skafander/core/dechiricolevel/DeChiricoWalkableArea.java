package it.orbonauts.skafander.core.dechiricolevel;

import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Polygon;
import it.orbonauts.skafander.pcf.geometry.impl.AreaBuilder;

import java.util.Iterator;

public class DeChiricoWalkableArea implements Area {

	private final Area area;

	DeChiricoWalkableArea() {
		area = new AreaBuilder()
				.buildBounds()
				.addVertex(2, 610)
				.addVertex(134, 654)
				.addVertex(342, 370)
				.addVertex(1005, 370)
				.addVertex(1009, 712)
				.addVertex(4, 706)
				.done()
				.addHole()
				.addVertex(413, 662)
				.addVertex(491, 462)
				.addVertex(623, 462)
				.addVertex(725, 662)
				.done()
				.addHole()
				.addVertex(687, 464)
				.addVertex(791, 464)
				.addVertex(785, 432)
				.addVertex(677, 432)
				.done()
				.buildArea();
	}

	@Override
	public Polygon getBounds() {
		return area.getBounds();
	}

	@Override
	public Iterator<Polygon> holesIterator() {
		return area.holesIterator();
	}

}
