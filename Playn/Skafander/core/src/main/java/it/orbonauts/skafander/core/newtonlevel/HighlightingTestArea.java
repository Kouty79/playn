package it.orbonauts.skafander.core.newtonlevel;

import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Polygon;
import it.orbonauts.skafander.pcf.geometry.impl.AreaBuilder;

import java.util.Iterator;

class HighlightingTestArea implements Area {

	private final Area delegate;

	public HighlightingTestArea() {
		delegate = new AreaBuilder()
				.buildBounds()
				.addVertex(509.5, 448.0)
				.addVertex(437.5706, 416.0)
				.addVertex(449.55884, 402.0)
				.addVertex(517.4922, 430.0)
				.done()
				.buildArea();
	}

	@Override
	public Polygon getBounds() {
		return delegate.getBounds();
	}

	@Override
	public Iterator<Polygon> holesIterator() {
		return delegate.holesIterator();
	}

}
