package it.orbonauts.skafander.pcf.impl;

import it.orbonauts.skafander.pcf.scaling.ScaleHelper;
import playn.core.Layer;

class ScaleHelperImpl implements ScaleHelper {

	private final float scaleX;
	private final float scaleY;

	public ScaleHelperImpl(float scaleX, float scaleY) {
		this.scaleX = scaleX;
		this.scaleY = scaleY;
	}

	@Override
	public final void scaleLayer(Layer layer) {
		layer.transform().scale(scaleX, scaleY);
		// layer.setScale(this.scaleX, this.scaleY);
	}

	@Override
	public float scaleX(double x) {
		return (float) (x * scaleX);
	}

	@Override
	public float scaleY(double y) {
		return (float) (y * scaleY);
	}

	@Override
	public float unscaleX(double x) {
		return (float) (x / scaleX);
	}

	@Override
	public float unscaleY(double y) {
		return (float) (y / scaleY);
	}

	@Override
	public float scaleW(double w) {
		return Math.max(1, scaleX(w));
	}

	@Override
	public float scaleH(double h) {
		return Math.max(1, scaleY(h));
	}

	@Override
	public float scaleXComponent() {
		return scaleX;
	}

	@Override
	public float scaleYComponent() {
		return scaleY;
	}

}
