package it.orbonauts.skafander.pcf.geometry.impl;

import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Polygon;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


public final class DefaultArea implements Area {

	private Polygon bounds;
	private List<Polygon> holes;

	public DefaultArea(Polygon bounds) {
		this(bounds, null);
	}

	public DefaultArea(Polygon bounds, Collection<? extends Polygon> holes) {
		this.bounds = bounds;
		this.holes = new ArrayList<Polygon>((holes == null) ? new ArrayList<Polygon>() : holes);
	}

	@Override
	public Polygon getBounds() {
		return bounds;
	}

	@Override
	public Iterator<Polygon> holesIterator() {
		return new ImmutableIterator<Polygon>(holes.iterator());
	}

}
