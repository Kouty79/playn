package it.orbonauts.skafander.core.gamemenu;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;
import it.orbonauts.skafander.core.GameLayers;
import it.orbonauts.skafander.core.inventory.InventoryPresenter;
import it.orbonauts.skafander.core.resources.Resource;
import it.orbonauts.skafander.core.resources.Resources;
import it.orbonauts.skafander.pcf.pointer.listener.LayerClickHelper;
import it.orbonauts.skafander.pcf.scaling.ScaleHelper;
import playn.core.GroupLayer;
import playn.core.Image;
import playn.core.Layer.HasSize;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import playn.core.Pointer.Listener;

public class MenuImpl implements Menu {

	private static final float ICON_ALPHA = .8f;
	private static final float ICON_WIDTH = 75;
	private static final float ICON_HEIGHT = 75;
	private static final Resource ICON = Resources.Menu.MENU_ICON;

	private HasSize menuIconLayer;
	private final ScaleHelper scaleHelper;
	private final InventoryPresenter inventoryPresenter;

	public MenuImpl(ScaleHelper scaleHelper, InventoryPresenter inventoryPresenter) {
		this.scaleHelper = scaleHelper;
		this.inventoryPresenter = inventoryPresenter;
		init();
	}

	@Override
	public void setIconVisible(boolean visible) {
		menuIconLayer.setVisible(visible);
	}

	private void init() {
		menuIconLayer = createMenuLayer();
		menuIconLayer.setVisible(false);
		GroupLayer menuLayer = GameLayers.getGameLayers().getMenuLayer();
		menuLayer.addAt(menuIconLayer, graphics().width() - scaleHelper.scaleW(ICON_WIDTH), 1);

		float scaleX = ICON_WIDTH / ICON.getWidth();
		float scaleY = ICON_HEIGHT / ICON.getHeight();
		menuIconLayer.setScale(scaleX, scaleY);
		menuIconLayer.setAlpha(ICON_ALPHA);
		menuIconLayer.addListener(createClickHandler());

		inventoryPresenter.addVisibilityChangeListener(new InventoryPresenter.OnVisibilityChangeListener() {

			@Override
			public void onVisivilityChanged(InventoryPresenter presenter, boolean newVisibility) {
				menuIconLayer.setVisible(!newVisibility);
			}
		});

		scaleHelper.scaleLayer(menuIconLayer);
	}

	private HasSize createMenuLayer() {
		Image menuIcon = assets().getImage(ICON.getURI());
		return graphics().createImageLayer(menuIcon);
	}

	private Listener createClickHandler() {
		return new LayerClickHelper(new Pointer.Adapter(), menuIconLayer, new LayerClickHelper.OnClickListener() {

			@Override
			public void onClick(Event event) {
				inventoryPresenter.setVisible(true);
			}
		});
	}
}
