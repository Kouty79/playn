package it.orbonauts.skafander.pcf.sprite;

import playn.core.Image;

public interface Sprite {

	void setCurrentIndex(int index);

	public int getCurrentIndex();

	public Image getCurrentImage();

	public int getSize();

}
