package it.orbonauts.skafander.pcf.areautils;

import it.orbonauts.skafander.pcf.geometry.Rectangle;
import playn.core.ImmediateLayer;
import playn.core.ImmediateLayer.Renderer;

public class ResultImpl implements AreaLayerBuilder.Result {

	private final ImmediateLayer layer;
	private final Renderer renderer;
	private final Rectangle bounds;

	public ResultImpl(ImmediateLayer layer, Renderer renderer, Rectangle bounds) {
		this.layer = layer;
		this.renderer = renderer;
		this.bounds = bounds;
	}

	@Override
	public ImmediateLayer getLayer() {
		return layer;
	}

	@Override
	public Renderer getRenderer() {
		return renderer;
	}

	@Override
	public Rectangle getBounds() {
		return bounds;
	}

}
