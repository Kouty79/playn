package it.orbonauts.skafander.core.newtonlevel.cloudriddle;

import playn.core.Pointer;
import playn.core.Pointer.Event;

class CloudDragListener extends Pointer.Adapter implements Pointer.Listener {

	private static final int MIN_X = 30;
	private static final int MAX_Y = 80;

	private float startX;
	private float startY;

	private final CloudRiddle riddle;
	private final int blockIndex;

	public CloudDragListener(CloudRiddle riddle, int blockIndex) {
		this.riddle = riddle;
		this.blockIndex = blockIndex;
	}

	@Override
	public void onPointerStart(Event event) {
		startX = event.localX();
		startY = event.localY();
	}

	@Override
	public void onPointerEnd(Event event) {
		if (!riddle.isPlayingState()) {
			return;
		}

		if (coordsInTollerance(event)) {
			if (event.localX() - startX > 0) {
				riddle.moveBlock(blockIndex, +1);
			} else if (event.localX() - startX < 0) {
				riddle.moveBlock(blockIndex, -1);
			}
		}

		if (riddle.hasWon()) {
			riddle.win();
		} else if (riddle.hasFailed()) {
			riddle.fail();
		}
	}

	private boolean deltaYOK(float startY, float y) {
		return Math.abs(y - startY) <= MAX_Y;
	}

	private boolean deltaXOK(float startX, float x) {
		return Math.abs(x - startX) >= MIN_X;
	}

	private boolean coordsInTollerance(Event event) {
		return deltaXOK(event.localX(), startX) && deltaYOK(event.localY(), startY);
	}
}
