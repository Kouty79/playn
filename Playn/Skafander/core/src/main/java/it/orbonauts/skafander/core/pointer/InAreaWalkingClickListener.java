package it.orbonauts.skafander.core.pointer;

import static playn.core.PlayN.log;
import it.orbonauts.skafander.core.character.Character;
import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Vertex;
import it.orbonauts.skafander.pcf.geometry.impl.DefaultVertex;
import it.orbonauts.skafander.pcf.geometry.utils.GeometryUtils;
import it.orbonauts.skafander.pcf.pointer.listener.GlobalClickHelper.OnClickListener;
import it.orbonauts.skafander.pcf.scaling.ScaleHelper;
import playn.core.Pointer.Event;

public class InAreaWalkingClickListener implements OnClickListener {

	private final Character character;
	private final Area walkableArea;
	private final ScaleHelper scaleHelper;
	private final GeometryUtils geometryUtils;

	public InAreaWalkingClickListener(
			Character character, Area walkableArea, ScaleHelper scaleHelper, GeometryUtils geometryUtils) {

		this.character = character;
		this.walkableArea = walkableArea;
		this.scaleHelper = scaleHelper;
		this.geometryUtils = geometryUtils;
	}

	@Override
	public void onClick(Event event) {
		float x = scaleHelper.unscaleX(event.localX());
		float y = scaleHelper.unscaleY(event.localY());

		log().debug("onClick coords: (" + x + ", " + y + ")");

		Vertex v = geometryUtils.nearestPoint(walkableArea, new DefaultVertex(x, y));
		character.moveTo((float) v.getX(), (float) v.getY());
	}

}
