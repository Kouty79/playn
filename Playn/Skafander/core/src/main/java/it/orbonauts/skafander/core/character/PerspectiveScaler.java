package it.orbonauts.skafander.core.character;

public interface PerspectiveScaler {

	void updatePosition(float x, float y);

	float getScaleW();

	float getScaleH();

	float getScaleSpeed();

}
