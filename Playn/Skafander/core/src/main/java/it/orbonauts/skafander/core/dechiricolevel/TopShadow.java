package it.orbonauts.skafander.core.dechiricolevel;

import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Polygon;
import it.orbonauts.skafander.pcf.geometry.impl.AreaBuilder;

import java.util.Iterator;

public class TopShadow implements Area {

	private final Area area;

	public TopShadow() {
		area = new AreaBuilder()
				.buildBounds()
				.addVertex(304, 454)
				.addVertex(340, 368)
				.addVertex(787, 368)
				.done().buildArea();
	}

	@Override
	public Polygon getBounds() {
		return area.getBounds();
	}

	@Override
	public Iterator<Polygon> holesIterator() {
		return area.holesIterator();
	}

}
