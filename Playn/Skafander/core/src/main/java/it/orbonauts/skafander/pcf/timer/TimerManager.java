package it.orbonauts.skafander.pcf.timer;

public class TimerManager {

	private static final TimerManager INSTANCE = new TimerManager();

	private TimerManager() {
	}

	public static TimerManager get() {
		return INSTANCE;
	}

	public Timer scheduleDelayed(int millis, TimerTask task) {
		return new TimerImpl(task, millis);
	}

}
