package it.orbonauts.skafander.core.storage;

import static playn.core.PlayN.json;
import static playn.core.PlayN.storage;
import playn.core.Json.Object;

public class StorageManager {

	private String schema;
	private String prefix;

	public StorageManager(String schema) {
		setSchema(schema);
	}

	public void save(JsonPersistable persistable) {
		String data = json().newWriter().object(persistable.getState()).write();
		storage().setItem(getKey(persistable), data);
	}

	public <T extends JsonPersistable> T read(T element) {
		String json = storage().getItem(getKey(element));
		Object state = (json == null) ? json().createObject() : json().parse(json);
		element.setState(state);

		return element;
	}

	public void delete(Class<? extends JsonPersistable> key) {
		storage().removeItem(getKey(key.getName()));
	}

	public String getSchema() {
		return schema;
	}

	private void setSchema(String schema) {
		this.schema = schema;
		this.prefix = this.schema + ".";
	}

	private String getSchemaPrefix() {
		return prefix;
	}

	private String getKey(JsonPersistable element) {
		return getKey(element.getClass().getName());
	}

	private String getKey(String className) {
		return getSchemaPrefix() + className;
	}
}
