package it.orbonauts.skafander.pcf.impl;

import it.orbonauts.skafander.pcf.animation.Animation;

public abstract class AbstractAnimation implements Animation {

	protected AbstractAnimation() {
		AnimationManager.get().registerElement(this);
	}

	@Override
	public final void destroy() {
		AnimationManager.get().unregisterElement(this);
	}

}
