package it.orbonauts.skafander.pcf.walkbehind;

import playn.core.GroupLayer;
import playn.core.Layer;

public class WalkBehindAreaHelper {

	private final GroupLayer walkBehindLayer;

	public WalkBehindAreaHelper(GroupLayer walkBehindLayer) {
		this.walkBehindLayer = walkBehindLayer;
	}

	public void update(float yBound) {
		for (int i = 0; i < walkBehindLayer.size(); i++) {
			Layer layer = walkBehindLayer.get(i);
			layer.setVisible(layer.ty() > yBound);
		}
	}
}
