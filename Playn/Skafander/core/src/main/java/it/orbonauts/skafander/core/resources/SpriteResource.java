package it.orbonauts.skafander.core.resources;

public interface SpriteResource extends Resource {

	int getSpriteWidth();

	int getSpriteHeight();

	int getNumImages();

}
