package it.orbonauts.skafander.pcf.animation;

import it.orbonauts.skafander.pcf.DynamicElement;

public interface Animation extends DynamicElement {

	void destroy();
}
