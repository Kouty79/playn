package it.orbonauts.skafander.pcf.areautils;

import it.orbonauts.skafander.pcf.geometry.Area;
import it.orbonauts.skafander.pcf.geometry.Rectangle;
import it.orbonauts.skafander.pcf.geometry.impl.DefaultVertex;
import it.orbonauts.skafander.pcf.geometry.utils.GeometryUtils;
import playn.core.Layer;
import playn.core.Layer.HasSize;
import playn.core.Layer.HitTester;
import pythagoras.f.Point;

public class AreaHitTester implements HitTester {

	private final Area area;
	private final GeometryUtils geometryUtils;
	private final Rectangle bounds;

	public AreaHitTester(Area area, GeometryUtils geometryUtils) {
		this.area = area;
		this.geometryUtils = geometryUtils;
		bounds = geometryUtils.bounds(area);
	}

	@Override
	public Layer hitTest(Layer layer, Point p) {
		if (layer instanceof HasSize) {
			if (inLayer((HasSize) layer, p)) {
				return (areaHitTest(p)) ? layer : null;
			}

			return null;
		}

		return (areaHitTest(p)) ? layer : null;
	}

	private boolean areaHitTest(Point p) {
		return geometryUtils.pointInArea(
				area,
				new DefaultVertex(p.x() + bounds.getV1().getX(), p.y() + bounds.getV1().getY()));
	}

	private boolean inLayer(HasSize layer, Point p) {
		return (p.x() >= 0 && p.y() >= 0 &&
				p.x() <= layer.width() && p.y() <= layer.height());
	}
}
