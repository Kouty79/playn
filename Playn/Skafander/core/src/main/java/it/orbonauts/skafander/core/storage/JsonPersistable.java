package it.orbonauts.skafander.core.storage;

import playn.core.Json;

public interface JsonPersistable {

	Json.Object getState();

	void setState(Json.Object state);
}
