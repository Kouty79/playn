package it.orbonauts.skafander.core.newtonlevel.cloudriddle;

import it.orbonauts.skafander.pcf.impl.HelperFactory;
import it.orbonauts.skafander.pcf.motion.MotionHelper;
import playn.core.Layer;

class Block {
	private final Layer layer;
	private final int initialPosition;
	private final int min, max;
	private final int winPosition;
	private final int unit;
	private int pos;
	private final MotionHelper motionHelper;

	Block(Layer layer, int pos, int winPosition, int min, int max, int unit) {
		this.layer = layer;
		this.initialPosition = pos;
		this.unit = unit;
		this.min = min;
		this.max = max;
		this.winPosition = winPosition;

		motionHelper = HelperFactory.createMotionHelper(layer, 0.2f);
	}

	void init() {
		this.setPosition(this.initialPosition);
	};

	void reset() {
		this.moveToPosition(initialPosition);
	}

	void win() {
		this.setPosition(this.winPosition);
	}

	private void setPosition(int position) {
		this.pos = position;
		layer.setTranslation(position * unit, layer.ty());
	}

	private void moveToPosition(int position) {
		this.pos = position;
		motionHelper.moveTo(position * unit, layer.ty());
	}

	void move(int direction) {
		if (this.canMove(direction)) {
			int newPos = this.pos + direction;
			this.moveToPosition(newPos);
		}
	};

	boolean canMove(int direction) {
		int newPos = this.pos + direction;
		return (newPos >= this.min && newPos < this.max);
	}

	Layer getLayer() {
		return layer;
	}

	int getPos() {
		return pos;
	}

}
