package it.orbonauts.skafander.pcf.impl;

import it.orbonauts.skafander.pcf.DynamicElement;

public abstract class AbstractRoom implements DynamicElement {

	private PointAndClickGame game;

	public abstract void enterRoom();

	public abstract void leaveRoom();

	@Override
	public void update(float delta) {
	}

	@Override
	public void paint(float alpha) {
	}

	protected void setGame(PointAndClickGame game) {
		this.game = game;
	}

	public PointAndClickGame getGame() {
		return game;
	}
}
