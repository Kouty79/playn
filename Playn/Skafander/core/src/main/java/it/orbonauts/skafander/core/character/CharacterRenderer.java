package it.orbonauts.skafander.core.character;

import playn.core.Image;
import playn.core.Layer.HasSize;

public interface CharacterRenderer {

	void init();

	HasSize getLayer();

	void update(Image currentSprite);

}
