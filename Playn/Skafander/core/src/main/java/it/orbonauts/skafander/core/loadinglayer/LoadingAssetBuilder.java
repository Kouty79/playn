package it.orbonauts.skafander.core.loadinglayer;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.log;
import it.orbonauts.skafander.core.GameLayers;
import it.orbonauts.skafander.pcf.asset.ProgressAssets;
import it.orbonauts.skafander.pcf.asset.ProgressAssets.ProgressCallback;

public class LoadingAssetBuilder {

	private LoadingAssetBuilder() {
	}

	public static Config newLoadingLayerBuilder() {
		return new BuilderImpl();
	}

	public interface Config {

		Options numberOfImages(int numOfImages);

	}

	public interface Options {

		Options withRange(float start, float end);

		ProgressAssets build();
	}

	private static final class BuilderImpl implements Config, Options {

		private int numOfImages;
		private float start, end;

		private BuilderImpl() {
			start = 0;
			end = 1;
		}

		@Override
		public Options withRange(float start, float end) {
			this.start = start;
			this.end = end;
			return this;
		}

		@Override
		public Options numberOfImages(int numOfImages) {
			this.numOfImages = numOfImages;
			return this;
		}

		@Override
		public ProgressAssets build() {
			LoadingLayerHelper loadingLayerHelper = new LoadingLayerHelper(start, end);
			GameLayers.getGameLayers().getWaitLayer().clear();
			GameLayers.getGameLayers().getWaitLayer().add(loadingLayerHelper.getLayer());
			return createProgressAsset(loadingLayerHelper, numOfImages);
		}

		private ProgressCallback createWaitLayerCallBack(final LoadingLayerHelper loadingLayerHelper) {
			return new ProgressCallback() {

				@Override
				public void onProgress(float progress, int pendingRequestCount) {
					loadingLayerHelper.update(progress);
					if (pendingRequestCount <= 0) {
						loadingLayerHelper.getLayer().setVisible(false);
						log().debug("Loading done.");
					}
				}
			};
		}

		private ProgressAssets createProgressAsset(final LoadingLayerHelper loadingLayerHelper, int imageCount) {
			return new ProgressAssets(assets(), imageCount, createWaitLayerCallBack(loadingLayerHelper));
		}
	}
}
