package it.orbonauts.skafander.pcf.pointer.listener;

import playn.core.GroupLayer;
import playn.core.Layer;
import playn.core.Pointer;
import playn.core.Pointer.Event;
import playn.core.Pointer.Listener;

public class BringToFrontHelper extends PointerListenerDelegator implements Pointer.Listener {

	public BringToFrontHelper(Listener delegate) {
		super(delegate);
	}

	@Override
	public void onPointerStart(Event event) {
		super.onPointerStart(event);
		Layer eventLayer = event.hit();
		if (eventLayer != null) {
			GroupLayer parent = eventLayer.parent();
			for (int i = 0; i < parent.size(); i++) {
				parent.get(i).setDepth(i);
			}
			eventLayer.setDepth(parent.size());
		}
	}

}
