package it.orbonauts.skafander.html;

import it.orbonauts.skafander.core.Skafander;
import playn.core.PlayN;
import playn.html.HtmlGame;
import playn.html.HtmlPlatform;

public class SkafanderHtml extends HtmlGame {

	@Override
	public void start() {
		HtmlPlatform platform = HtmlPlatform.register();
		platform.assets().setPathPrefix("skafander/");
		PlayN.run(new Skafander());
	}
}
